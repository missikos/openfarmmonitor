# Remote Pump Monitor for Farmers

This project provides a way to check on remote devices, like pumps via SMS. A network of nodes report the status of each device to a hub. The hub then forwards the data to the internet where it hooks into an SMS provider allowing you to query and be notified of device states simply.


### Prerequisites

* Hardware as per 'electronics' folder
* Particle account (device to cloud provider)
* Twilio account (SMS provider)
* Losant account (Backend provider)


### Installing

Steps TBA

```
Example
```

Example TBA

## Running the tests

TBA

### End to end tests

TBA

```
Example
```

## Deployment

TBA

## Versioning

TBA

## Authors

* **John Missikos** - *Initial work* - [Tinker Electric](http://tinkerelectric.com)


## License

TBA - Open Source Hardware with Atribution

## Acknowledgments

TBA

