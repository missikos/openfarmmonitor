<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="b3D" color="5" fill="4" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="10" fill="10" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="11" fill="10" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="12" fill="10" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="13" fill="10" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="14" fill="10" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="15" fill="10" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="16" fill="10" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="17" fill="10" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="TinkerElectric">
<packages>
<package name="1210_220">
<smd name="1" x="-1.4986" y="0" dx="1.1176" dy="2.6924" layer="1"/>
<smd name="2" x="1.4986" y="0" dx="1.1176" dy="2.6924" layer="1"/>
<text x="-3.2766" y="-3.175" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
<wire x1="-0.6096" y1="-1.3462" x2="0.6096" y2="-1.3462" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="1.3462" x2="-0.6096" y2="1.3462" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="-1.3462" x2="-0.9398" y2="1.3462" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="1.3462" x2="-1.7018" y2="1.3462" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="-1.3462" x2="-0.9398" y2="-1.3462" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="1.3462" x2="0.9398" y2="-1.3462" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="-1.3462" x2="1.7018" y2="-1.3462" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="1.3462" x2="0.9398" y2="1.3462" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="-1.3462" x2="0.9398" y2="-1.3462" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-1.3462" x2="1.7018" y2="1.3462" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="1.3462" x2="-0.9398" y2="1.3462" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="1.3462" x2="-1.7018" y2="-1.3462" width="0.1524" layer="25"/>
<text x="-2.8702" y="1.905" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="C3216X7R2A105M160AA">
<smd name="1" x="-1.4986" y="0" dx="1.1176" dy="1.8034" layer="1"/>
<smd name="2" x="1.4986" y="0" dx="1.1176" dy="1.8034" layer="1"/>
<wire x1="-0.6096" y1="-0.9144" x2="0.6096" y2="-0.9144" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="0.9144" x2="-0.6096" y2="0.9144" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="-0.9144" x2="-0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="0.9144" x2="-1.7018" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="-0.9144" x2="-0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="0.9144" x2="0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-0.889" x2="1.7018" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="0.9144" x2="0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="-0.9144" x2="1.7018" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="0.9144" x2="-0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="0.9144" x2="-1.7018" y2="-0.9144" width="0.1524" layer="25"/>
<text x="-2.8702" y="1.27" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-2.54" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
<package name="CC1206KRX7R9BB393">
<smd name="1" x="-1.4986" y="0" dx="1.1176" dy="1.8034" layer="1"/>
<smd name="2" x="1.4986" y="0" dx="1.1176" dy="1.8034" layer="1"/>
<wire x1="-0.6096" y1="-0.9144" x2="0.6096" y2="-0.9144" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="0.9144" x2="-0.6096" y2="0.9144" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="-0.9144" x2="-0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="0.9144" x2="-1.7018" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="-0.9144" x2="-0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="0.9144" x2="0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-0.889" x2="1.7018" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="0.9144" x2="0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="-0.9144" x2="1.7018" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="0.9144" x2="-0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="0.9144" x2="-1.7018" y2="-0.9144" width="0.1524" layer="25"/>
<text x="-2.8702" y="1.27" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-2.6416" y="-2.54" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
<package name="CRCW120663K4FKEA">
<smd name="1" x="-1.4986" y="0" dx="1.1176" dy="1.8034" layer="1"/>
<smd name="2" x="1.4986" y="0" dx="1.1176" dy="1.8034" layer="1"/>
<wire x1="-0.6096" y1="-0.9144" x2="0.6096" y2="-0.9144" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="0.9144" x2="-0.6096" y2="0.9144" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="-0.9144" x2="-0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="0.9144" x2="-1.7018" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="-0.9144" x2="-0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="0.9144" x2="0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-0.889" x2="1.7018" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="0.9144" x2="0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="-0.9144" x2="1.7018" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="0.9144" x2="-0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="0.9144" x2="-1.7018" y2="-0.9144" width="0.1524" layer="25"/>
<text x="-2.8702" y="1.27" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-2.54" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
<package name="CRCW12061K00FKEA">
<smd name="1" x="-1.4986" y="0" dx="1.1176" dy="1.8034" layer="1"/>
<smd name="2" x="1.4986" y="0" dx="1.1176" dy="1.8034" layer="1"/>
<wire x1="-0.6096" y1="-0.9144" x2="0.6096" y2="-0.9144" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="0.9144" x2="-0.6096" y2="0.9144" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="-0.9144" x2="-0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="0.9144" x2="-1.7018" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="-0.9144" x2="-0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="0.9144" x2="0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-0.889" x2="1.7018" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="0.9144" x2="0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="-0.9144" x2="1.7018" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="0.9144" x2="-0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="0.9144" x2="-1.7018" y2="-0.9144" width="0.1524" layer="25"/>
<text x="-2.8702" y="1.27" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-2.54" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
<package name="CRCW12062K94FKEA">
<smd name="1" x="-1.4986" y="0" dx="1.1176" dy="1.8034" layer="1"/>
<smd name="2" x="1.4986" y="0" dx="1.1176" dy="1.8034" layer="1"/>
<wire x1="-0.6096" y1="-0.9144" x2="0.6096" y2="-0.9144" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="0.9144" x2="-0.6096" y2="0.9144" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="-0.9144" x2="-0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="0.9144" x2="-1.7018" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="-0.9144" x2="-0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="0.9144" x2="0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-0.889" x2="1.7018" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="0.9144" x2="0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="-0.9144" x2="1.7018" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="0.9144" x2="-0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="0.9144" x2="-1.7018" y2="-0.9144" width="0.1524" layer="25"/>
<text x="-2.8702" y="1.27" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-2.54" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
<package name="1206">
<smd name="1" x="-1.4986" y="0" dx="1.1176" dy="1.8034" layer="1"/>
<smd name="2" x="1.4986" y="0" dx="1.1176" dy="1.8034" layer="1"/>
<wire x1="0.9398" y1="-0.9144" x2="1.7018" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="-0.6096" y1="-0.9144" x2="0.6096" y2="-0.9144" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="0.9144" x2="-0.6096" y2="0.9144" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="-0.9144" x2="-0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="0.9144" x2="-1.7018" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="-0.9144" x2="-0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="0.9144" x2="0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-0.889" x2="1.7018" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="0.9144" x2="0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-0.9398" y1="-0.9144" x2="0.9398" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="1.7018" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="0.9398" y1="0.9144" x2="-0.9398" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="-1.7018" y1="0.9144" x2="-1.7018" y2="-0.9144" width="0.1524" layer="25"/>
<text x="-2.8702" y="1.27" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-2.6416" y="-2.54" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
<package name="SRN8040">
<smd name="1" x="-2.850003125" y="0" dx="2.2" dy="8" layer="1"/>
<smd name="2" x="2.850003125" y="0" dx="2.2" dy="8" layer="1"/>
<wire x1="-1.4224" y1="-3.9878" x2="1.4224" y2="-3.9878" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="3.9878" x2="-1.4224" y2="3.9878" width="0.1524" layer="21"/>
<wire x1="-2.0066" y1="3.9878" x2="-3.9878" y2="2.0066" width="0.1524" layer="25"/>
<wire x1="-3.9878" y1="2.0066" x2="-3.9878" y2="-2.0066" width="0.1524" layer="25"/>
<wire x1="-3.9878" y1="-2.0066" x2="-2.0066" y2="-3.9878" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="-3.9878" x2="2.0066" y2="-3.9878" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="-3.9878" x2="3.9878" y2="-2.0066" width="0.1524" layer="25"/>
<wire x1="3.9878" y1="-2.0066" x2="3.9878" y2="2.0066" width="0.1524" layer="25"/>
<wire x1="3.9878" y1="2.0066" x2="2.0066" y2="3.9878" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="3.9878" x2="-2.0066" y2="3.9878" width="0.1524" layer="25"/>
<text x="4.1148" y="-1.27" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="4.3434" y="2.54" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
<package name="SMA">
<smd name="1" x="-2" y="0" dx="2" dy="2" layer="1" rot="R270"/>
<smd name="2" x="2" y="0" dx="2" dy="2" layer="1" rot="R270"/>
<polygon width="0.0254" layer="21">
<vertex x="-3.6576" y="-1.5748"/>
<vertex x="-3.1496" y="-1.5748"/>
<vertex x="-3.1496" y="1.574803125"/>
<vertex x="-3.6576" y="1.574803125"/>
</polygon>
<polygon width="0.0254" layer="25">
<vertex x="-3.6576" y="-1.5748"/>
<vertex x="-3.1496" y="-1.5748"/>
<vertex x="-3.1496" y="1.574803125"/>
<vertex x="-3.6576" y="1.574803125"/>
</polygon>
<polygon width="0.0254" layer="21">
<vertex x="-3.6576" y="-1.5748"/>
<vertex x="-3.1496" y="-1.5748"/>
<vertex x="-3.1496" y="1.574803125"/>
<vertex x="-3.6576" y="1.574803125"/>
</polygon>
<polygon width="0.0254" layer="25">
<vertex x="-3.6576" y="-1.5748"/>
<vertex x="-3.1496" y="-1.5748"/>
<vertex x="-3.1496" y="1.574803125"/>
<vertex x="-3.6576" y="1.574803125"/>
</polygon>
<wire x1="-3.3274" y1="-1.4986" x2="2.794" y2="-1.4986" width="0.1778" layer="21"/>
<wire x1="-3.3274" y1="1.4986" x2="2.794" y2="1.4986" width="0.1778" layer="21"/>
<polygon width="0.0254" layer="21">
<vertex x="-3.6576" y="-1.5748"/>
<vertex x="-3.1496" y="-1.5748"/>
<vertex x="-3.1496" y="1.574803125"/>
<vertex x="-3.6576" y="1.574803125"/>
</polygon>
<wire x1="-3.3274" y1="1.4986" x2="2.794" y2="1.4986" width="0.1524" layer="25"/>
<wire x1="-3.3274" y1="-1.4986" x2="2.794" y2="-1.4986" width="0.1524" layer="25"/>
<wire x1="2.794" y1="-1.4986" x2="2.794" y2="1.4986" width="0.1524" layer="25"/>
<polygon width="0.0254" layer="25">
<vertex x="-3.6576" y="-1.5748"/>
<vertex x="-3.1496" y="-1.5748"/>
<vertex x="-3.1496" y="1.574803125"/>
<vertex x="-3.6576" y="1.574803125"/>
</polygon>
<text x="-2.8702" y="1.905" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-3.175" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
<package name="3528-21">
<smd name="1" x="-1.1303" y="0" dx="2.3114" dy="1.7526" layer="1" rot="R90"/>
<smd name="2" x="1.1303" y="0" dx="2.3114" dy="1.7526" layer="1" rot="R90"/>
<text x="-2.8702" y="1.905" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="-2.9972" y1="0.381" x2="-2.9972" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-1.8542" y1="-1.4986" x2="1.8542" y2="-1.4986" width="0.1524" layer="21"/>
<wire x1="1.8542" y1="1.4986" x2="-1.8542" y2="1.4986" width="0.1524" layer="21"/>
<wire x1="-3.2512" y1="0" x2="-2.7432" y2="0" width="0.1524" layer="25"/>
<wire x1="-1.8542" y1="-1.4986" x2="1.8542" y2="-1.4986" width="0.1524" layer="25"/>
<wire x1="1.8542" y1="-1.4986" x2="1.8542" y2="1.4986" width="0.1524" layer="25"/>
<wire x1="1.8542" y1="1.4986" x2="-1.8542" y2="1.4986" width="0.1524" layer="25"/>
<wire x1="-1.8542" y1="1.4986" x2="-1.8542" y2="-1.4986" width="0.1524" layer="25"/>
<text x="-2.8702" y="-3.175" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="MRA08B">
<smd name="1" x="-2.6543" y="1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="2" x="-2.6543" y="0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="3" x="-2.6543" y="-0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="4" x="-2.6543" y="-1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="5" x="2.6543" y="-1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="6" x="2.6543" y="-0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="7" x="2.6543" y="0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="8" x="2.6543" y="1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="9" x="0" y="0" dx="2.413" dy="3.0988" layer="1" cream="no"/>
<wire x1="-2.0066" y1="1.6764" x2="-2.0066" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.1336" x2="-3.0988" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.1336" x2="-3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-2.0066" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.4064" x2="-2.0066" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.8636" x2="-3.0988" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.8636" x2="-3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-2.0066" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.8636" x2="-2.0066" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.4064" x2="-3.0988" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.4064" x2="-3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-2.0066" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.1336" x2="-2.0066" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-1.6764" x2="-3.0988" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.6764" x2="-3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-2.0066" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-1.6764" x2="2.0066" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.1336" x2="3.0988" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.1336" x2="3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="2.0066" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.4064" x2="2.0066" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.8636" x2="3.0988" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.8636" x2="3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="2.0066" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.8636" x2="2.0066" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.4064" x2="3.0988" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.4064" x2="3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="2.0066" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.1336" x2="2.0066" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="1.6764" x2="3.0988" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.6764" x2="3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="2.0066" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.4892" x2="2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.4892" x2="-2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="51" curve="-180"/>
<text x="-2.2098" y="1.1684" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="1.4494"/>
<vertex x="-1.1065" y="0.1"/>
<vertex x="1.1065" y="0.1"/>
<vertex x="1.1065" y="1.4494"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="-0.1"/>
<vertex x="-1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-0.1"/>
</polygon>
<wire x1="-2.1336" y1="-2.6416" x2="2.1336" y2="-2.6416" width="0.1524" layer="21"/>
<wire x1="2.1336" y1="2.6416" x2="-2.1336" y2="2.6416" width="0.1524" layer="21"/>
<text x="-3.4798" y="2.3114" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="2.0066" y1="-2.4892" x2="2.0066" y2="-2.1336" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="-1.6764" x2="2.0066" y2="-0.8636" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="0.8636" x2="2.0066" y2="1.6764" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="2.1336" x2="2.0066" y2="2.4892" width="0.1524" layer="25"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="2.4892" x2="-2.0066" y2="2.1336" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="1.6764" x2="-2.0066" y2="0.8636" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="-0.8636" x2="-2.0066" y2="-1.6764" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="-2.1336" x2="-2.0066" y2="-2.4892" width="0.1524" layer="25"/>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="25" curve="-180"/>
<text x="-3.4798" y="2.3114" size="1.27" layer="25" ratio="6" rot="SR0">*</text>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="1.4494"/>
<vertex x="-1.1065" y="0.1"/>
<vertex x="1.1065" y="0.1"/>
<vertex x="1.1065" y="1.4494"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="-0.1"/>
<vertex x="-1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-0.1"/>
</polygon>
<wire x1="-0.3048" y1="2.4892" x2="-1.8034" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="21" curve="-180"/>
<text x="-3.4798" y="2.3114" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="25" curve="-180"/>
<text x="-3.4798" y="2.3114" size="1.27" layer="25" ratio="6" rot="SR0">*</text>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="1.4494"/>
<vertex x="-1.1065" y="0.1"/>
<vertex x="1.1065" y="0.1"/>
<vertex x="1.1065" y="1.4494"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="-0.1"/>
<vertex x="-1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-0.1"/>
</polygon>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="21" curve="-180"/>
<text x="-3.4798" y="2.3114" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="-2.0066" y1="1.6764" x2="-2.0066" y2="2.1336" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="2.1336" x2="-3.0988" y2="2.1336" width="0.1524" layer="25"/>
<wire x1="-3.0988" y1="2.1336" x2="-3.0988" y2="1.651" width="0.1524" layer="25"/>
<wire x1="-3.0988" y1="1.651" x2="-2.0066" y2="1.6764" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="0.4064" x2="-2.0066" y2="0.8636" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="0.8636" x2="-3.0988" y2="0.8636" width="0.1524" layer="25"/>
<wire x1="-3.0988" y1="0.8636" x2="-3.0988" y2="0.381" width="0.1524" layer="25"/>
<wire x1="-3.0988" y1="0.381" x2="-2.0066" y2="0.4064" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="-0.8636" x2="-2.0066" y2="-0.4064" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="-0.4064" x2="-3.0988" y2="-0.4064" width="0.1524" layer="25"/>
<wire x1="-3.0988" y1="-0.4064" x2="-3.0988" y2="-0.889" width="0.1524" layer="25"/>
<wire x1="-3.0988" y1="-0.889" x2="-2.0066" y2="-0.8636" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="-2.1336" x2="-2.0066" y2="-1.6764" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="-1.6764" x2="-3.0988" y2="-1.6764" width="0.1524" layer="25"/>
<wire x1="-3.0988" y1="-1.6764" x2="-3.0988" y2="-2.159" width="0.1524" layer="25"/>
<wire x1="-3.0988" y1="-2.159" x2="-2.0066" y2="-2.1336" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="-1.6764" x2="2.0066" y2="-2.1336" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="-2.1336" x2="3.0988" y2="-2.1336" width="0.1524" layer="25"/>
<wire x1="3.0988" y1="-2.1336" x2="3.0988" y2="-1.651" width="0.1524" layer="25"/>
<wire x1="3.0988" y1="-1.651" x2="2.0066" y2="-1.6764" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="-0.4064" x2="2.0066" y2="-0.8636" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="-0.8636" x2="3.0988" y2="-0.8636" width="0.1524" layer="25"/>
<wire x1="3.0988" y1="-0.8636" x2="3.0988" y2="-0.381" width="0.1524" layer="25"/>
<wire x1="3.0988" y1="-0.381" x2="2.0066" y2="-0.4064" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="0.8636" x2="2.0066" y2="0.4064" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="0.4064" x2="3.0988" y2="0.4064" width="0.1524" layer="25"/>
<wire x1="3.0988" y1="0.4064" x2="3.0988" y2="0.889" width="0.1524" layer="25"/>
<wire x1="3.0988" y1="0.889" x2="2.0066" y2="0.8636" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="2.1336" x2="2.0066" y2="1.6764" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="1.6764" x2="3.0988" y2="1.6764" width="0.1524" layer="25"/>
<wire x1="3.0988" y1="1.6764" x2="3.0988" y2="2.159" width="0.1524" layer="25"/>
<wire x1="3.0988" y1="2.159" x2="2.0066" y2="2.1336" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="-0.4064" x2="2.0066" y2="0.4064" width="0.1524" layer="25"/>
<wire x1="2.0066" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="25"/>
<wire x1="-2.0066" y1="0.4064" x2="-2.0066" y2="-0.4064" width="0.1524" layer="25"/>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="25" curve="-180"/>
<text x="-3.4798" y="2.3114" size="1.27" layer="25" ratio="6" rot="SR0">*</text>
<text x="-2.8702" y="-4.445" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="1.4494"/>
<vertex x="-1.1065" y="0.1"/>
<vertex x="1.1065" y="0.1"/>
<vertex x="1.1065" y="1.4494"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="-0.1"/>
<vertex x="-1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-0.1"/>
</polygon>
<wire x1="-1.8034" y1="-2.4892" x2="1.8034" y2="-2.4892" width="0.1524" layer="21"/>
<wire x1="1.8034" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="21" curve="-180"/>
<text x="-3.4798" y="2.3114" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-2.6416" y="3.175" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
<package name="MODULE_1">
<wire x1="15.24" y1="-28.88" x2="15.24" y2="0" width="0.2032" layer="21"/>
<wire x1="15.24" y1="0" x2="-8.885990625" y2="0" width="0.2032" layer="21"/>
<wire x1="-8.885990625" y1="0" x2="-15.22" y2="-6.334009375" width="0.2032" layer="21"/>
<wire x1="-15.22" y1="-6.334009375" x2="-15.22" y2="-28.88" width="0.2032" layer="21"/>
<wire x1="-15.22" y1="-28.88" x2="15.24" y2="-28.88" width="0.2032" layer="21"/>
<pad name="B1" x="12.7" y="-8.89" drill="0.8" rot="R180"/>
<pad name="B2" x="12.7" y="-11.43" drill="0.8" rot="R180"/>
<pad name="B3" x="12.7" y="-13.97" drill="0.8" rot="R180"/>
<pad name="B4" x="12.7" y="-16.51" drill="0.8" rot="R180"/>
<pad name="B5" x="12.7" y="-19.05" drill="0.8" rot="R180"/>
<pad name="A1" x="-12.7" y="-8.89" drill="0.8" rot="R180"/>
<pad name="A2" x="-12.7" y="-11.43" drill="0.8" rot="R180"/>
<pad name="A3" x="-12.7" y="-13.97" drill="0.8" rot="R180"/>
<pad name="A4" x="-12.7" y="-16.51" drill="0.8" rot="R180"/>
<pad name="A5" x="-12.7" y="-19.05" drill="0.8" rot="R180"/>
<text x="3.81" y="-22.86" size="1.27" layer="21" rot="R180">&gt;NAME</text>
<text x="3.81" y="-19.05" size="1.27" layer="21" rot="R180">&gt;VALUE</text>
<hole x="12.7" y="-2.54" drill="3.2"/>
<polygon width="0.127" layer="41">
<vertex x="15.24" y="0"/>
<vertex x="10.16" y="0"/>
<vertex x="10.16" y="-5.08"/>
<vertex x="15.24" y="-5.08"/>
</polygon>
<polygon width="0.127" layer="42">
<vertex x="10.16" y="-5.08"/>
<vertex x="15.24" y="-5.08"/>
<vertex x="15.24" y="0"/>
<vertex x="10.16" y="0"/>
</polygon>
</package>
<package name="SOLENOID_THIN">
<text x="-3.89" y="-9.27" size="1.27" layer="21">&gt;NAME</text>
<text x="-3.89" y="-11.83" size="1.27" layer="21">&gt;VALUE</text>
<wire x1="-10" y1="-18" x2="-10" y2="14.094878125" width="0.127" layer="21"/>
<wire x1="-10" y1="14.094878125" x2="-6.094878125" y2="18" width="0.127" layer="21"/>
<wire x1="-6.094878125" y1="18" x2="12" y2="18" width="0.127" layer="21"/>
<wire x1="12" y1="18" x2="12" y2="-18" width="0.127" layer="21"/>
<wire x1="12" y1="-18" x2="-10" y2="-18" width="0.127" layer="21"/>
<pad name="A3" x="0" y="15" drill="0.8"/>
<pad name="A2" x="-2.54" y="15" drill="0.8"/>
<pad name="A1" x="-5.08" y="15" drill="0.8"/>
<pad name="A4" x="2.54" y="15" drill="0.8"/>
<pad name="A5" x="5.08" y="15" drill="0.8"/>
<pad name="B3" x="0" y="-15" drill="0.8"/>
<pad name="B2" x="-2.54" y="-15" drill="0.8"/>
<pad name="B1" x="-5.08" y="-15" drill="0.8"/>
<pad name="B4" x="2.54" y="-15" drill="0.8"/>
<pad name="B5" x="5.08" y="-15" drill="0.8"/>
<hole x="9.525" y="15.24" drill="3.2"/>
</package>
</packages>
<symbols>
<symbol name="WB_CAPACITOR">
<pin name="1" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<wire x1="0.762" y1="0" x2="0.762" y2="3.048" width="0.1524" layer="94"/>
<text x="-4.7244" y="6.5786" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<wire x1="-0.762" y1="0" x2="-0.762" y2="3.048" width="0.1524" layer="94"/>
<text x="-4.7244" y="6.5786" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<wire x1="0.762" y1="-3.048" x2="0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="-3.048" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<text x="-4.7244" y="6.5786" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
<symbol name="WB_RESISTOR">
<pin name="1" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<text x="-4.7244" y="4.6736" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<text x="-4.7244" y="4.6736" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<wire x1="7.62" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="3.81" y2="1.524" width="0.1524" layer="94"/>
<wire x1="3.81" y1="1.524" x2="1.905" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-1.524" x2="0" y2="1.524" width="0.1524" layer="94"/>
<wire x1="0" y1="1.524" x2="-1.905" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.524" x2="-3.81" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="1.524" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<text x="-4.7244" y="4.6736" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
<symbol name="WB_INDUCTOR">
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<wire x1="2.54" y1="0" x2="2.6924" y2="0.8382" width="0.1524" layer="94" curve="-12"/>
<wire x1="2.6924" y1="0.8382" x2="3.81" y2="1.8542" width="0.1524" layer="94" curve="-78"/>
<wire x1="3.81" y1="1.8542" x2="4.9022" y2="0.8382" width="0.1524" layer="94" curve="-79"/>
<wire x1="4.9022" y1="0.8382" x2="5.08" y2="0.0762" width="0.1524" layer="94" curve="-2"/>
<wire x1="0" y1="0" x2="0.1524" y2="0.8382" width="0.1524" layer="94" curve="-12"/>
<wire x1="0.1524" y1="0.8382" x2="1.27" y2="1.8542" width="0.1524" layer="94" curve="-78"/>
<wire x1="1.27" y1="1.8542" x2="2.3622" y2="0.8382" width="0.1524" layer="94" curve="-79"/>
<wire x1="2.3622" y1="0.8382" x2="2.54" y2="-0.1778" width="0.1524" layer="94" curve="-17"/>
<wire x1="-2.54" y1="0" x2="-2.3876" y2="0.8382" width="0.1524" layer="94" curve="-12"/>
<wire x1="-2.3876" y1="0.8382" x2="-1.27" y2="1.8542" width="0.1524" layer="94" curve="-78"/>
<wire x1="-1.27" y1="1.8542" x2="-0.1778" y2="0.8382" width="0.1524" layer="94" curve="-79"/>
<wire x1="-0.1778" y1="0.8382" x2="0" y2="-0.1778" width="0.1524" layer="94" curve="-17"/>
<wire x1="-5.08" y1="0" x2="-4.9276" y2="0.8382" width="0.1524" layer="94" curve="-12"/>
<wire x1="-4.9276" y1="0.8382" x2="-3.81" y2="1.8542" width="0.1524" layer="94" curve="-78"/>
<wire x1="-3.81" y1="1.8542" x2="-2.7178" y2="0.8382" width="0.1524" layer="94" curve="-79"/>
<wire x1="-2.7178" y1="0.8382" x2="-2.54" y2="-0.1778" width="0.1524" layer="94" curve="-17"/>
<wire x1="-2.54" y1="-0.1778" x2="-2.54" y2="0" width="0.1524" layer="94" curve="-53"/>
<wire x1="0" y1="-0.1778" x2="0" y2="0" width="0.1524" layer="94" curve="-53"/>
<wire x1="2.54" y1="-0.1778" x2="2.54" y2="0" width="0.1524" layer="94" curve="-53"/>
<text x="-4.7244" y="4.6736" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<wire x1="2.54" y1="0" x2="2.6924" y2="0.8382" width="0.1524" layer="94" curve="-12"/>
<wire x1="2.6924" y1="0.8382" x2="3.81" y2="1.8542" width="0.1524" layer="94" curve="-78"/>
<wire x1="3.81" y1="1.8542" x2="4.9022" y2="0.8382" width="0.1524" layer="94" curve="-79"/>
<wire x1="4.9022" y1="0.8382" x2="5.08" y2="0.0762" width="0.1524" layer="94" curve="-2"/>
<wire x1="0" y1="0" x2="0.1524" y2="0.8382" width="0.1524" layer="94" curve="-12"/>
<wire x1="0.1524" y1="0.8382" x2="1.27" y2="1.8542" width="0.1524" layer="94" curve="-78"/>
<wire x1="1.27" y1="1.8542" x2="2.3622" y2="0.8382" width="0.1524" layer="94" curve="-79"/>
<wire x1="2.3622" y1="0.8382" x2="2.54" y2="-0.1778" width="0.1524" layer="94" curve="-17"/>
<wire x1="-2.54" y1="0" x2="-2.3876" y2="0.8382" width="0.1524" layer="94" curve="-12"/>
<wire x1="-2.3876" y1="0.8382" x2="-1.27" y2="1.8542" width="0.1524" layer="94" curve="-78"/>
<wire x1="-1.27" y1="1.8542" x2="-0.1778" y2="0.8382" width="0.1524" layer="94" curve="-79"/>
<wire x1="-0.1778" y1="0.8382" x2="0" y2="-0.1778" width="0.1524" layer="94" curve="-17"/>
<wire x1="-5.08" y1="0" x2="-4.9276" y2="0.8382" width="0.1524" layer="94" curve="-12"/>
<wire x1="-4.9276" y1="0.8382" x2="-3.81" y2="1.8542" width="0.1524" layer="94" curve="-78"/>
<wire x1="-3.81" y1="1.8542" x2="-2.7178" y2="0.8382" width="0.1524" layer="94" curve="-79"/>
<wire x1="-2.7178" y1="0.8382" x2="-2.54" y2="-0.1778" width="0.1524" layer="94" curve="-17"/>
<wire x1="-2.54" y1="-0.1778" x2="-2.54" y2="0" width="0.1524" layer="94" curve="-53"/>
<wire x1="0" y1="-0.1778" x2="0" y2="0" width="0.1524" layer="94" curve="-53"/>
<wire x1="2.54" y1="-0.1778" x2="2.54" y2="0" width="0.1524" layer="94" curve="-53"/>
<text x="-4.7244" y="4.6736" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<wire x1="5.08" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.6924" y2="0.8382" width="0.1524" layer="94" curve="-12"/>
<wire x1="2.6924" y1="0.8382" x2="3.81" y2="1.8542" width="0.1524" layer="94" curve="-78"/>
<wire x1="3.81" y1="1.8542" x2="4.9022" y2="0.8382" width="0.1524" layer="94" curve="-79"/>
<wire x1="4.9022" y1="0.8382" x2="5.08" y2="0.0762" width="0.1524" layer="94" curve="-2"/>
<wire x1="0" y1="0" x2="0.1524" y2="0.8382" width="0.1524" layer="94" curve="-12"/>
<wire x1="0.1524" y1="0.8382" x2="1.27" y2="1.8542" width="0.1524" layer="94" curve="-78"/>
<wire x1="1.27" y1="1.8542" x2="2.3622" y2="0.8382" width="0.1524" layer="94" curve="-79"/>
<wire x1="2.3622" y1="0.8382" x2="2.54" y2="-0.1778" width="0.1524" layer="94" curve="-17"/>
<wire x1="-2.54" y1="0" x2="-2.3876" y2="0.8382" width="0.1524" layer="94" curve="-12"/>
<wire x1="-2.3876" y1="0.8382" x2="-1.27" y2="1.8542" width="0.1524" layer="94" curve="-78"/>
<wire x1="-1.27" y1="1.8542" x2="-0.1778" y2="0.8382" width="0.1524" layer="94" curve="-79"/>
<wire x1="-0.1778" y1="0.8382" x2="0" y2="-0.1778" width="0.1524" layer="94" curve="-17"/>
<wire x1="-5.08" y1="0" x2="-4.9276" y2="0.8382" width="0.1524" layer="94" curve="-12"/>
<wire x1="-4.9276" y1="0.8382" x2="-3.81" y2="1.8542" width="0.1524" layer="94" curve="-78"/>
<wire x1="-3.81" y1="1.8542" x2="-2.7178" y2="0.8382" width="0.1524" layer="94" curve="-79"/>
<wire x1="-2.7178" y1="0.8382" x2="-2.54" y2="-0.1778" width="0.1524" layer="94" curve="-17"/>
<wire x1="-2.54" y1="-0.1778" x2="-2.54" y2="0" width="0.1524" layer="94" curve="-53"/>
<wire x1="0" y1="-0.1778" x2="0" y2="0" width="0.1524" layer="94" curve="-53"/>
<wire x1="2.54" y1="-0.1778" x2="2.54" y2="0" width="0.1524" layer="94" curve="-53"/>
<text x="-4.7244" y="4.6736" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
<symbol name="WB_DIODE_SCHOTTKY">
<pin name="A" x="7.62" y="0" length="short" direction="pas" rot="R180"/>
<pin name="K" x="-7.62" y="0" length="short" direction="pas"/>
<polygon width="0.1524" layer="94">
<vertex x="2.032" y="2.54"/>
<vertex x="-2.032" y="0"/>
<vertex x="2.032" y="-2.54"/>
</polygon>
<text x="-4.7244" y="-1.0414" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<polygon width="0.1524" layer="94">
<vertex x="2.032" y="2.54"/>
<vertex x="-2.032" y="0"/>
<vertex x="2.032" y="-2.54"/>
</polygon>
<text x="-4.7244" y="-1.0414" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<wire x1="-7.62" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.794" y1="2.032" x2="-2.794" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.794" y1="2.54" x2="-2.032" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="2.54" x2="-2.032" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-2.032" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="2.032" y="2.54"/>
<vertex x="-2.032" y="0"/>
<vertex x="2.032" y="-2.54"/>
</polygon>
<text x="-4.7244" y="-1.0414" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
<symbol name="WB_CAP_POLARIZED">
<pin name="+" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="-" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<wire x1="1.016" y1="0" x2="1.016" y2="3.302" width="0.1524" layer="94"/>
<wire x1="-1.651" y1="3.048" x2="-1.778" y2="-3.175" width="0.1524" layer="94" curve="-87"/>
<text x="-4.7244" y="6.5786" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<wire x1="-1.651" y1="3.048" x2="-1.778" y2="-3.175" width="0.1524" layer="94" curve="-87"/>
<text x="-4.7244" y="6.5786" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<wire x1="1.016" y1="-3.302" x2="1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="3.556" y1="-2.54" x2="3.556" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-1.651" y1="3.048" x2="-1.778" y2="-3.175" width="0.1524" layer="94" curve="-87"/>
<text x="-4.7244" y="6.5786" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
<symbol name="LM22680QMRE-ADJ/NOPB">
<pin name="VIN" x="-25.4" y="7.62" length="short" direction="pas"/>
<pin name="FB" x="0" y="15.24" length="short" direction="pas" rot="R270"/>
<pin name="BOOT" x="25.4" y="7.62" length="short" direction="pas" rot="R180"/>
<pin name="SW" x="25.4" y="-2.54" length="short" direction="pas" rot="R180"/>
<pin name="EN" x="15.24" y="-15.24" length="short" direction="pas" rot="R90"/>
<pin name="RT/SYNC" x="2.54" y="-15.24" length="short" direction="pas" rot="R90"/>
<pin name="GND_2" x="-17.78" y="-15.24" length="short" direction="pas" rot="R90"/>
<pin name="SS" x="-25.4" y="0" length="short" direction="pas"/>
<wire x1="-22.86" y1="12.7" x2="0" y2="12.7" width="0.1524" layer="94"/>
<wire x1="22.86" y1="12.7" x2="22.86" y2="7.62" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-2.54" x2="22.86" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-12.7" x2="15.24" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="2.54" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-12.7" x2="-22.86" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="-12.7" x2="-22.86" y2="0" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="7.62" x2="-22.86" y2="12.7" width="0.1524" layer="94"/>
<text x="-4.7244" y="-1.0414" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<text x="-4.7244" y="-1.0414" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<wire x1="0" y1="12.7" x2="22.86" y2="12.7" width="0.1524" layer="94"/>
<wire x1="22.86" y1="7.62" x2="22.86" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="-17.78" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="0" x2="-22.86" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="7.62" x2="-22.86" y2="7.62" width="0.1524" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="12.7" width="0.1524" layer="94"/>
<wire x1="25.4" y1="7.62" x2="22.86" y2="7.62" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-2.54" x2="22.86" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="15.24" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="2.54" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-15.24" x2="-17.78" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="0" x2="-22.86" y2="0" width="0.1524" layer="94"/>
<text x="-4.7244" y="-1.0414" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="-1.0414" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
<symbol name="MODULE_SOCKET_1">
<pin name="A5" x="-10.16" y="-2.54" length="middle"/>
<pin name="A4" x="-10.16" y="2.54" length="middle"/>
<pin name="A3" x="-10.16" y="7.62" length="middle"/>
<pin name="A2" x="-10.16" y="12.7" length="middle"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="19.2679" width="0.254" layer="94"/>
<wire x1="7.62" y1="19.2679" x2="4.0279" y2="22.86" width="0.254" layer="94"/>
<wire x1="4.0279" y1="22.86" x2="-1.4879" y2="22.86" width="0.254" layer="94"/>
<wire x1="-1.4879" y1="22.86" x2="-5.08" y2="19.2679" width="0.254" layer="94"/>
<wire x1="-5.08" y1="19.2679" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<pin name="A1" x="-10.16" y="17.78" length="middle"/>
<pin name="B1" x="12.7" y="17.78" length="middle" rot="R180"/>
<pin name="B2" x="12.7" y="12.7" length="middle" rot="R180"/>
<pin name="B3" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="B4" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="B5" x="12.7" y="-2.54" length="middle" rot="R180"/>
<text x="-2.54" y="25.4" size="1.778" layer="94">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.778" layer="94">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CGA6M3X7S2A475K200AB">
<gates>
<gate name="A" symbol="WB_CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1210_220">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CAP" value="4.7E6" constant="no"/>
<attribute name="DATASHEETURL" value="httpsproduct.tdk.comensearchcapacitorceramicmlccinfo?part%5Fno%3DCGA6M3X7S2A475K200AB" constant="no"/>
<attribute name="ESR" value="0.004236" constant="no"/>
<attribute name="IRMS" value="3.57337" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TDK" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CGA6M3X7S2A475K200AB" constant="no"/>
<attribute name="VDC" value="100.0" constant="no"/>
<attribute name="VENDOR" value="TDK" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C3216X7R2A105M160AA">
<gates>
<gate name="A" symbol="WB_CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C3216X7R2A105M160AA">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CAP" value="1.0E6" constant="no"/>
<attribute name="DATASHEETURL" value="httpsproduct.tdk.cominfoencatalogdatasheetsmlcc%5Fcommercial%5Fmidvoltage%5Fen.pdf" constant="no"/>
<attribute name="ESR" value="0.0075" constant="no"/>
<attribute name="IRMS" value="5.923488" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TDK" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="C3216X7R2A105M160AA" constant="no"/>
<attribute name="VDC" value="100.0" constant="no"/>
<attribute name="VENDOR" value="TDK" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CC1206KRX7R9BB393">
<gates>
<gate name="A" symbol="WB_CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CC1206KRX7R9BB393">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CAP" value="3.9E8" constant="no"/>
<attribute name="ESR" value="0.0" constant="no"/>
<attribute name="IRMS" value="0.0" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Yageo America" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CC1206KRX7R9BB393" constant="no"/>
<attribute name="VDC" value="50.0" constant="no"/>
<attribute name="VENDOR" value="Yageo America" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRCW120663K4FKEA">
<gates>
<gate name="A" symbol="WB_RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CRCW120663K4FKEA">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_NAME" value="VishayDale" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CRCW120663K4FKEA" constant="no"/>
<attribute name="POWER" value="0.25" constant="no"/>
<attribute name="RESISTANCE" value="63400.0" constant="no"/>
<attribute name="TOLERANCE" value="1.0" constant="no"/>
<attribute name="VENDOR" value="VishayDale" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRCW12061K00FKEA">
<gates>
<gate name="A" symbol="WB_RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CRCW12061K00FKEA">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_NAME" value="VishayDale" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CRCW12061K00FKEA" constant="no"/>
<attribute name="POWER" value="0.25" constant="no"/>
<attribute name="RESISTANCE" value="1000.0" constant="no"/>
<attribute name="TOLERANCE" value="1.0" constant="no"/>
<attribute name="VENDOR" value="VishayDale" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRCW12062K94FKEA">
<gates>
<gate name="A" symbol="WB_RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CRCW12062K94FKEA">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_NAME" value="VishayDale" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CRCW12062K94FKEA" constant="no"/>
<attribute name="POWER" value="0.25" constant="no"/>
<attribute name="RESISTANCE" value="2940.0" constant="no"/>
<attribute name="TOLERANCE" value="1.0" constant="no"/>
<attribute name="VENDOR" value="VishayDale" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="12065C103KAT2A">
<gates>
<gate name="A" symbol="WB_CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1206">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CAP" value="1.0E8" constant="no"/>
<attribute name="ESR" value="0.119" constant="no"/>
<attribute name="IRMS" value="0.0" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="AVX" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="12065C103KAT2A" constant="no"/>
<attribute name="VDC" value="50.0" constant="no"/>
<attribute name="VENDOR" value="AVX" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SRN8040-330M">
<gates>
<gate name="A" symbol="WB_INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SRN8040">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEETURL" value="httpwww.bourns.comdataglobalpdfsSRN8040.pdf" constant="no"/>
<attribute name="DCR" value="0.145" constant="no"/>
<attribute name="IDC" value="1.7" constant="no"/>
<attribute name="L" value="3.3E5" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Bourns" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="SRN8040330M" constant="no"/>
<attribute name="VENDOR" value="Bourns" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SK220ATR">
<gates>
<gate name="A" symbol="WB_DIODE_SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMA">
<connects>
<connect gate="A" pin="A" pad="2"/>
<connect gate="A" pin="K" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEETURL" value="httpwww.smc%2Ddiodes.compropdfSK220A%2520N0942%2520REV.A.pdf" constant="no"/>
<attribute name="IO" value="2.0" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="SMC Diode Solutions" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="SK220ATR" constant="no"/>
<attribute name="VENDOR" value="SMC Diode Solutions" constant="no"/>
<attribute name="VFATIO" value="0.9" constant="no"/>
<attribute name="VRRM" value="200.0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPSB476K010R0250">
<gates>
<gate name="A" symbol="WB_CAP_POLARIZED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="3528-21">
<connects>
<connect gate="A" pin="+" pad="1"/>
<connect gate="A" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CAP" value="4.7E5" constant="no"/>
<attribute name="ESR" value="0.25" constant="no"/>
<attribute name="IRMS" value="0.525" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="AVX" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="TPSB476K010R0250" constant="no"/>
<attribute name="VDC" value="10.0" constant="no"/>
<attribute name="VENDOR" value="AVX" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LM22680QMRE-ADJ/NOPB">
<gates>
<gate name="A" symbol="LM22680QMRE-ADJ/NOPB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MRA08B">
<connects>
<connect gate="A" pin="BOOT" pad="1"/>
<connect gate="A" pin="EN" pad="5"/>
<connect gate="A" pin="FB" pad="4"/>
<connect gate="A" pin="GND_2" pad="6 9"/>
<connect gate="A" pin="RT/SYNC" pad="3"/>
<connect gate="A" pin="SS" pad="2"/>
<connect gate="A" pin="SW" pad="8"/>
<connect gate="A" pin="VIN" pad="7"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEETURL" value="httpwww.ti.comproductlm22680q1" constant="no"/>
<attribute name="FREQUENCY" value="1000000.0" constant="no"/>
<attribute name="IMAX" value="2.0" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="LM22680QMREADJNOPB" constant="no"/>
<attribute name="MKTG_PACKAGE" value="NA" constant="no"/>
<attribute name="NSID" value="" constant="no"/>
<attribute name="PACKAGE" value="NA" constant="no"/>
<attribute name="VENDOR" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MODULE_1">
<gates>
<gate name="G$1" symbol="MODULE_SOCKET_1" x="5.08" y="5.08"/>
</gates>
<devices>
<device name="" package="MODULE_1">
<connects>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="B1" pad="B1"/>
<connect gate="G$1" pin="B2" pad="B2"/>
<connect gate="G$1" pin="B3" pad="B3"/>
<connect gate="G$1" pin="B4" pad="B4"/>
<connect gate="G$1" pin="B5" pad="B5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOLENOID_THIN" package="SOLENOID_THIN">
<connects>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="B1" pad="B1"/>
<connect gate="G$1" pin="B2" pad="B2"/>
<connect gate="G$1" pin="B3" pad="B3"/>
<connect gate="G$1" pin="B4" pad="B4"/>
<connect gate="G$1" pin="B5" pad="B5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<description>&lt;h3&gt;Digital Ground Supply&lt;/h3&gt;</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-0.254" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;h3&gt;Ground Supply Symbol&lt;/h3&gt;
&lt;p&gt;Generic signal ground supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Common-Parts-Library-Eagle">
<packages>
<package name="DIOMELF3515N">
<wire x1="-0.9" y1="0.75" x2="0.9" y2="0.75" width="0.127" layer="21"/>
<wire x1="-0.9" y1="-0.75" x2="0.9" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-0.9" y1="0.75" x2="-1.2" y2="0.75" width="0.127" layer="51"/>
<wire x1="-1.2" y1="0.75" x2="-1.45" y2="0.5" width="0.127" layer="51" curve="90"/>
<wire x1="-0.9" y1="-0.75" x2="-1.2" y2="-0.75" width="0.127" layer="51"/>
<wire x1="-1.2" y1="-0.75" x2="-1.45" y2="-0.5" width="0.127" layer="51" curve="-90"/>
<wire x1="-1.45" y1="-0.5" x2="-1.45" y2="0.5" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.75" x2="1.2" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.2" y1="0.75" x2="1.45" y2="0.5" width="0.127" layer="51" curve="-90"/>
<wire x1="0.9" y1="-0.75" x2="1.2" y2="-0.75" width="0.127" layer="51"/>
<wire x1="1.2" y1="-0.75" x2="1.45" y2="-0.5" width="0.127" layer="51" curve="90"/>
<wire x1="1.45" y1="-0.5" x2="1.45" y2="0.5" width="0.127" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.75" y2="0.7" width="0.127" layer="51" curve="90"/>
<wire x1="-1.75" y1="0.7" x2="-1.75" y2="-0.7" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-0.7" x2="-1.7" y2="-0.75" width="0.127" layer="51" curve="90"/>
<wire x1="-1.7" y1="-0.75" x2="-1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.75" x2="-1.45" y2="-0.7" width="0.127" layer="51" curve="90"/>
<wire x1="-1.45" y1="-0.7" x2="-1.45" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="-1.5" y1="0.75" x2="-1.45" y2="0.7" width="0.127" layer="51" curve="-90"/>
<wire x1="-1.45" y1="0.7" x2="-1.45" y2="0.5" width="0.127" layer="51"/>
<wire x1="1.75" y1="0.7" x2="1.7" y2="0.75" width="0.127" layer="51" curve="90"/>
<wire x1="1.7" y1="0.75" x2="1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.75" x2="1.45" y2="0.7" width="0.127" layer="51" curve="90"/>
<wire x1="1.45" y1="0.7" x2="1.45" y2="0.5" width="0.127" layer="51"/>
<wire x1="1.75" y1="0.7" x2="1.75" y2="-0.7" width="0.127" layer="51"/>
<wire x1="1.75" y1="-0.7" x2="1.7" y2="-0.75" width="0.127" layer="51" curve="-90"/>
<wire x1="1.7" y1="-0.75" x2="1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.75" x2="1.45" y2="-0.7" width="0.127" layer="51" curve="-90"/>
<wire x1="1.45" y1="-0.7" x2="1.45" y2="-0.5" width="0.127" layer="51"/>
<rectangle x1="-0.8" y1="-0.75" x2="-0.35" y2="0.75" layer="21"/>
<wire x1="2.3" y1="1.15" x2="2.3" y2="0.95" width="0.05" layer="21"/>
<wire x1="2.2" y1="1.05" x2="2.4" y2="1.05" width="0.05" layer="21"/>
<wire x1="-2.5" y1="1.25" x2="-2.5" y2="-1.25" width="0.127" layer="39"/>
<wire x1="-2.5" y1="-1.25" x2="2.5" y2="-1.25" width="0.127" layer="39"/>
<wire x1="2.5" y1="-1.25" x2="2.5" y2="1.25" width="0.127" layer="39"/>
<wire x1="2.5" y1="1.25" x2="-2.5" y2="1.25" width="0.127" layer="39"/>
<text x="-2.6" y="1.4" size="0.8128" layer="25">&gt;NAME</text>
<text x="-2.6" y="-2.2" size="0.8128" layer="27">&gt;VALUE</text>
<smd name="A" x="1.75" y="0" dx="1" dy="1.75" layer="1" rot="R180"/>
<smd name="C" x="-1.75" y="0" dx="1" dy="1.75" layer="1" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="1N4148">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="-3.556" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-5.08" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<pin name="A" x="-7.62" y="0" length="short" direction="pas"/>
<pin name="C" x="7.62" y="0" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1N4148" prefix="D">
<description>SMD Diode Switching 53V-300mA</description>
<gates>
<gate name="G$1" symbol="1N4148" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIOMELF3515N">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER" value="JCET"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="CBST" library="TinkerElectric" deviceset="12065C103KAT2A" device="" value="10nF">
<attribute name="PARTNO" value="12061C103JAT2A"/>
</part>
<part name="CIN" library="TinkerElectric" deviceset="CGA6M3X7S2A475K200AB" device="" value="4.7uF">
<attribute name="PARTNO" value="CGA6M3X7S2A475K200AB"/>
</part>
<part name="CINX" library="TinkerElectric" deviceset="C3216X7R2A105M160AA" device="" value="1uF">
<attribute name="PARTNO" value="C3216X7R2A105M160AA"/>
</part>
<part name="COUT" library="TinkerElectric" deviceset="TPSB476K010R0250" device="" value="47uF">
<attribute name="PARTNO" value="TPSB476K010R0250"/>
</part>
<part name="D1" library="TinkerElectric" deviceset="SK220ATR" device="" value="900mV">
<attribute name="MOUSER" value="621-B280-13-F"/>
<attribute name="PARTNO" value="621-B280-13-F"/>
</part>
<part name="L1" library="TinkerElectric" deviceset="SRN8040-330M" device="" value="33uH">
<attribute name="PARTNO" value="SRN8040-330M"/>
</part>
<part name="RT" library="TinkerElectric" deviceset="CRCW120663K4FKEA" device="" value="63.4k">
<attribute name="PARTNO" value="CRCW120663K4FKEA"/>
</part>
<part name="CSS" library="TinkerElectric" deviceset="CC1206KRX7R9BB393" device="" value="39nF">
<attribute name="PARTNO" value="CC1206KRX7R9BB393"/>
</part>
<part name="U1" library="TinkerElectric" deviceset="LM22680QMRE-ADJ/NOPB" device="" value="LM22680QMRE-ADJ/NOPB">
<attribute name="PARTNO" value="MRA08B"/>
</part>
<part name="RFB1" library="TinkerElectric" deviceset="CRCW12061K00FKEA" device="" value="1k">
<attribute name="PARTNO" value="RMCF1206FT1K00"/>
</part>
<part name="RFB2" library="TinkerElectric" deviceset="CRCW12062K94FKEA" device="" value="2.94k">
<attribute name="PARTNO" value="CRCW12062K94FKEA"/>
</part>
<part name="CIN_2" library="TinkerElectric" deviceset="CGA6M3X7S2A475K200AB" device="" value="4.7uF">
<attribute name="PARTNO" value="CGA6M3X7S2A475K200AB"/>
</part>
<part name="CIN_3" library="TinkerElectric" deviceset="CGA6M3X7S2A475K200AB" device="" value="4.7uF">
<attribute name="PARTNO" value="CGA6M3X7S2A475K200AB"/>
</part>
<part name="GND1" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND2" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="D2" library="Common-Parts-Library-Eagle" deviceset="1N4148" device="">
<attribute name="PARTNO" value="1N4148W-7-F"/>
</part>
<part name="D3" library="Common-Parts-Library-Eagle" deviceset="1N4148" device="" value="SMF4L20A">
<attribute name="PARTNO" value="SMF4L20A"/>
</part>
<part name="U$1" library="TinkerElectric" deviceset="MODULE_1" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="53.34" y="210.82" size="1.778" layer="91">TODO: Make inductor match foot more as prone to rotation</text>
<text x="81.28" y="180.34" size="1.778" layer="91">Vout set by:
RFB2 = ((Vout/1.285)-1)*RFB1

if RFB1 = 1k
Vout = 3.3 means RFB2 = 1.5681k
Vout = 5 means RFB2 = 2.891k</text>
</plain>
<instances>
<instance part="CBST" gate="A" x="162.56" y="96.52" smashed="yes">
<attribute name="NAME" x="162.56" y="102.616" size="2" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="162.56" y="89.662" size="2" layer="96" align="bottom-center"/>
<attribute name="PARTNO" x="162.56" y="96.52" size="1.778" layer="96" display="off"/>
</instance>
<instance part="CIN" gate="A" x="35.56" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="38.608" y="55.118" size="2" layer="95"/>
<attribute name="VALUE" x="38.608" y="50.546" size="2" layer="96"/>
<attribute name="PARTNO" x="35.56" y="50.8" size="1.778" layer="96" display="off"/>
</instance>
<instance part="CINX" gate="A" x="83.82" y="71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="86.868" y="75.438" size="2" layer="95"/>
<attribute name="VALUE" x="86.868" y="70.866" size="2" layer="96"/>
<attribute name="PARTNO" x="83.82" y="71.12" size="1.778" layer="96" display="off"/>
</instance>
<instance part="COUT" gate="A" x="208.28" y="55.88" smashed="yes" rot="R90">
<attribute name="NAME" x="211.582" y="60.452" size="2" layer="95"/>
<attribute name="VALUE" x="211.582" y="55.88" size="2" layer="96"/>
<attribute name="PARTNO" x="208.28" y="55.88" size="1.778" layer="96" display="off"/>
</instance>
<instance part="D1" gate="A" x="170.18" y="68.58" smashed="yes" rot="R270">
<attribute name="NAME" x="172.72" y="72.39" size="2" layer="95"/>
<attribute name="VALUE" x="172.72" y="67.818" size="2" layer="96"/>
<attribute name="MOUSER" x="170.18" y="68.58" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PARTNO" x="170.18" y="68.58" size="1.778" layer="96" display="off"/>
</instance>
<instance part="L1" gate="A" x="195.58" y="86.36" smashed="yes">
<attribute name="NAME" x="195.58" y="90.678" size="2" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="195.58" y="81.28" size="2" layer="96" align="bottom-center"/>
<attribute name="PARTNO" x="195.58" y="86.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="RT" gate="A" x="129.54" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="131.064" y="56.134" size="2" layer="95"/>
<attribute name="VALUE" x="131.064" y="51.562" size="2" layer="96"/>
<attribute name="PARTNO" x="129.54" y="53.34" size="1.778" layer="96" display="off"/>
</instance>
<instance part="CSS" gate="A" x="96.52" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="99.568" y="70.358" size="2" layer="95"/>
<attribute name="VALUE" x="99.568" y="65.786" size="2" layer="96"/>
<attribute name="PARTNO" x="96.52" y="66.04" size="1.778" layer="96" display="off"/>
</instance>
<instance part="U1" gate="A" x="127" y="88.9" smashed="yes">
<attribute name="NAME" x="127" y="88.9" size="2" layer="95" align="bottom-center"/>
<attribute name="TYPE" x="127" y="85.09" size="2" layer="96" align="bottom-center"/>
<attribute name="PARTNO" x="127" y="88.9" size="1.778" layer="96" display="off"/>
</instance>
<instance part="RFB1" gate="A" x="190.5" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="190.5" y="154.432" size="2" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="190.5" y="144.526" size="2" layer="96" align="bottom-center"/>
<attribute name="PARTNO" x="190.5" y="149.86" size="1.778" layer="96" display="off"/>
</instance>
<instance part="RFB2" gate="A" x="175.26" y="114.3" smashed="yes">
<attribute name="NAME" x="175.26" y="118.872" size="2" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="175.26" y="108.966" size="2" layer="96" align="bottom-center"/>
<attribute name="PARTNO" x="175.26" y="114.3" size="1.778" layer="96" display="off"/>
</instance>
<instance part="CIN_2" gate="A" x="50.8" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="53.848" y="55.118" size="2" layer="95"/>
<attribute name="VALUE" x="53.848" y="50.546" size="2" layer="96"/>
<attribute name="PARTNO" x="50.8" y="50.8" size="1.778" layer="96" display="off"/>
</instance>
<instance part="CIN_3" gate="A" x="68.58" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="71.628" y="55.118" size="2" layer="95"/>
<attribute name="VALUE" x="71.628" y="50.546" size="2" layer="96"/>
<attribute name="PARTNO" x="68.58" y="50.8" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND1" gate="1" x="134.62" y="27.94"/>
<instance part="GND2" gate="1" x="210.82" y="129.54"/>
<instance part="D2" gate="G$1" x="-17.78" y="55.88">
<attribute name="PARTNO" x="-17.78" y="55.88" size="1.778" layer="96" display="off"/>
</instance>
<instance part="D3" gate="G$1" x="15.24" y="50.8" rot="R90">
<attribute name="PARTNO" x="15.24" y="50.8" size="1.778" layer="96" display="off"/>
</instance>
<instance part="U$1" gate="G$1" x="15.24" y="119.38"/>
</instances>
<busses>
</busses>
<nets>
<net name="4" class="0">
<segment>
<pinref part="CBST" gate="A" pin="1"/>
<wire x1="170.18" y1="96.52" x2="177.8" y2="96.52" width="0.1524" layer="91"/>
<pinref part="L1" gate="A" pin="1"/>
<pinref part="U1" gate="A" pin="SW"/>
<wire x1="187.96" y1="86.36" x2="177.8" y2="86.36" width="0.1524" layer="91"/>
<wire x1="170.18" y1="86.36" x2="152.4" y2="86.36" width="0.1524" layer="91"/>
<wire x1="177.8" y1="96.52" x2="177.8" y2="86.36" width="0.1524" layer="91"/>
<pinref part="D1" gate="A" pin="K"/>
<wire x1="177.8" y1="86.36" x2="170.18" y2="86.36" width="0.1524" layer="91"/>
<wire x1="170.18" y1="86.36" x2="170.18" y2="76.2" width="0.1524" layer="91"/>
<junction x="170.18" y="86.36"/>
<junction x="177.8" y="86.36"/>
<junction x="152.4" y="86.36"/>
</segment>
</net>
<net name="13" class="0">
<segment>
<pinref part="CBST" gate="A" pin="2"/>
<pinref part="U1" gate="A" pin="BOOT"/>
<wire x1="152.4" y1="96.52" x2="154.94" y2="96.52" width="0" layer="91"/>
<junction x="152.4" y="96.52"/>
<junction x="154.94" y="96.52"/>
</segment>
</net>
<net name="2" class="0">
<segment>
<wire x1="182.88" y1="149.86" x2="152.4" y2="149.86" width="0.1524" layer="91"/>
<wire x1="152.4" y1="149.86" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
<pinref part="RFB1" gate="A" pin="1"/>
<pinref part="RFB2" gate="A" pin="2"/>
<pinref part="U1" gate="A" pin="FB"/>
<wire x1="167.64" y1="114.3" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
<wire x1="152.4" y1="114.3" x2="127" y2="114.3" width="0.1524" layer="91"/>
<wire x1="127" y1="114.3" x2="127" y2="104.14" width="0.1524" layer="91"/>
<junction x="152.4" y="114.3"/>
<junction x="127" y="104.14"/>
</segment>
</net>
<net name="VOUT" class="0">
<segment>
<pinref part="L1" gate="A" pin="2"/>
<wire x1="203.2" y1="86.36" x2="208.28" y2="86.36" width="0.1524" layer="91"/>
<pinref part="COUT" gate="A" pin="+"/>
<wire x1="208.28" y1="86.36" x2="226.06" y2="86.36" width="0.1524" layer="91"/>
<wire x1="208.28" y1="63.5" x2="208.28" y2="86.36" width="0.1524" layer="91"/>
<pinref part="RFB2" gate="A" pin="1"/>
<wire x1="182.88" y1="114.3" x2="226.06" y2="114.3" width="0.1524" layer="91"/>
<wire x1="226.06" y1="114.3" x2="226.06" y2="86.36" width="0.1524" layer="91"/>
<wire x1="226.06" y1="86.36" x2="226.06" y2="63.5" width="0.1524" layer="91"/>
<junction x="226.06" y="86.36"/>
<junction x="208.28" y="86.36"/>
<wire x1="236.22" y1="63.5" x2="226.06" y2="63.5" width="0.1524" layer="91"/>
<label x="231.14" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="45.72" y1="116.84" x2="33.02" y2="116.84" width="0.1524" layer="91"/>
<label x="43.18" y="116.84" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="B5"/>
<pinref part="U$1" gate="G$1" pin="B4"/>
<wire x1="33.02" y1="116.84" x2="27.94" y2="116.84" width="0.1524" layer="91"/>
<wire x1="27.94" y1="121.92" x2="33.02" y2="121.92" width="0.1524" layer="91"/>
<wire x1="33.02" y1="121.92" x2="33.02" y2="116.84" width="0.1524" layer="91"/>
<junction x="33.02" y="116.84"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="CINX" gate="A" pin="1"/>
<pinref part="U1" gate="A" pin="VIN"/>
<wire x1="101.6" y1="96.52" x2="83.82" y2="96.52" width="0.1524" layer="91"/>
<wire x1="83.82" y1="96.52" x2="83.82" y2="86.36" width="0.1524" layer="91"/>
<pinref part="CIN" gate="A" pin="1"/>
<pinref part="CIN_3" gate="A" pin="1"/>
<pinref part="CIN_2" gate="A" pin="1"/>
<wire x1="83.82" y1="86.36" x2="83.82" y2="78.74" width="0.1524" layer="91"/>
<wire x1="50.8" y1="58.42" x2="68.58" y2="58.42" width="0.1524" layer="91"/>
<wire x1="50.8" y1="58.42" x2="35.56" y2="58.42" width="0.1524" layer="91"/>
<wire x1="83.82" y1="86.36" x2="35.56" y2="86.36" width="0.1524" layer="91"/>
<wire x1="35.56" y1="86.36" x2="35.56" y2="68.58" width="0.1524" layer="91"/>
<wire x1="35.56" y1="58.42" x2="35.56" y2="68.58" width="0.1524" layer="91"/>
<junction x="35.56" y="68.58"/>
<junction x="35.56" y="58.42"/>
<junction x="50.8" y="58.42"/>
<junction x="83.82" y="86.36"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="15.24" y1="58.42" x2="15.24" y2="60.96" width="0.1524" layer="91"/>
<wire x1="15.24" y1="60.96" x2="15.24" y2="68.58" width="0.1524" layer="91"/>
<wire x1="15.24" y1="68.58" x2="35.56" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="-10.16" y1="55.88" x2="5.08" y2="55.88" width="0.1524" layer="91"/>
<wire x1="5.08" y1="55.88" x2="5.08" y2="60.96" width="0.1524" layer="91"/>
<wire x1="5.08" y1="60.96" x2="15.24" y2="60.96" width="0.1524" layer="91"/>
<junction x="15.24" y="60.96"/>
<junction x="101.6" y="96.52"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U1" gate="A" pin="SS"/>
<pinref part="CSS" gate="A" pin="1"/>
<wire x1="101.6" y1="88.9" x2="96.52" y2="88.9" width="0.1524" layer="91"/>
<wire x1="96.52" y1="88.9" x2="96.52" y2="73.66" width="0.1524" layer="91"/>
<junction x="101.6" y="88.9"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="A" pin="RT/SYNC"/>
<pinref part="RT" gate="A" pin="1"/>
<wire x1="129.54" y1="73.66" x2="129.54" y2="60.96" width="0.1524" layer="91"/>
<junction x="129.54" y="73.66"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="A" pin="GND_2"/>
<pinref part="U1" gate="A" pin="GND_2"/>
<pinref part="CINX" gate="A" pin="2"/>
<wire x1="83.82" y1="63.5" x2="83.82" y2="35.56" width="0.1524" layer="91"/>
<pinref part="COUT" gate="A" pin="-"/>
<wire x1="226.06" y1="35.56" x2="208.28" y2="35.56" width="0.1524" layer="91"/>
<wire x1="208.28" y1="35.56" x2="208.28" y2="48.26" width="0.1524" layer="91"/>
<pinref part="RT" gate="A" pin="2"/>
<wire x1="208.28" y1="35.56" x2="170.18" y2="35.56" width="0.1524" layer="91"/>
<wire x1="170.18" y1="35.56" x2="134.62" y2="35.56" width="0.1524" layer="91"/>
<wire x1="134.62" y1="35.56" x2="129.54" y2="35.56" width="0.1524" layer="91"/>
<wire x1="129.54" y1="35.56" x2="129.54" y2="45.72" width="0.1524" layer="91"/>
<pinref part="CSS" gate="A" pin="2"/>
<wire x1="96.52" y1="58.42" x2="96.52" y2="35.56" width="0.1524" layer="91"/>
<wire x1="96.52" y1="35.56" x2="109.22" y2="35.56" width="0.1524" layer="91"/>
<wire x1="109.22" y1="35.56" x2="129.54" y2="35.56" width="0.1524" layer="91"/>
<wire x1="96.52" y1="35.56" x2="83.82" y2="35.56" width="0.1524" layer="91"/>
<pinref part="CIN_3" gate="A" pin="2"/>
<pinref part="CIN_2" gate="A" pin="2"/>
<wire x1="50.8" y1="43.18" x2="68.58" y2="43.18" width="0.1524" layer="91"/>
<pinref part="CIN" gate="A" pin="2"/>
<wire x1="35.56" y1="43.18" x2="50.8" y2="43.18" width="0.1524" layer="91"/>
<wire x1="83.82" y1="35.56" x2="35.56" y2="35.56" width="0.1524" layer="91"/>
<wire x1="35.56" y1="35.56" x2="35.56" y2="43.18" width="0.1524" layer="91"/>
<junction x="50.8" y="43.18"/>
<junction x="35.56" y="43.18"/>
<junction x="35.56" y="35.56"/>
<junction x="83.82" y="35.56"/>
<junction x="96.52" y="35.56"/>
<junction x="129.54" y="35.56"/>
<junction x="208.28" y="35.56"/>
<wire x1="134.62" y1="30.48" x2="134.62" y2="35.56" width="0.1524" layer="91"/>
<junction x="134.62" y="35.56"/>
<pinref part="D1" gate="A" pin="A"/>
<wire x1="170.18" y1="60.96" x2="170.18" y2="35.56" width="0.1524" layer="91"/>
<junction x="170.18" y="35.56"/>
<wire x1="109.22" y1="73.66" x2="109.22" y2="35.56" width="0.1524" layer="91"/>
<junction x="109.22" y="35.56"/>
<wire x1="236.22" y1="60.96" x2="226.06" y2="60.96" width="0.1524" layer="91"/>
<wire x1="226.06" y1="60.96" x2="226.06" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="35.56" y1="35.56" x2="15.24" y2="35.56" width="0.1524" layer="91"/>
<wire x1="15.24" y1="35.56" x2="15.24" y2="40.64" width="0.1524" layer="91"/>
<wire x1="15.24" y1="40.64" x2="15.24" y2="43.18" width="0.1524" layer="91"/>
<wire x1="15.24" y1="40.64" x2="-22.86" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="40.64" x2="-22.86" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="50.8" x2="-33.02" y2="50.8" width="0.1524" layer="91"/>
<junction x="15.24" y="40.64"/>
<junction x="109.22" y="73.66"/>
<label x="-43.18" y="50.8" size="1.778" layer="95"/>
<label x="231.14" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RFB1" gate="A" pin="2"/>
<wire x1="198.12" y1="149.86" x2="210.82" y2="149.86" width="0.1524" layer="91"/>
<wire x1="210.82" y1="149.86" x2="210.82" y2="132.08" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="5.08" y1="116.84" x2="-2.54" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="A5"/>
<pinref part="U$1" gate="G$1" pin="A4"/>
<wire x1="-2.54" y1="116.84" x2="-10.16" y2="116.84" width="0.1524" layer="91"/>
<wire x1="5.08" y1="121.92" x2="-2.54" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="121.92" x2="-2.54" y2="116.84" width="0.1524" layer="91"/>
<label x="-10.16" y="116.84" size="1.778" layer="95"/>
<junction x="-2.54" y="116.84"/>
</segment>
<segment>
<wire x1="45.72" y1="137.16" x2="33.02" y2="137.16" width="0.1524" layer="91"/>
<label x="40.64" y="137.16" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="B1"/>
<wire x1="33.02" y1="137.16" x2="27.94" y2="137.16" width="0.1524" layer="91"/>
<wire x1="33.02" y1="137.16" x2="33.02" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="B2"/>
<wire x1="33.02" y1="132.08" x2="27.94" y2="132.08" width="0.1524" layer="91"/>
<junction x="33.02" y="137.16"/>
</segment>
</net>
<net name="VIN_FIRST" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="-25.4" y1="55.88" x2="-27.94" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="55.88" x2="-27.94" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="53.34" x2="-33.02" y2="53.34" width="0.1524" layer="91"/>
<label x="-43.18" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="A1"/>
<wire x1="-5.08" y1="137.16" x2="-2.54" y2="137.16" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="A2"/>
<wire x1="-2.54" y1="137.16" x2="5.08" y2="137.16" width="0.1524" layer="91"/>
<wire x1="5.08" y1="132.08" x2="-2.54" y2="132.08" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="132.08" x2="-2.54" y2="137.16" width="0.1524" layer="91"/>
<label x="-10.16" y="137.16" size="1.778" layer="95"/>
<junction x="-2.54" y="137.16"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
