<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="b3D" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-RF">
<description>&lt;h3&gt;SparkFun RF, WiFi, Cellular, and Bluetooth&lt;/h3&gt;
In this library you'll find things that send or receive RF-- cellular modules, Bluetooth, WiFi, etc.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="XBEE-SILK">
<description>&lt;h3&gt;Digi XBee and XBee-PRO RF Modules (Silkscreen Dimension Indicators)&lt;/h3&gt;
&lt;p&gt;20-pin 2mm PTH headers. Silkscreen to indicate polarity and dimensions of XBee (non-Pro's)&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Wireless/Zigbee/XBee-Dimensional.pdf"&gt;XBee Mechanical Drawings&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.sparkfun.com/datasheets/Prototyping/XBee-Connector.pdf"&gt;Header Mechanical Drawing&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/products/8272"&gt;Header SparkFun Product Link&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-5" y1="27.6" x2="5" y2="27.6" width="0.127" layer="51"/>
<wire x1="-12.25" y1="21.25" x2="-5" y2="27.6" width="0.127" layer="51"/>
<wire x1="12.25" y1="21.25" x2="5" y2="27.6" width="0.127" layer="51"/>
<wire x1="9.75" y1="21.25" x2="12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="12.25" y1="21.25" x2="12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0.75" x2="9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="9.75" y1="21.25" x2="9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="21.25" x2="-12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-12.25" y2="0" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0.75" x2="12.25" y2="0" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0" x2="12.25" y2="0" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0" x2="12.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-12.25" y1="0" x2="-12.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-12.25" y1="-6.25" x2="12.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-5" y1="27.6" x2="-5.7" y2="27" width="0.2032" layer="21"/>
<wire x1="-5" y1="27.6" x2="5" y2="27.6" width="0.2032" layer="21"/>
<wire x1="5" y1="27.6" x2="5.7" y2="27" width="0.2032" layer="21"/>
<pad name="1" x="-11" y="20" drill="0.8" diameter="1.524"/>
<pad name="2" x="-11" y="18" drill="0.8" diameter="1.524"/>
<pad name="3" x="-11" y="16" drill="0.8" diameter="1.524"/>
<pad name="4" x="-11" y="14" drill="0.8" diameter="1.524"/>
<pad name="5" x="-11" y="12" drill="0.8" diameter="1.524"/>
<pad name="6" x="-11" y="10" drill="0.8" diameter="1.524"/>
<pad name="7" x="-11" y="8" drill="0.8" diameter="1.524"/>
<pad name="8" x="-11" y="6" drill="0.8" diameter="1.524"/>
<pad name="9" x="-11" y="4" drill="0.8" diameter="1.524"/>
<pad name="10" x="-11" y="2" drill="0.8" diameter="1.524"/>
<pad name="11" x="11" y="2" drill="0.8" diameter="1.524"/>
<pad name="12" x="11" y="4" drill="0.8" diameter="1.524"/>
<pad name="13" x="11" y="6" drill="0.8" diameter="1.524"/>
<pad name="14" x="11" y="8" drill="0.8" diameter="1.524"/>
<pad name="15" x="11" y="10" drill="0.8" diameter="1.524"/>
<pad name="16" x="11" y="12" drill="0.8" diameter="1.524"/>
<pad name="17" x="11" y="14" drill="0.8" diameter="1.524"/>
<pad name="18" x="11" y="16" drill="0.8" diameter="1.524"/>
<pad name="19" x="11" y="18" drill="0.8" diameter="1.524"/>
<pad name="20" x="11" y="20" drill="0.8" diameter="1.524"/>
<text x="0" y="9.16" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="8.62" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<text x="-9.5" y="20" size="1.016" layer="21" font="vector" ratio="15" align="center-left">1</text>
</package>
<package name="XBEE-SMD">
<description>&lt;h3&gt;Digi XBee and XBee-PRO RF Modules (SMD pads)&lt;/h3&gt;
&lt;p&gt;20-pin 2mm SMD headers.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Wireless/Zigbee/XBee-Dimensional.pdf"&gt;XBee Mechanical Drawings&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Prototyping/Sockets/xbeeheadersmd2.pdf"&gt;Header Mechanical Drawing&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/products/10030"&gt;Header SparkFun Product Link&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-5" y1="27.6" x2="5" y2="27.6" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="21.25" x2="-5" y2="27.6" width="0.2032" layer="21"/>
<wire x1="12.25" y1="21.25" x2="5" y2="27.6" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="1.3" x2="-12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-12.25" y2="0" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0.75" x2="12.25" y2="0" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0" x2="12.25" y2="0" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0" x2="12.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-12.25" y1="0" x2="-12.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-12.25" y1="-6.25" x2="12.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-12.25" y1="0.75" x2="-12.25" y2="21.25" width="0.127" layer="51"/>
<wire x1="-9.75" y1="21.25" x2="-9.75" y2="0.75" width="0.127" layer="51"/>
<wire x1="-12.25" y1="0.75" x2="-9.75" y2="0.75" width="0.127" layer="51"/>
<wire x1="-12.25" y1="21.25" x2="-9.75" y2="21.25" width="0.127" layer="51"/>
<wire x1="-12.25" y1="0.75" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="0.75" x2="-9.75" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="20.6" x2="-9.75" y2="21.25" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="21.25" x2="-12.25" y2="20.6" width="0.2032" layer="21"/>
<wire x1="9.75" y1="0.75" x2="9.75" y2="21.25" width="0.127" layer="51"/>
<wire x1="12.25" y1="21.25" x2="12.25" y2="0.75" width="0.127" layer="51"/>
<wire x1="9.75" y1="0.75" x2="12.25" y2="0.75" width="0.127" layer="51"/>
<wire x1="9.75" y1="21.25" x2="12.25" y2="21.25" width="0.127" layer="51"/>
<wire x1="9.75" y1="0.75" x2="9.75" y2="1.3" width="0.2032" layer="21"/>
<wire x1="9.75" y1="0.75" x2="12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0.75" x2="12.25" y2="1.3" width="0.2032" layer="21"/>
<wire x1="12.25" y1="20.6" x2="12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="12.25" y1="21.25" x2="9.75" y2="21.25" width="0.2032" layer="21"/>
<wire x1="9.75" y1="21.25" x2="9.75" y2="20.6" width="0.2032" layer="21"/>
<smd name="1" x="-12.5" y="20" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="2" x="-9.5" y="18" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="3" x="-12.5" y="16" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="4" x="-9.5" y="14" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="5" x="-12.5" y="12" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="6" x="-9.5" y="10" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="7" x="-12.5" y="8" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="8" x="-9.5" y="6" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="9" x="-12.5" y="4" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="10" x="-9.5" y="2" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="11" x="12.5" y="2" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="12" x="9.5" y="4" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="13" x="12.5" y="6" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="14" x="9.5" y="8" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="15" x="12.5" y="10" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="16" x="9.5" y="12" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="17" x="12.5" y="14" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="18" x="9.5" y="16" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="19" x="12.5" y="18" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="20" x="9.5" y="20" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<text x="0" y="9.16" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="8.62" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<text x="-9.5" y="20" size="1.016" layer="21" font="vector" ratio="15" align="center-left">1</text>
</package>
<package name="XBEE-LONGPAD">
<description>&lt;h3&gt;Digi XBee and XBee-PRO RF Modules (Long pads)&lt;/h3&gt;
&lt;p&gt;20-pin 2mm PTH headers. Elongated annular rings to make soldering easier.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Wireless/Zigbee/XBee-Dimensional.pdf"&gt;XBee Mechanical Drawings&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.sparkfun.com/datasheets/Prototyping/XBee-Connector.pdf"&gt;Header Mechanical Drawing&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/products/8272"&gt;Header SparkFun Product Link&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-5" y1="27.6" x2="5" y2="27.6" width="0.127" layer="51"/>
<wire x1="-12.25" y1="21.25" x2="-5" y2="27.6" width="0.127" layer="51"/>
<wire x1="12.25" y1="21.25" x2="5" y2="27.6" width="0.127" layer="51"/>
<wire x1="9.5595" y1="21.3135" x2="12.4405" y2="21.25" width="0.2032" layer="21"/>
<wire x1="12.4405" y1="21.25" x2="12.4405" y2="0.75" width="0.2032" layer="21"/>
<wire x1="12.4405" y1="0.75" x2="9.5595" y2="0.8135" width="0.2032" layer="21"/>
<wire x1="9.5595" y1="21.3135" x2="9.5595" y2="0.8135" width="0.2032" layer="21"/>
<wire x1="-9.5595" y1="21.25" x2="-12.4405" y2="21.25" width="0.2032" layer="21"/>
<wire x1="-12.4405" y1="21.25" x2="-12.4405" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.4405" y1="0.75" x2="-9.5595" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.5595" y1="21.25" x2="-9.5595" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-12.25" y2="0" width="0.127" layer="51"/>
<wire x1="12.25" y1="0.75" x2="12.25" y2="0" width="0.127" layer="51"/>
<wire x1="-12.25" y1="0" x2="12.25" y2="0" width="0.127" layer="51"/>
<wire x1="12.25" y1="0" x2="12.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-12.25" y1="0" x2="-12.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-12.25" y1="-6.25" x2="12.25" y2="-6.25" width="0.127" layer="51"/>
<pad name="1" x="-11" y="20" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="2" x="-11" y="18" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="3" x="-11" y="16" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="4" x="-11" y="14" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="5" x="-11" y="12" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="6" x="-11" y="10" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="7" x="-11" y="8" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="8" x="-11" y="6" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="9" x="-11" y="4" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="10" x="-11" y="2" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="11" x="11" y="2" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="12" x="11" y="4" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="13" x="11" y="6" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="14" x="11" y="8" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="15" x="11" y="10" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="16" x="11" y="12" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="17" x="11" y="14" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="18" x="11" y="16" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="19" x="11" y="18" drill="0.8" diameter="1.2065" shape="long"/>
<pad name="20" x="11" y="20" drill="0.8" diameter="1.2065" shape="long"/>
<text x="0" y="9.16" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="8.62" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<text x="-9.373" y="20" size="1.016" layer="21" font="vector" ratio="15" align="center-left">1</text>
</package>
<package name="XBEE-LOCK">
<description>&lt;h3&gt;Digi XBee and XBee-PRO RF Modules (Locking footprint)&lt;/h3&gt;
&lt;p&gt;20-pin 2mm PTH headers - offset to "lock" headers in place for soldering.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Wireless/Zigbee/XBee-Dimensional.pdf"&gt;XBee Mechanical Drawings&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.sparkfun.com/datasheets/Prototyping/XBee-Connector.pdf"&gt;Header Mechanical Drawing&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/products/8272"&gt;Header SparkFun Product Link&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-5" y1="27.6" x2="5" y2="27.6" width="0.127" layer="51"/>
<wire x1="-12.25" y1="21.25" x2="-5" y2="27.6" width="0.127" layer="51"/>
<wire x1="12.25" y1="21.25" x2="5" y2="27.6" width="0.127" layer="51"/>
<wire x1="9.75" y1="21.25" x2="12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="12.25" y1="21.25" x2="12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0.75" x2="9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="9.75" y1="21.25" x2="9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="21.25" x2="-12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-12.25" y2="0" width="0.127" layer="51"/>
<wire x1="12.25" y1="0.75" x2="12.25" y2="0" width="0.127" layer="51"/>
<wire x1="-12.25" y1="0" x2="12.25" y2="0" width="0.127" layer="51"/>
<wire x1="12.25" y1="0" x2="12.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-12.25" y1="0" x2="-12.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-12.25" y1="-6.25" x2="12.25" y2="-6.25" width="0.127" layer="51"/>
<rectangle x1="-11.0998" y1="1.7272" x2="-10.8966" y2="2.2352" layer="51"/>
<rectangle x1="-11.0998" y1="3.7338" x2="-10.8966" y2="4.2418" layer="51"/>
<rectangle x1="-11.0998" y1="5.715" x2="-10.8966" y2="6.223" layer="51"/>
<rectangle x1="-11.0998" y1="7.7216" x2="-10.8966" y2="8.2296" layer="51"/>
<rectangle x1="-11.0998" y1="9.7282" x2="-10.8966" y2="10.2362" layer="51"/>
<rectangle x1="-11.0998" y1="11.7602" x2="-10.8966" y2="12.2682" layer="51"/>
<rectangle x1="-11.0998" y1="13.7414" x2="-10.8966" y2="14.2494" layer="51"/>
<rectangle x1="-11.0998" y1="15.7226" x2="-10.8966" y2="16.2306" layer="51"/>
<rectangle x1="-11.0998" y1="17.7292" x2="-10.8966" y2="18.2372" layer="51"/>
<rectangle x1="-11.0998" y1="19.7358" x2="-10.8966" y2="20.2438" layer="51"/>
<rectangle x1="10.8966" y1="1.7272" x2="11.0998" y2="2.2352" layer="51"/>
<rectangle x1="10.8966" y1="3.7338" x2="11.0998" y2="4.2418" layer="51"/>
<rectangle x1="10.8966" y1="5.715" x2="11.0998" y2="6.223" layer="51"/>
<rectangle x1="10.8966" y1="7.7216" x2="11.0998" y2="8.2296" layer="51"/>
<rectangle x1="10.8966" y1="9.7282" x2="11.0998" y2="10.2362" layer="51"/>
<rectangle x1="10.8966" y1="11.7602" x2="11.0998" y2="12.2682" layer="51"/>
<rectangle x1="10.8966" y1="13.7414" x2="11.0998" y2="14.2494" layer="51"/>
<rectangle x1="10.8966" y1="15.7226" x2="11.0998" y2="16.2306" layer="51"/>
<rectangle x1="10.8966" y1="17.7292" x2="11.0998" y2="18.2372" layer="51"/>
<rectangle x1="10.8966" y1="19.7358" x2="11.0998" y2="20.2438" layer="51"/>
<pad name="1" x="-11.1778" y="20" drill="0.7366" diameter="1.524"/>
<pad name="2" x="-10.8222" y="18" drill="0.7366" diameter="1.524"/>
<pad name="3" x="-11.1778" y="16" drill="0.7366" diameter="1.524"/>
<pad name="4" x="-10.8222" y="14" drill="0.7366" diameter="1.524"/>
<pad name="5" x="-11.1778" y="12" drill="0.7366" diameter="1.524"/>
<pad name="6" x="-10.8222" y="10" drill="0.7366" diameter="1.524"/>
<pad name="7" x="-11.1778" y="8" drill="0.7366" diameter="1.524"/>
<pad name="8" x="-10.8222" y="6" drill="0.7366" diameter="1.524"/>
<pad name="9" x="-11.1778" y="4" drill="0.7366" diameter="1.524"/>
<pad name="10" x="-10.8222" y="2" drill="0.7366" diameter="1.524"/>
<pad name="11" x="11.1778" y="2" drill="0.7366" diameter="1.524"/>
<pad name="12" x="10.8222" y="4" drill="0.7366" diameter="1.524"/>
<pad name="13" x="11.1778" y="6" drill="0.7366" diameter="1.524"/>
<pad name="14" x="10.8222" y="8" drill="0.7366" diameter="1.524"/>
<pad name="15" x="11.1778" y="10" drill="0.7366" diameter="1.524"/>
<pad name="16" x="10.8222" y="12" drill="0.7366" diameter="1.524"/>
<pad name="17" x="11.1778" y="14" drill="0.7366" diameter="1.524"/>
<pad name="18" x="10.8222" y="16" drill="0.7366" diameter="1.524"/>
<pad name="19" x="11.1778" y="18" drill="0.7366" diameter="1.524"/>
<pad name="20" x="10.8222" y="20" drill="0.7366" diameter="1.524"/>
<text x="0" y="9.16" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="8.62" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<text x="-9.5" y="20" size="1.016" layer="21" font="vector" ratio="15" align="center-left">1</text>
</package>
<package name="XBEE">
<description>&lt;h3&gt;Digi XBee and XBee-PRO RF Modules&lt;/h3&gt;
&lt;p&gt;20-pin 2mm PTH headers.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Wireless/Zigbee/XBee-Dimensional.pdf"&gt;XBee Mechanical Drawings&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.sparkfun.com/datasheets/Prototyping/XBee-Connector.pdf"&gt;Header Mechanical Drawing&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/products/8272"&gt;Header SparkFun Product Link&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-5" y1="27.6" x2="5" y2="27.6" width="0.127" layer="51"/>
<wire x1="-12.25" y1="21.25" x2="-5" y2="27.6" width="0.127" layer="51"/>
<wire x1="12.25" y1="21.25" x2="5" y2="27.6" width="0.127" layer="51"/>
<wire x1="9.75" y1="21.25" x2="12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="12.25" y1="21.25" x2="12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0.75" x2="9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="9.75" y1="21.25" x2="9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="21.25" x2="-12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-12.25" y2="0" width="0.127" layer="51"/>
<wire x1="12.25" y1="0.75" x2="12.25" y2="0" width="0.127" layer="51"/>
<wire x1="-12.25" y1="0" x2="12.25" y2="0" width="0.127" layer="51"/>
<wire x1="12.25" y1="0" x2="12.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-12.25" y1="0" x2="-12.25" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-12.25" y1="-6.25" x2="12.25" y2="-6.25" width="0.127" layer="51"/>
<pad name="1" x="-11" y="20" drill="0.8" diameter="1.524"/>
<pad name="2" x="-11" y="18" drill="0.8" diameter="1.524" rot="R180"/>
<pad name="3" x="-11" y="16" drill="0.8" diameter="1.524"/>
<pad name="4" x="-11" y="14" drill="0.8" diameter="1.524"/>
<pad name="5" x="-11" y="12" drill="0.8" diameter="1.524"/>
<pad name="6" x="-11" y="10" drill="0.8" diameter="1.524"/>
<pad name="7" x="-11" y="8" drill="0.8" diameter="1.524"/>
<pad name="8" x="-11" y="6" drill="0.8" diameter="1.524"/>
<pad name="9" x="-11" y="4" drill="0.8" diameter="1.524"/>
<pad name="10" x="-11" y="2" drill="0.8" diameter="1.524"/>
<pad name="11" x="11" y="2" drill="0.8" diameter="1.524"/>
<pad name="12" x="11" y="4" drill="0.8" diameter="1.524"/>
<pad name="13" x="11" y="6" drill="0.8" diameter="1.524"/>
<pad name="14" x="11" y="8" drill="0.8" diameter="1.524"/>
<pad name="15" x="11" y="10" drill="0.8" diameter="1.524"/>
<pad name="16" x="11" y="12" drill="0.8" diameter="1.524"/>
<pad name="17" x="11" y="14" drill="0.8" diameter="1.524"/>
<pad name="18" x="11" y="16" drill="0.8" diameter="1.524"/>
<pad name="19" x="11" y="18" drill="0.8" diameter="1.524"/>
<pad name="20" x="11" y="20" drill="0.8" diameter="1.524"/>
<text x="0" y="9.16" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="8.62" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<text x="-9.5" y="20" size="1.016" layer="21" font="vector" ratio="15" align="center-left">1</text>
</package>
</packages>
<symbols>
<symbol name="XBEE">
<description>&lt;h3&gt;Digi XBee and XBee Pro&lt;/h3&gt;
&lt;p&gt;XBees are tiny blue chips that can communicate wirelessly with each other. They can do simple things, like replacing a couple of wires in serial communication, which is nice when you want to make a remote for your paintball vehicle.&lt;/p&gt;</description>
<wire x1="-15.24" y1="12.7" x2="15.24" y2="12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="12.7" x2="15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="-15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-15.24" x2="-15.24" y2="12.7" width="0.254" layer="94"/>
<pin name="CTS" x="17.78" y="-10.16" length="short" direction="out" function="dot" rot="R180"/>
<pin name="DIN" x="-17.78" y="5.08" length="short" direction="in"/>
<pin name="DIO0" x="17.78" y="10.16" length="short" rot="R180"/>
<pin name="DIO1" x="17.78" y="7.62" length="short" rot="R180"/>
<pin name="DIO2" x="17.78" y="5.08" length="short" rot="R180"/>
<pin name="DIO3" x="17.78" y="2.54" length="short" rot="R180"/>
<pin name="DIO4" x="17.78" y="-12.7" length="short" rot="R180"/>
<pin name="DIO5" x="17.78" y="-2.54" length="short" rot="R180"/>
<pin name="DIO9" x="17.78" y="-7.62" length="short" rot="R180"/>
<pin name="DIO11" x="-17.78" y="-5.08" length="short"/>
<pin name="DIO12" x="-17.78" y="2.54" length="short"/>
<pin name="DOUT" x="-17.78" y="7.62" length="short" direction="out"/>
<pin name="DTR" x="-17.78" y="-10.16" length="short" function="dot"/>
<pin name="GND" x="-17.78" y="-12.7" length="short" direction="pwr"/>
<pin name="RES@8" x="-17.78" y="-7.62" length="short"/>
<pin name="RES@14" x="17.78" y="-5.08" length="short" rot="R180"/>
<pin name="RESET" x="-17.78" y="0" length="short" direction="in" function="dot"/>
<pin name="RSSI" x="-17.78" y="-2.54" length="short"/>
<pin name="RTS" x="17.78" y="0" length="short" direction="in" function="dot" rot="R180"/>
<pin name="VDD" x="-17.78" y="10.16" length="short" direction="pwr"/>
<text x="-15.24" y="12.954" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-15.24" y="-15.494" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="XBEE" prefix="JP" uservalue="yes">
<description>&lt;h3&gt;Digi XBee and XBee Pro&lt;/h3&gt;
&lt;p&gt;XBees are tiny blue chips that can communicate wirelessly with each other. They can do simple things, like replacing a couple of wires in serial communication, which is nice when you want to make a remote for your paintball vehicle.&lt;/p&gt;
&lt;h4&gt;SparkFun Products&lt;/h4&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/pages/xbee_guide"&gt;SparkFun XBee Buying Guide&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11812"&gt;SparkFun XBee Explorer USB&lt;/a&gt; (WRL-11812)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8276"&gt;Breakout Board for XBee Module&lt;/a&gt; (WRL-8276)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11373"&gt;SparkFun XBee Explorer Regulated&lt;/a&gt; (WRL-11373)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11697"&gt;SparkFun XBee Explorer Dongle&lt;/a&gt; (WRL-11697)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12847"&gt;SparkFun XBee Shield&lt;/a&gt; (WRL-12847)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13225"&gt;SparkFun XBee Explorer Serial&lt;/a&gt; (WRL-13225)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="XBEE" x="0" y="0"/>
</gates>
<devices>
<device name="SILK" package="XBEE-SILK">
<connects>
<connect gate="G$1" pin="CTS" pad="12"/>
<connect gate="G$1" pin="DIN" pad="3"/>
<connect gate="G$1" pin="DIO0" pad="20"/>
<connect gate="G$1" pin="DIO1" pad="19"/>
<connect gate="G$1" pin="DIO11" pad="7"/>
<connect gate="G$1" pin="DIO12" pad="4"/>
<connect gate="G$1" pin="DIO2" pad="18"/>
<connect gate="G$1" pin="DIO3" pad="17"/>
<connect gate="G$1" pin="DIO4" pad="11"/>
<connect gate="G$1" pin="DIO5" pad="15"/>
<connect gate="G$1" pin="DIO9" pad="13"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="DTR" pad="9"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="RES@14" pad="14"/>
<connect gate="G$1" pin="RES@8" pad="8"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="RSSI" pad="6"/>
<connect gate="G$1" pin="RTS" pad="16"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="XBEE-SMD">
<connects>
<connect gate="G$1" pin="CTS" pad="12"/>
<connect gate="G$1" pin="DIN" pad="3"/>
<connect gate="G$1" pin="DIO0" pad="20"/>
<connect gate="G$1" pin="DIO1" pad="19"/>
<connect gate="G$1" pin="DIO11" pad="7"/>
<connect gate="G$1" pin="DIO12" pad="4"/>
<connect gate="G$1" pin="DIO2" pad="18"/>
<connect gate="G$1" pin="DIO3" pad="17"/>
<connect gate="G$1" pin="DIO4" pad="11"/>
<connect gate="G$1" pin="DIO5" pad="15"/>
<connect gate="G$1" pin="DIO9" pad="13"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="DTR" pad="9"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="RES@14" pad="14"/>
<connect gate="G$1" pin="RES@8" pad="8"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="RSSI" pad="6"/>
<connect gate="G$1" pin="RTS" pad="16"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09042" constant="no"/>
</technology>
</technologies>
</device>
<device name="LONGPAD" package="XBEE-LONGPAD">
<connects>
<connect gate="G$1" pin="CTS" pad="12"/>
<connect gate="G$1" pin="DIN" pad="3"/>
<connect gate="G$1" pin="DIO0" pad="20"/>
<connect gate="G$1" pin="DIO1" pad="19"/>
<connect gate="G$1" pin="DIO11" pad="7"/>
<connect gate="G$1" pin="DIO12" pad="4"/>
<connect gate="G$1" pin="DIO2" pad="18"/>
<connect gate="G$1" pin="DIO3" pad="17"/>
<connect gate="G$1" pin="DIO4" pad="11"/>
<connect gate="G$1" pin="DIO5" pad="15"/>
<connect gate="G$1" pin="DIO9" pad="13"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="DTR" pad="9"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="RES@14" pad="14"/>
<connect gate="G$1" pin="RES@8" pad="8"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="RSSI" pad="6"/>
<connect gate="G$1" pin="RTS" pad="16"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="XBEE-LOCK">
<connects>
<connect gate="G$1" pin="CTS" pad="12"/>
<connect gate="G$1" pin="DIN" pad="3"/>
<connect gate="G$1" pin="DIO0" pad="20"/>
<connect gate="G$1" pin="DIO1" pad="19"/>
<connect gate="G$1" pin="DIO11" pad="7"/>
<connect gate="G$1" pin="DIO12" pad="4"/>
<connect gate="G$1" pin="DIO2" pad="18"/>
<connect gate="G$1" pin="DIO3" pad="17"/>
<connect gate="G$1" pin="DIO4" pad="11"/>
<connect gate="G$1" pin="DIO5" pad="15"/>
<connect gate="G$1" pin="DIO9" pad="13"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="DTR" pad="9"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="RES@14" pad="14"/>
<connect gate="G$1" pin="RES@8" pad="8"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="RSSI" pad="6"/>
<connect gate="G$1" pin="RTS" pad="16"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="XBEE">
<connects>
<connect gate="G$1" pin="CTS" pad="12"/>
<connect gate="G$1" pin="DIN" pad="3"/>
<connect gate="G$1" pin="DIO0" pad="20"/>
<connect gate="G$1" pin="DIO1" pad="19"/>
<connect gate="G$1" pin="DIO11" pad="7"/>
<connect gate="G$1" pin="DIO12" pad="4"/>
<connect gate="G$1" pin="DIO2" pad="18"/>
<connect gate="G$1" pin="DIO3" pad="17"/>
<connect gate="G$1" pin="DIO4" pad="11"/>
<connect gate="G$1" pin="DIO5" pad="15"/>
<connect gate="G$1" pin="DIO9" pad="13"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="DTR" pad="9"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="RES@14" pad="14"/>
<connect gate="G$1" pin="RES@8" pad="8"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="RSSI" pad="6"/>
<connect gate="G$1" pin="RTS" pad="16"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="schematic">
<description>Generated from &lt;b&gt;schematic.sch&lt;/b&gt;&lt;p&gt;
by exp-lbrs-replace2.ulp</description>
<packages>
</packages>
<symbols>
<symbol name="3.3V">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="3.3V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Particle">
<packages>
<package name="ELECTRON_DUAL-ROW">
<description>Electron socket for a dual-row header for easy prototyping, with the pins also duplicated to unpopulated through-holes for easy soldering.</description>
<wire x1="17.145" y1="3.81" x2="17.78" y2="3.175" width="0.2032" layer="21"/>
<wire x1="17.78" y1="1.905" x2="17.145" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.7" y1="3.175" x2="13.335" y2="3.81" width="0.2032" layer="21"/>
<wire x1="14.605" y1="3.81" x2="15.24" y2="3.175" width="0.2032" layer="21"/>
<wire x1="15.24" y1="1.905" x2="14.605" y2="1.27" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="12.7" y2="1.905" width="0.2032" layer="21"/>
<wire x1="15.875" y1="3.81" x2="15.24" y2="3.175" width="0.2032" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="3.81" x2="10.16" y2="3.175" width="0.2032" layer="21"/>
<wire x1="10.16" y1="1.905" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.16" y1="3.175" x2="10.795" y2="3.81" width="0.2032" layer="21"/>
<wire x1="12.065" y1="3.81" x2="12.7" y2="3.175" width="0.2032" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="10.16" y2="1.905" width="0.2032" layer="21"/>
<wire x1="5.08" y1="3.175" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="6.985" y1="3.81" x2="7.62" y2="3.175" width="0.2032" layer="21"/>
<wire x1="7.62" y1="1.905" x2="6.985" y2="1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="8.255" y1="3.81" x2="7.62" y2="3.175" width="0.2032" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="2.54" y2="3.175" width="0.2032" layer="21"/>
<wire x1="2.54" y1="1.905" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="2.54" y1="3.175" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.08" y2="3.175" width="0.2032" layer="21"/>
<wire x1="5.08" y1="1.905" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="0" y1="3.175" x2="0" y2="1.905" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="0" y2="3.175" width="0.2032" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="3.81" x2="20.32" y2="3.175" width="0.2032" layer="21"/>
<wire x1="20.32" y1="1.905" x2="19.685" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="3.81" x2="17.78" y2="3.175" width="0.2032" layer="21"/>
<wire x1="17.78" y1="1.905" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="3.81" x2="22.86" y2="3.175" width="0.2032" layer="21"/>
<wire x1="22.86" y1="1.905" x2="22.225" y2="1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="3.81" x2="20.32" y2="3.175" width="0.2032" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.955" y2="1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="1.27" x2="20.955" y2="1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="3.81" x2="25.4" y2="3.175" width="0.2032" layer="21"/>
<wire x1="25.4" y1="1.905" x2="24.765" y2="1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="3.81" x2="22.86" y2="3.175" width="0.2032" layer="21"/>
<wire x1="22.86" y1="1.905" x2="23.495" y2="1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="1.27" x2="23.495" y2="1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="3.81" x2="27.94" y2="3.175" width="0.2032" layer="21"/>
<wire x1="27.94" y1="1.905" x2="27.305" y2="1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="3.81" x2="25.4" y2="3.175" width="0.2032" layer="21"/>
<wire x1="25.4" y1="1.905" x2="26.035" y2="1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="1.27" x2="26.035" y2="1.27" width="0.2032" layer="21"/>
<wire x1="29.845" y1="3.81" x2="30.48" y2="3.175" width="0.2032" layer="21"/>
<wire x1="30.48" y1="1.905" x2="29.845" y2="1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="3.81" x2="27.94" y2="3.175" width="0.2032" layer="21"/>
<wire x1="27.94" y1="1.905" x2="28.575" y2="1.27" width="0.2032" layer="21"/>
<wire x1="29.845" y1="1.27" x2="28.575" y2="1.27" width="0.2032" layer="21"/>
<wire x1="32.385" y1="3.81" x2="33.02" y2="3.175" width="0.2032" layer="21"/>
<wire x1="33.02" y1="1.905" x2="32.385" y2="1.27" width="0.2032" layer="21"/>
<wire x1="31.115" y1="3.81" x2="30.48" y2="3.175" width="0.2032" layer="21"/>
<wire x1="30.48" y1="1.905" x2="31.115" y2="1.27" width="0.2032" layer="21"/>
<wire x1="32.385" y1="1.27" x2="31.115" y2="1.27" width="0.2032" layer="21"/>
<wire x1="34.925" y1="3.81" x2="35.56" y2="3.175" width="0.2032" layer="21"/>
<wire x1="35.56" y1="1.905" x2="34.925" y2="1.27" width="0.2032" layer="21"/>
<wire x1="33.655" y1="3.81" x2="33.02" y2="3.175" width="0.2032" layer="21"/>
<wire x1="33.02" y1="1.905" x2="33.655" y2="1.27" width="0.2032" layer="21"/>
<wire x1="34.925" y1="1.27" x2="33.655" y2="1.27" width="0.2032" layer="21"/>
<wire x1="37.465" y1="3.81" x2="38.1" y2="3.175" width="0.2032" layer="21"/>
<wire x1="38.1" y1="1.905" x2="37.465" y2="1.27" width="0.2032" layer="21"/>
<wire x1="36.195" y1="3.81" x2="35.56" y2="3.175" width="0.2032" layer="21"/>
<wire x1="35.56" y1="1.905" x2="36.195" y2="1.27" width="0.2032" layer="21"/>
<wire x1="37.465" y1="1.27" x2="36.195" y2="1.27" width="0.2032" layer="21"/>
<wire x1="40.005" y1="3.81" x2="40.64" y2="3.175" width="0.2032" layer="21"/>
<wire x1="40.64" y1="1.905" x2="40.005" y2="1.27" width="0.2032" layer="21"/>
<wire x1="38.735" y1="3.81" x2="38.1" y2="3.175" width="0.2032" layer="21"/>
<wire x1="38.1" y1="1.905" x2="38.735" y2="1.27" width="0.2032" layer="21"/>
<wire x1="40.005" y1="1.27" x2="38.735" y2="1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="43.18" y2="3.175" width="0.2032" layer="21"/>
<wire x1="43.18" y1="1.905" x2="42.545" y2="1.27" width="0.2032" layer="21"/>
<wire x1="41.275" y1="3.81" x2="40.64" y2="3.175" width="0.2032" layer="21"/>
<wire x1="40.64" y1="1.905" x2="41.275" y2="1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="1.27" x2="41.275" y2="1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="45.72" y2="3.175" width="0.2032" layer="21"/>
<wire x1="45.72" y1="3.175" x2="45.72" y2="1.905" width="0.2032" layer="21"/>
<wire x1="45.72" y1="1.905" x2="45.085" y2="1.27" width="0.2032" layer="21"/>
<wire x1="43.815" y1="3.81" x2="43.18" y2="3.175" width="0.2032" layer="21"/>
<wire x1="43.18" y1="1.905" x2="43.815" y2="1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="1.27" x2="43.815" y2="1.27" width="0.2032" layer="21"/>
<rectangle x1="16.256" y1="2.286" x2="16.764" y2="2.794" layer="51"/>
<rectangle x1="13.716" y1="2.286" x2="14.224" y2="2.794" layer="51"/>
<rectangle x1="11.176" y1="2.286" x2="11.684" y2="2.794" layer="51"/>
<rectangle x1="8.636" y1="2.286" x2="9.144" y2="2.794" layer="51"/>
<rectangle x1="6.096" y1="2.286" x2="6.604" y2="2.794" layer="51"/>
<rectangle x1="3.556" y1="2.286" x2="4.064" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="18.796" y1="2.286" x2="19.304" y2="2.794" layer="51"/>
<rectangle x1="21.336" y1="2.286" x2="21.844" y2="2.794" layer="51"/>
<rectangle x1="23.876" y1="2.286" x2="24.384" y2="2.794" layer="51"/>
<rectangle x1="26.416" y1="2.286" x2="26.924" y2="2.794" layer="51"/>
<rectangle x1="28.956" y1="2.286" x2="29.464" y2="2.794" layer="51"/>
<rectangle x1="31.496" y1="2.286" x2="32.004" y2="2.794" layer="51"/>
<rectangle x1="34.036" y1="2.286" x2="34.544" y2="2.794" layer="51"/>
<rectangle x1="36.576" y1="2.286" x2="37.084" y2="2.794" layer="51"/>
<rectangle x1="39.116" y1="2.286" x2="39.624" y2="2.794" layer="51"/>
<rectangle x1="41.656" y1="2.286" x2="42.164" y2="2.794" layer="51"/>
<rectangle x1="44.196" y1="2.286" x2="44.704" y2="2.794" layer="51"/>
<pad name="1" x="1.27" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="2" x="3.81" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="3" x="6.35" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="4" x="8.89" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="5" x="11.43" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="6" x="13.97" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="7" x="16.51" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="8" x="19.05" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="9" x="21.59" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="10" x="24.13" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="11" x="26.67" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="12" x="29.21" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="13" x="31.75" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="14" x="34.29" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="15" x="36.83" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="16" x="39.37" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="17" x="41.91" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="18" x="44.45" y="2.54" drill="0.889" diameter="1.7526" rot="R90"/>
<wire x1="17.145" y1="6.35" x2="17.78" y2="5.715" width="0.2032" layer="21"/>
<wire x1="17.78" y1="4.445" x2="17.145" y2="3.81" width="0.2032" layer="21"/>
<wire x1="12.7" y1="5.715" x2="13.335" y2="6.35" width="0.2032" layer="21"/>
<wire x1="14.605" y1="6.35" x2="15.24" y2="5.715" width="0.2032" layer="21"/>
<wire x1="15.24" y1="4.445" x2="14.605" y2="3.81" width="0.2032" layer="21"/>
<wire x1="14.605" y1="3.81" x2="13.335" y2="3.81" width="0.2032" layer="21"/>
<wire x1="13.335" y1="3.81" x2="12.7" y2="4.445" width="0.2032" layer="21"/>
<wire x1="15.875" y1="6.35" x2="15.24" y2="5.715" width="0.2032" layer="21"/>
<wire x1="15.24" y1="4.445" x2="15.875" y2="3.81" width="0.2032" layer="21"/>
<wire x1="17.145" y1="3.81" x2="15.875" y2="3.81" width="0.2032" layer="21"/>
<wire x1="9.525" y1="6.35" x2="10.16" y2="5.715" width="0.2032" layer="21"/>
<wire x1="10.16" y1="4.445" x2="9.525" y2="3.81" width="0.2032" layer="21"/>
<wire x1="10.16" y1="5.715" x2="10.795" y2="6.35" width="0.2032" layer="21"/>
<wire x1="12.065" y1="6.35" x2="12.7" y2="5.715" width="0.2032" layer="21"/>
<wire x1="12.7" y1="4.445" x2="12.065" y2="3.81" width="0.2032" layer="21"/>
<wire x1="12.065" y1="3.81" x2="10.795" y2="3.81" width="0.2032" layer="21"/>
<wire x1="10.795" y1="3.81" x2="10.16" y2="4.445" width="0.2032" layer="21"/>
<wire x1="5.08" y1="5.715" x2="5.715" y2="6.35" width="0.2032" layer="21"/>
<wire x1="6.985" y1="6.35" x2="7.62" y2="5.715" width="0.2032" layer="21"/>
<wire x1="7.62" y1="4.445" x2="6.985" y2="3.81" width="0.2032" layer="21"/>
<wire x1="6.985" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="5.08" y2="4.445" width="0.2032" layer="21"/>
<wire x1="8.255" y1="6.35" x2="7.62" y2="5.715" width="0.2032" layer="21"/>
<wire x1="7.62" y1="4.445" x2="8.255" y2="3.81" width="0.2032" layer="21"/>
<wire x1="9.525" y1="3.81" x2="8.255" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="6.35" x2="2.54" y2="5.715" width="0.2032" layer="21"/>
<wire x1="2.54" y1="4.445" x2="1.905" y2="3.81" width="0.2032" layer="21"/>
<wire x1="2.54" y1="5.715" x2="3.175" y2="6.35" width="0.2032" layer="21"/>
<wire x1="4.445" y1="6.35" x2="5.08" y2="5.715" width="0.2032" layer="21"/>
<wire x1="5.08" y1="4.445" x2="4.445" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="2.54" y2="4.445" width="0.2032" layer="21"/>
<wire x1="0" y1="5.715" x2="0" y2="4.445" width="0.2032" layer="21"/>
<wire x1="0.635" y1="6.35" x2="0" y2="5.715" width="0.2032" layer="21"/>
<wire x1="0" y1="4.445" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="19.685" y1="6.35" x2="20.32" y2="5.715" width="0.2032" layer="21"/>
<wire x1="20.32" y1="4.445" x2="19.685" y2="3.81" width="0.2032" layer="21"/>
<wire x1="18.415" y1="6.35" x2="17.78" y2="5.715" width="0.2032" layer="21"/>
<wire x1="17.78" y1="4.445" x2="18.415" y2="3.81" width="0.2032" layer="21"/>
<wire x1="19.685" y1="3.81" x2="18.415" y2="3.81" width="0.2032" layer="21"/>
<wire x1="22.225" y1="6.35" x2="22.86" y2="5.715" width="0.2032" layer="21"/>
<wire x1="22.86" y1="4.445" x2="22.225" y2="3.81" width="0.2032" layer="21"/>
<wire x1="20.955" y1="6.35" x2="20.32" y2="5.715" width="0.2032" layer="21"/>
<wire x1="20.32" y1="4.445" x2="20.955" y2="3.81" width="0.2032" layer="21"/>
<wire x1="22.225" y1="3.81" x2="20.955" y2="3.81" width="0.2032" layer="21"/>
<wire x1="24.765" y1="6.35" x2="25.4" y2="5.715" width="0.2032" layer="21"/>
<wire x1="25.4" y1="4.445" x2="24.765" y2="3.81" width="0.2032" layer="21"/>
<wire x1="23.495" y1="6.35" x2="22.86" y2="5.715" width="0.2032" layer="21"/>
<wire x1="22.86" y1="4.445" x2="23.495" y2="3.81" width="0.2032" layer="21"/>
<wire x1="24.765" y1="3.81" x2="23.495" y2="3.81" width="0.2032" layer="21"/>
<wire x1="27.305" y1="6.35" x2="27.94" y2="5.715" width="0.2032" layer="21"/>
<wire x1="27.94" y1="4.445" x2="27.305" y2="3.81" width="0.2032" layer="21"/>
<wire x1="26.035" y1="6.35" x2="25.4" y2="5.715" width="0.2032" layer="21"/>
<wire x1="25.4" y1="4.445" x2="26.035" y2="3.81" width="0.2032" layer="21"/>
<wire x1="27.305" y1="3.81" x2="26.035" y2="3.81" width="0.2032" layer="21"/>
<wire x1="29.845" y1="6.35" x2="30.48" y2="5.715" width="0.2032" layer="21"/>
<wire x1="30.48" y1="4.445" x2="29.845" y2="3.81" width="0.2032" layer="21"/>
<wire x1="28.575" y1="6.35" x2="27.94" y2="5.715" width="0.2032" layer="21"/>
<wire x1="27.94" y1="4.445" x2="28.575" y2="3.81" width="0.2032" layer="21"/>
<wire x1="29.845" y1="3.81" x2="28.575" y2="3.81" width="0.2032" layer="21"/>
<wire x1="32.385" y1="6.35" x2="33.02" y2="5.715" width="0.2032" layer="21"/>
<wire x1="33.02" y1="4.445" x2="32.385" y2="3.81" width="0.2032" layer="21"/>
<wire x1="31.115" y1="6.35" x2="30.48" y2="5.715" width="0.2032" layer="21"/>
<wire x1="30.48" y1="4.445" x2="31.115" y2="3.81" width="0.2032" layer="21"/>
<wire x1="32.385" y1="3.81" x2="31.115" y2="3.81" width="0.2032" layer="21"/>
<wire x1="34.925" y1="6.35" x2="35.56" y2="5.715" width="0.2032" layer="21"/>
<wire x1="35.56" y1="4.445" x2="34.925" y2="3.81" width="0.2032" layer="21"/>
<wire x1="33.655" y1="6.35" x2="33.02" y2="5.715" width="0.2032" layer="21"/>
<wire x1="33.02" y1="4.445" x2="33.655" y2="3.81" width="0.2032" layer="21"/>
<wire x1="34.925" y1="3.81" x2="33.655" y2="3.81" width="0.2032" layer="21"/>
<wire x1="37.465" y1="6.35" x2="38.1" y2="5.715" width="0.2032" layer="21"/>
<wire x1="38.1" y1="4.445" x2="37.465" y2="3.81" width="0.2032" layer="21"/>
<wire x1="36.195" y1="6.35" x2="35.56" y2="5.715" width="0.2032" layer="21"/>
<wire x1="35.56" y1="4.445" x2="36.195" y2="3.81" width="0.2032" layer="21"/>
<wire x1="37.465" y1="3.81" x2="36.195" y2="3.81" width="0.2032" layer="21"/>
<wire x1="40.005" y1="6.35" x2="40.64" y2="5.715" width="0.2032" layer="21"/>
<wire x1="40.64" y1="4.445" x2="40.005" y2="3.81" width="0.2032" layer="21"/>
<wire x1="38.735" y1="6.35" x2="38.1" y2="5.715" width="0.2032" layer="21"/>
<wire x1="38.1" y1="4.445" x2="38.735" y2="3.81" width="0.2032" layer="21"/>
<wire x1="40.005" y1="3.81" x2="38.735" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="6.35" x2="43.18" y2="5.715" width="0.2032" layer="21"/>
<wire x1="43.18" y1="4.445" x2="42.545" y2="3.81" width="0.2032" layer="21"/>
<wire x1="41.275" y1="6.35" x2="40.64" y2="5.715" width="0.2032" layer="21"/>
<wire x1="40.64" y1="4.445" x2="41.275" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="41.275" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="6.35" x2="45.72" y2="5.715" width="0.2032" layer="21"/>
<wire x1="45.72" y1="5.715" x2="45.72" y2="4.445" width="0.2032" layer="21"/>
<wire x1="45.72" y1="4.445" x2="45.085" y2="3.81" width="0.2032" layer="21"/>
<wire x1="43.815" y1="6.35" x2="43.18" y2="5.715" width="0.2032" layer="21"/>
<wire x1="43.18" y1="4.445" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<rectangle x1="16.256" y1="4.826" x2="16.764" y2="5.334" layer="51"/>
<rectangle x1="13.716" y1="4.826" x2="14.224" y2="5.334" layer="51"/>
<rectangle x1="11.176" y1="4.826" x2="11.684" y2="5.334" layer="51"/>
<rectangle x1="8.636" y1="4.826" x2="9.144" y2="5.334" layer="51"/>
<rectangle x1="6.096" y1="4.826" x2="6.604" y2="5.334" layer="51"/>
<rectangle x1="3.556" y1="4.826" x2="4.064" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="18.796" y1="4.826" x2="19.304" y2="5.334" layer="51"/>
<rectangle x1="21.336" y1="4.826" x2="21.844" y2="5.334" layer="51"/>
<rectangle x1="23.876" y1="4.826" x2="24.384" y2="5.334" layer="51"/>
<rectangle x1="26.416" y1="4.826" x2="26.924" y2="5.334" layer="51"/>
<rectangle x1="28.956" y1="4.826" x2="29.464" y2="5.334" layer="51"/>
<rectangle x1="31.496" y1="4.826" x2="32.004" y2="5.334" layer="51"/>
<rectangle x1="34.036" y1="4.826" x2="34.544" y2="5.334" layer="51"/>
<rectangle x1="36.576" y1="4.826" x2="37.084" y2="5.334" layer="51"/>
<rectangle x1="39.116" y1="4.826" x2="39.624" y2="5.334" layer="51"/>
<rectangle x1="41.656" y1="4.826" x2="42.164" y2="5.334" layer="51"/>
<rectangle x1="44.196" y1="4.826" x2="44.704" y2="5.334" layer="51"/>
<pad name="VIN" x="1.27" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="GND@2" x="3.81" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="TX" x="6.35" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="RX" x="8.89" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="WKP" x="11.43" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="DAC" x="13.97" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="A5" x="16.51" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="A4" x="19.05" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="A3" x="21.59" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="A2" x="24.13" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="A1" x="26.67" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="A0" x="29.21" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="B5" x="31.75" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="B4" x="34.29" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="B3" x="36.83" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="B2" x="39.37" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="B1" x="41.91" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="B0" x="44.45" y="5.08" drill="0.889" diameter="1.7526" rot="R90"/>
<wire x1="14.605" y1="6.35" x2="13.335" y2="6.35" width="0.2032" layer="21"/>
<wire x1="17.145" y1="6.35" x2="15.875" y2="6.35" width="0.2032" layer="21"/>
<wire x1="12.065" y1="6.35" x2="10.795" y2="6.35" width="0.2032" layer="21"/>
<wire x1="6.985" y1="6.35" x2="5.715" y2="6.35" width="0.2032" layer="21"/>
<wire x1="9.525" y1="6.35" x2="8.255" y2="6.35" width="0.2032" layer="21"/>
<wire x1="4.445" y1="6.35" x2="3.175" y2="6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="6.35" x2="0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="19.685" y1="6.35" x2="18.415" y2="6.35" width="0.2032" layer="21"/>
<wire x1="22.225" y1="6.35" x2="20.955" y2="6.35" width="0.2032" layer="21"/>
<wire x1="24.765" y1="6.35" x2="23.495" y2="6.35" width="0.2032" layer="21"/>
<wire x1="27.305" y1="6.35" x2="26.035" y2="6.35" width="0.2032" layer="21"/>
<wire x1="29.845" y1="6.35" x2="28.575" y2="6.35" width="0.2032" layer="21"/>
<wire x1="32.385" y1="6.35" x2="31.115" y2="6.35" width="0.2032" layer="21"/>
<wire x1="34.925" y1="6.35" x2="33.655" y2="6.35" width="0.2032" layer="21"/>
<wire x1="37.465" y1="6.35" x2="36.195" y2="6.35" width="0.2032" layer="21"/>
<wire x1="40.005" y1="6.35" x2="38.735" y2="6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="6.35" x2="41.275" y2="6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="6.35" x2="43.815" y2="6.35" width="0.2032" layer="21"/>
<pad name="37" x="1.27" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="38" x="3.81" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="39" x="6.35" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="40" x="8.89" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="41" x="11.43" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="42" x="13.97" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="43" x="16.51" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="44" x="19.05" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="45" x="21.59" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="46" x="24.13" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="47" x="26.67" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="48" x="29.21" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="49" x="31.75" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="50" x="34.29" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="51" x="36.83" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="52" x="39.37" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="53" x="41.91" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="54" x="44.45" y="7.62" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="55" x="1.27" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="56" x="3.81" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="57" x="6.35" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="58" x="8.89" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="59" x="11.43" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="60" x="13.97" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="61" x="16.51" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="62" x="19.05" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="63" x="21.59" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="64" x="24.13" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="65" x="26.67" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="66" x="29.21" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="67" x="31.75" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="68" x="34.29" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="69" x="36.83" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="70" x="39.37" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="71" x="41.91" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="72" x="44.45" y="20.32" drill="0.889" diameter="1.7526" rot="R90"/>
<wire x1="17.145" y1="24.13" x2="17.78" y2="23.495" width="0.2032" layer="21"/>
<wire x1="17.78" y1="22.225" x2="17.145" y2="21.59" width="0.2032" layer="21"/>
<wire x1="12.7" y1="23.495" x2="13.335" y2="24.13" width="0.2032" layer="21"/>
<wire x1="14.605" y1="24.13" x2="15.24" y2="23.495" width="0.2032" layer="21"/>
<wire x1="15.24" y1="22.225" x2="14.605" y2="21.59" width="0.2032" layer="21"/>
<wire x1="14.605" y1="21.59" x2="13.335" y2="21.59" width="0.2032" layer="21"/>
<wire x1="13.335" y1="21.59" x2="12.7" y2="22.225" width="0.2032" layer="21"/>
<wire x1="15.875" y1="24.13" x2="15.24" y2="23.495" width="0.2032" layer="21"/>
<wire x1="15.24" y1="22.225" x2="15.875" y2="21.59" width="0.2032" layer="21"/>
<wire x1="17.145" y1="21.59" x2="15.875" y2="21.59" width="0.2032" layer="21"/>
<wire x1="9.525" y1="24.13" x2="10.16" y2="23.495" width="0.2032" layer="21"/>
<wire x1="10.16" y1="22.225" x2="9.525" y2="21.59" width="0.2032" layer="21"/>
<wire x1="10.16" y1="23.495" x2="10.795" y2="24.13" width="0.2032" layer="21"/>
<wire x1="12.065" y1="24.13" x2="12.7" y2="23.495" width="0.2032" layer="21"/>
<wire x1="12.7" y1="22.225" x2="12.065" y2="21.59" width="0.2032" layer="21"/>
<wire x1="12.065" y1="21.59" x2="10.795" y2="21.59" width="0.2032" layer="21"/>
<wire x1="10.795" y1="21.59" x2="10.16" y2="22.225" width="0.2032" layer="21"/>
<wire x1="5.08" y1="23.495" x2="5.715" y2="24.13" width="0.2032" layer="21"/>
<wire x1="6.985" y1="24.13" x2="7.62" y2="23.495" width="0.2032" layer="21"/>
<wire x1="7.62" y1="22.225" x2="6.985" y2="21.59" width="0.2032" layer="21"/>
<wire x1="6.985" y1="21.59" x2="5.715" y2="21.59" width="0.2032" layer="21"/>
<wire x1="5.715" y1="21.59" x2="5.08" y2="22.225" width="0.2032" layer="21"/>
<wire x1="8.255" y1="24.13" x2="7.62" y2="23.495" width="0.2032" layer="21"/>
<wire x1="7.62" y1="22.225" x2="8.255" y2="21.59" width="0.2032" layer="21"/>
<wire x1="9.525" y1="21.59" x2="8.255" y2="21.59" width="0.2032" layer="21"/>
<wire x1="1.905" y1="24.13" x2="2.54" y2="23.495" width="0.2032" layer="21"/>
<wire x1="2.54" y1="22.225" x2="1.905" y2="21.59" width="0.2032" layer="21"/>
<wire x1="2.54" y1="23.495" x2="3.175" y2="24.13" width="0.2032" layer="21"/>
<wire x1="4.445" y1="24.13" x2="5.08" y2="23.495" width="0.2032" layer="21"/>
<wire x1="5.08" y1="22.225" x2="4.445" y2="21.59" width="0.2032" layer="21"/>
<wire x1="4.445" y1="21.59" x2="3.175" y2="21.59" width="0.2032" layer="21"/>
<wire x1="3.175" y1="21.59" x2="2.54" y2="22.225" width="0.2032" layer="21"/>
<wire x1="0" y1="23.495" x2="0" y2="22.225" width="0.2032" layer="21"/>
<wire x1="0.635" y1="24.13" x2="0" y2="23.495" width="0.2032" layer="21"/>
<wire x1="0" y1="22.225" x2="0.635" y2="21.59" width="0.2032" layer="21"/>
<wire x1="1.905" y1="21.59" x2="0.635" y2="21.59" width="0.2032" layer="21"/>
<wire x1="19.685" y1="24.13" x2="20.32" y2="23.495" width="0.2032" layer="21"/>
<wire x1="20.32" y1="22.225" x2="19.685" y2="21.59" width="0.2032" layer="21"/>
<wire x1="18.415" y1="24.13" x2="17.78" y2="23.495" width="0.2032" layer="21"/>
<wire x1="17.78" y1="22.225" x2="18.415" y2="21.59" width="0.2032" layer="21"/>
<wire x1="19.685" y1="21.59" x2="18.415" y2="21.59" width="0.2032" layer="21"/>
<wire x1="22.225" y1="24.13" x2="22.86" y2="23.495" width="0.2032" layer="21"/>
<wire x1="22.86" y1="22.225" x2="22.225" y2="21.59" width="0.2032" layer="21"/>
<wire x1="20.955" y1="24.13" x2="20.32" y2="23.495" width="0.2032" layer="21"/>
<wire x1="20.32" y1="22.225" x2="20.955" y2="21.59" width="0.2032" layer="21"/>
<wire x1="22.225" y1="21.59" x2="20.955" y2="21.59" width="0.2032" layer="21"/>
<wire x1="24.765" y1="24.13" x2="25.4" y2="23.495" width="0.2032" layer="21"/>
<wire x1="25.4" y1="22.225" x2="24.765" y2="21.59" width="0.2032" layer="21"/>
<wire x1="23.495" y1="24.13" x2="22.86" y2="23.495" width="0.2032" layer="21"/>
<wire x1="22.86" y1="22.225" x2="23.495" y2="21.59" width="0.2032" layer="21"/>
<wire x1="24.765" y1="21.59" x2="23.495" y2="21.59" width="0.2032" layer="21"/>
<wire x1="27.305" y1="24.13" x2="27.94" y2="23.495" width="0.2032" layer="21"/>
<wire x1="27.94" y1="22.225" x2="27.305" y2="21.59" width="0.2032" layer="21"/>
<wire x1="26.035" y1="24.13" x2="25.4" y2="23.495" width="0.2032" layer="21"/>
<wire x1="25.4" y1="22.225" x2="26.035" y2="21.59" width="0.2032" layer="21"/>
<wire x1="27.305" y1="21.59" x2="26.035" y2="21.59" width="0.2032" layer="21"/>
<wire x1="29.845" y1="24.13" x2="30.48" y2="23.495" width="0.2032" layer="21"/>
<wire x1="30.48" y1="22.225" x2="29.845" y2="21.59" width="0.2032" layer="21"/>
<wire x1="28.575" y1="24.13" x2="27.94" y2="23.495" width="0.2032" layer="21"/>
<wire x1="27.94" y1="22.225" x2="28.575" y2="21.59" width="0.2032" layer="21"/>
<wire x1="29.845" y1="21.59" x2="28.575" y2="21.59" width="0.2032" layer="21"/>
<wire x1="32.385" y1="24.13" x2="33.02" y2="23.495" width="0.2032" layer="21"/>
<wire x1="33.02" y1="22.225" x2="32.385" y2="21.59" width="0.2032" layer="21"/>
<wire x1="31.115" y1="24.13" x2="30.48" y2="23.495" width="0.2032" layer="21"/>
<wire x1="30.48" y1="22.225" x2="31.115" y2="21.59" width="0.2032" layer="21"/>
<wire x1="32.385" y1="21.59" x2="31.115" y2="21.59" width="0.2032" layer="21"/>
<wire x1="34.925" y1="24.13" x2="35.56" y2="23.495" width="0.2032" layer="21"/>
<wire x1="35.56" y1="22.225" x2="34.925" y2="21.59" width="0.2032" layer="21"/>
<wire x1="33.655" y1="24.13" x2="33.02" y2="23.495" width="0.2032" layer="21"/>
<wire x1="33.02" y1="22.225" x2="33.655" y2="21.59" width="0.2032" layer="21"/>
<wire x1="34.925" y1="21.59" x2="33.655" y2="21.59" width="0.2032" layer="21"/>
<wire x1="37.465" y1="24.13" x2="38.1" y2="23.495" width="0.2032" layer="21"/>
<wire x1="38.1" y1="22.225" x2="37.465" y2="21.59" width="0.2032" layer="21"/>
<wire x1="36.195" y1="24.13" x2="35.56" y2="23.495" width="0.2032" layer="21"/>
<wire x1="35.56" y1="22.225" x2="36.195" y2="21.59" width="0.2032" layer="21"/>
<wire x1="37.465" y1="21.59" x2="36.195" y2="21.59" width="0.2032" layer="21"/>
<wire x1="40.005" y1="24.13" x2="40.64" y2="23.495" width="0.2032" layer="21"/>
<wire x1="40.64" y1="22.225" x2="40.005" y2="21.59" width="0.2032" layer="21"/>
<wire x1="38.735" y1="24.13" x2="38.1" y2="23.495" width="0.2032" layer="21"/>
<wire x1="38.1" y1="22.225" x2="38.735" y2="21.59" width="0.2032" layer="21"/>
<wire x1="40.005" y1="21.59" x2="38.735" y2="21.59" width="0.2032" layer="21"/>
<wire x1="42.545" y1="24.13" x2="43.18" y2="23.495" width="0.2032" layer="21"/>
<wire x1="43.18" y1="22.225" x2="42.545" y2="21.59" width="0.2032" layer="21"/>
<wire x1="41.275" y1="24.13" x2="40.64" y2="23.495" width="0.2032" layer="21"/>
<wire x1="40.64" y1="22.225" x2="41.275" y2="21.59" width="0.2032" layer="21"/>
<wire x1="42.545" y1="21.59" x2="41.275" y2="21.59" width="0.2032" layer="21"/>
<wire x1="45.085" y1="24.13" x2="45.72" y2="23.495" width="0.2032" layer="21"/>
<wire x1="45.72" y1="23.495" x2="45.72" y2="22.225" width="0.2032" layer="21"/>
<wire x1="45.72" y1="22.225" x2="45.085" y2="21.59" width="0.2032" layer="21"/>
<wire x1="43.815" y1="24.13" x2="43.18" y2="23.495" width="0.2032" layer="21"/>
<wire x1="43.18" y1="22.225" x2="43.815" y2="21.59" width="0.2032" layer="21"/>
<wire x1="45.085" y1="21.59" x2="43.815" y2="21.59" width="0.2032" layer="21"/>
<rectangle x1="16.256" y1="22.606" x2="16.764" y2="23.114" layer="51"/>
<rectangle x1="13.716" y1="22.606" x2="14.224" y2="23.114" layer="51"/>
<rectangle x1="11.176" y1="22.606" x2="11.684" y2="23.114" layer="51"/>
<rectangle x1="8.636" y1="22.606" x2="9.144" y2="23.114" layer="51"/>
<rectangle x1="6.096" y1="22.606" x2="6.604" y2="23.114" layer="51"/>
<rectangle x1="3.556" y1="22.606" x2="4.064" y2="23.114" layer="51"/>
<rectangle x1="1.016" y1="22.606" x2="1.524" y2="23.114" layer="51"/>
<rectangle x1="18.796" y1="22.606" x2="19.304" y2="23.114" layer="51"/>
<rectangle x1="21.336" y1="22.606" x2="21.844" y2="23.114" layer="51"/>
<rectangle x1="23.876" y1="22.606" x2="24.384" y2="23.114" layer="51"/>
<rectangle x1="26.416" y1="22.606" x2="26.924" y2="23.114" layer="51"/>
<rectangle x1="28.956" y1="22.606" x2="29.464" y2="23.114" layer="51"/>
<rectangle x1="31.496" y1="22.606" x2="32.004" y2="23.114" layer="51"/>
<rectangle x1="34.036" y1="22.606" x2="34.544" y2="23.114" layer="51"/>
<rectangle x1="36.576" y1="22.606" x2="37.084" y2="23.114" layer="51"/>
<rectangle x1="39.116" y1="22.606" x2="39.624" y2="23.114" layer="51"/>
<rectangle x1="41.656" y1="22.606" x2="42.164" y2="23.114" layer="51"/>
<rectangle x1="44.196" y1="22.606" x2="44.704" y2="23.114" layer="51"/>
<pad name="3V3" x="1.27" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="RST" x="3.81" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="VBAT" x="6.35" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="GND" x="8.89" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="D7" x="11.43" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="D6" x="13.97" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="D5" x="16.51" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="D4" x="19.05" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="D3" x="21.59" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="D2" x="24.13" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="D1" x="26.67" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="D0" x="29.21" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="C5" x="31.75" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="C4" x="34.29" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="C3" x="36.83" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="C2" x="39.37" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="C1" x="41.91" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="C0" x="44.45" y="22.86" drill="0.889" diameter="1.7526" rot="R90"/>
<wire x1="15.875" y1="26.67" x2="17.145" y2="26.67" width="0.2032" layer="21"/>
<wire x1="17.145" y1="26.67" x2="17.78" y2="26.035" width="0.2032" layer="21"/>
<wire x1="17.78" y1="24.765" x2="17.145" y2="24.13" width="0.2032" layer="21"/>
<wire x1="12.7" y1="26.035" x2="13.335" y2="26.67" width="0.2032" layer="21"/>
<wire x1="13.335" y1="26.67" x2="14.605" y2="26.67" width="0.2032" layer="21"/>
<wire x1="14.605" y1="26.67" x2="15.24" y2="26.035" width="0.2032" layer="21"/>
<wire x1="15.24" y1="24.765" x2="14.605" y2="24.13" width="0.2032" layer="21"/>
<wire x1="14.605" y1="24.13" x2="13.335" y2="24.13" width="0.2032" layer="21"/>
<wire x1="13.335" y1="24.13" x2="12.7" y2="24.765" width="0.2032" layer="21"/>
<wire x1="15.875" y1="26.67" x2="15.24" y2="26.035" width="0.2032" layer="21"/>
<wire x1="15.24" y1="24.765" x2="15.875" y2="24.13" width="0.2032" layer="21"/>
<wire x1="17.145" y1="24.13" x2="15.875" y2="24.13" width="0.2032" layer="21"/>
<wire x1="8.255" y1="26.67" x2="9.525" y2="26.67" width="0.2032" layer="21"/>
<wire x1="9.525" y1="26.67" x2="10.16" y2="26.035" width="0.2032" layer="21"/>
<wire x1="10.16" y1="24.765" x2="9.525" y2="24.13" width="0.2032" layer="21"/>
<wire x1="10.16" y1="26.035" x2="10.795" y2="26.67" width="0.2032" layer="21"/>
<wire x1="10.795" y1="26.67" x2="12.065" y2="26.67" width="0.2032" layer="21"/>
<wire x1="12.065" y1="26.67" x2="12.7" y2="26.035" width="0.2032" layer="21"/>
<wire x1="12.7" y1="24.765" x2="12.065" y2="24.13" width="0.2032" layer="21"/>
<wire x1="12.065" y1="24.13" x2="10.795" y2="24.13" width="0.2032" layer="21"/>
<wire x1="10.795" y1="24.13" x2="10.16" y2="24.765" width="0.2032" layer="21"/>
<wire x1="5.08" y1="26.035" x2="5.715" y2="26.67" width="0.2032" layer="21"/>
<wire x1="5.715" y1="26.67" x2="6.985" y2="26.67" width="0.2032" layer="21"/>
<wire x1="6.985" y1="26.67" x2="7.62" y2="26.035" width="0.2032" layer="21"/>
<wire x1="7.62" y1="24.765" x2="6.985" y2="24.13" width="0.2032" layer="21"/>
<wire x1="6.985" y1="24.13" x2="5.715" y2="24.13" width="0.2032" layer="21"/>
<wire x1="5.715" y1="24.13" x2="5.08" y2="24.765" width="0.2032" layer="21"/>
<wire x1="8.255" y1="26.67" x2="7.62" y2="26.035" width="0.2032" layer="21"/>
<wire x1="7.62" y1="24.765" x2="8.255" y2="24.13" width="0.2032" layer="21"/>
<wire x1="9.525" y1="24.13" x2="8.255" y2="24.13" width="0.2032" layer="21"/>
<wire x1="0.635" y1="26.67" x2="1.905" y2="26.67" width="0.2032" layer="21"/>
<wire x1="1.905" y1="26.67" x2="2.54" y2="26.035" width="0.2032" layer="21"/>
<wire x1="2.54" y1="24.765" x2="1.905" y2="24.13" width="0.2032" layer="21"/>
<wire x1="2.54" y1="26.035" x2="3.175" y2="26.67" width="0.2032" layer="21"/>
<wire x1="3.175" y1="26.67" x2="4.445" y2="26.67" width="0.2032" layer="21"/>
<wire x1="4.445" y1="26.67" x2="5.08" y2="26.035" width="0.2032" layer="21"/>
<wire x1="5.08" y1="24.765" x2="4.445" y2="24.13" width="0.2032" layer="21"/>
<wire x1="4.445" y1="24.13" x2="3.175" y2="24.13" width="0.2032" layer="21"/>
<wire x1="3.175" y1="24.13" x2="2.54" y2="24.765" width="0.2032" layer="21"/>
<wire x1="0" y1="26.035" x2="0" y2="24.765" width="0.2032" layer="21"/>
<wire x1="0.635" y1="26.67" x2="0" y2="26.035" width="0.2032" layer="21"/>
<wire x1="0" y1="24.765" x2="0.635" y2="24.13" width="0.2032" layer="21"/>
<wire x1="1.905" y1="24.13" x2="0.635" y2="24.13" width="0.2032" layer="21"/>
<wire x1="18.415" y1="26.67" x2="19.685" y2="26.67" width="0.2032" layer="21"/>
<wire x1="19.685" y1="26.67" x2="20.32" y2="26.035" width="0.2032" layer="21"/>
<wire x1="20.32" y1="24.765" x2="19.685" y2="24.13" width="0.2032" layer="21"/>
<wire x1="18.415" y1="26.67" x2="17.78" y2="26.035" width="0.2032" layer="21"/>
<wire x1="17.78" y1="24.765" x2="18.415" y2="24.13" width="0.2032" layer="21"/>
<wire x1="19.685" y1="24.13" x2="18.415" y2="24.13" width="0.2032" layer="21"/>
<wire x1="20.955" y1="26.67" x2="22.225" y2="26.67" width="0.2032" layer="21"/>
<wire x1="22.225" y1="26.67" x2="22.86" y2="26.035" width="0.2032" layer="21"/>
<wire x1="22.86" y1="24.765" x2="22.225" y2="24.13" width="0.2032" layer="21"/>
<wire x1="20.955" y1="26.67" x2="20.32" y2="26.035" width="0.2032" layer="21"/>
<wire x1="20.32" y1="24.765" x2="20.955" y2="24.13" width="0.2032" layer="21"/>
<wire x1="22.225" y1="24.13" x2="20.955" y2="24.13" width="0.2032" layer="21"/>
<wire x1="23.495" y1="26.67" x2="24.765" y2="26.67" width="0.2032" layer="21"/>
<wire x1="24.765" y1="26.67" x2="25.4" y2="26.035" width="0.2032" layer="21"/>
<wire x1="25.4" y1="24.765" x2="24.765" y2="24.13" width="0.2032" layer="21"/>
<wire x1="23.495" y1="26.67" x2="22.86" y2="26.035" width="0.2032" layer="21"/>
<wire x1="22.86" y1="24.765" x2="23.495" y2="24.13" width="0.2032" layer="21"/>
<wire x1="24.765" y1="24.13" x2="23.495" y2="24.13" width="0.2032" layer="21"/>
<wire x1="26.035" y1="26.67" x2="27.305" y2="26.67" width="0.2032" layer="21"/>
<wire x1="27.305" y1="26.67" x2="27.94" y2="26.035" width="0.2032" layer="21"/>
<wire x1="27.94" y1="24.765" x2="27.305" y2="24.13" width="0.2032" layer="21"/>
<wire x1="26.035" y1="26.67" x2="25.4" y2="26.035" width="0.2032" layer="21"/>
<wire x1="25.4" y1="24.765" x2="26.035" y2="24.13" width="0.2032" layer="21"/>
<wire x1="27.305" y1="24.13" x2="26.035" y2="24.13" width="0.2032" layer="21"/>
<wire x1="28.575" y1="26.67" x2="29.845" y2="26.67" width="0.2032" layer="21"/>
<wire x1="29.845" y1="26.67" x2="30.48" y2="26.035" width="0.2032" layer="21"/>
<wire x1="30.48" y1="24.765" x2="29.845" y2="24.13" width="0.2032" layer="21"/>
<wire x1="28.575" y1="26.67" x2="27.94" y2="26.035" width="0.2032" layer="21"/>
<wire x1="27.94" y1="24.765" x2="28.575" y2="24.13" width="0.2032" layer="21"/>
<wire x1="29.845" y1="24.13" x2="28.575" y2="24.13" width="0.2032" layer="21"/>
<wire x1="31.115" y1="26.67" x2="32.385" y2="26.67" width="0.2032" layer="21"/>
<wire x1="32.385" y1="26.67" x2="33.02" y2="26.035" width="0.2032" layer="21"/>
<wire x1="33.02" y1="24.765" x2="32.385" y2="24.13" width="0.2032" layer="21"/>
<wire x1="31.115" y1="26.67" x2="30.48" y2="26.035" width="0.2032" layer="21"/>
<wire x1="30.48" y1="24.765" x2="31.115" y2="24.13" width="0.2032" layer="21"/>
<wire x1="32.385" y1="24.13" x2="31.115" y2="24.13" width="0.2032" layer="21"/>
<wire x1="33.655" y1="26.67" x2="34.925" y2="26.67" width="0.2032" layer="21"/>
<wire x1="34.925" y1="26.67" x2="35.56" y2="26.035" width="0.2032" layer="21"/>
<wire x1="35.56" y1="24.765" x2="34.925" y2="24.13" width="0.2032" layer="21"/>
<wire x1="33.655" y1="26.67" x2="33.02" y2="26.035" width="0.2032" layer="21"/>
<wire x1="33.02" y1="24.765" x2="33.655" y2="24.13" width="0.2032" layer="21"/>
<wire x1="34.925" y1="24.13" x2="33.655" y2="24.13" width="0.2032" layer="21"/>
<wire x1="36.195" y1="26.67" x2="37.465" y2="26.67" width="0.2032" layer="21"/>
<wire x1="37.465" y1="26.67" x2="38.1" y2="26.035" width="0.2032" layer="21"/>
<wire x1="38.1" y1="24.765" x2="37.465" y2="24.13" width="0.2032" layer="21"/>
<wire x1="36.195" y1="26.67" x2="35.56" y2="26.035" width="0.2032" layer="21"/>
<wire x1="35.56" y1="24.765" x2="36.195" y2="24.13" width="0.2032" layer="21"/>
<wire x1="37.465" y1="24.13" x2="36.195" y2="24.13" width="0.2032" layer="21"/>
<wire x1="38.735" y1="26.67" x2="40.005" y2="26.67" width="0.2032" layer="21"/>
<wire x1="40.005" y1="26.67" x2="40.64" y2="26.035" width="0.2032" layer="21"/>
<wire x1="40.64" y1="24.765" x2="40.005" y2="24.13" width="0.2032" layer="21"/>
<wire x1="38.735" y1="26.67" x2="38.1" y2="26.035" width="0.2032" layer="21"/>
<wire x1="38.1" y1="24.765" x2="38.735" y2="24.13" width="0.2032" layer="21"/>
<wire x1="40.005" y1="24.13" x2="38.735" y2="24.13" width="0.2032" layer="21"/>
<wire x1="41.275" y1="26.67" x2="42.545" y2="26.67" width="0.2032" layer="21"/>
<wire x1="42.545" y1="26.67" x2="43.18" y2="26.035" width="0.2032" layer="21"/>
<wire x1="43.18" y1="24.765" x2="42.545" y2="24.13" width="0.2032" layer="21"/>
<wire x1="41.275" y1="26.67" x2="40.64" y2="26.035" width="0.2032" layer="21"/>
<wire x1="40.64" y1="24.765" x2="41.275" y2="24.13" width="0.2032" layer="21"/>
<wire x1="42.545" y1="24.13" x2="41.275" y2="24.13" width="0.2032" layer="21"/>
<wire x1="43.815" y1="26.67" x2="45.085" y2="26.67" width="0.2032" layer="21"/>
<wire x1="45.085" y1="26.67" x2="45.72" y2="26.035" width="0.2032" layer="21"/>
<wire x1="45.72" y1="26.035" x2="45.72" y2="24.765" width="0.2032" layer="21"/>
<wire x1="45.72" y1="24.765" x2="45.085" y2="24.13" width="0.2032" layer="21"/>
<wire x1="43.815" y1="26.67" x2="43.18" y2="26.035" width="0.2032" layer="21"/>
<wire x1="43.18" y1="24.765" x2="43.815" y2="24.13" width="0.2032" layer="21"/>
<wire x1="45.085" y1="24.13" x2="43.815" y2="24.13" width="0.2032" layer="21"/>
<rectangle x1="16.256" y1="25.146" x2="16.764" y2="25.654" layer="51"/>
<rectangle x1="13.716" y1="25.146" x2="14.224" y2="25.654" layer="51"/>
<rectangle x1="11.176" y1="25.146" x2="11.684" y2="25.654" layer="51"/>
<rectangle x1="8.636" y1="25.146" x2="9.144" y2="25.654" layer="51"/>
<rectangle x1="6.096" y1="25.146" x2="6.604" y2="25.654" layer="51"/>
<rectangle x1="3.556" y1="25.146" x2="4.064" y2="25.654" layer="51"/>
<rectangle x1="1.016" y1="25.146" x2="1.524" y2="25.654" layer="51"/>
<rectangle x1="18.796" y1="25.146" x2="19.304" y2="25.654" layer="51"/>
<rectangle x1="21.336" y1="25.146" x2="21.844" y2="25.654" layer="51"/>
<rectangle x1="23.876" y1="25.146" x2="24.384" y2="25.654" layer="51"/>
<rectangle x1="26.416" y1="25.146" x2="26.924" y2="25.654" layer="51"/>
<rectangle x1="28.956" y1="25.146" x2="29.464" y2="25.654" layer="51"/>
<rectangle x1="31.496" y1="25.146" x2="32.004" y2="25.654" layer="51"/>
<rectangle x1="34.036" y1="25.146" x2="34.544" y2="25.654" layer="51"/>
<rectangle x1="36.576" y1="25.146" x2="37.084" y2="25.654" layer="51"/>
<rectangle x1="39.116" y1="25.146" x2="39.624" y2="25.654" layer="51"/>
<rectangle x1="41.656" y1="25.146" x2="42.164" y2="25.654" layer="51"/>
<rectangle x1="44.196" y1="25.146" x2="44.704" y2="25.654" layer="51"/>
<pad name="91" x="1.27" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="92" x="3.81" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="93" x="6.35" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="94" x="8.89" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="95" x="11.43" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="96" x="13.97" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="97" x="16.51" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="98" x="19.05" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="99" x="21.59" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="100" x="24.13" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="101" x="26.67" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="102" x="29.21" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="103" x="31.75" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="104" x="34.29" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="105" x="36.83" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="106" x="39.37" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="107" x="41.91" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<pad name="108" x="44.45" y="25.4" drill="0.889" diameter="1.7526" rot="R90"/>
<text x="-0.0762" y="27.2288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-2.54" y1="24.13" x2="45.72" y2="24.13" width="0.2032" layer="21"/>
<wire x1="45.72" y1="24.13" x2="48.26" y2="22.86" width="0.2032" layer="21"/>
<wire x1="48.26" y1="22.86" x2="48.26" y2="5.08" width="0.2032" layer="21"/>
<wire x1="48.26" y1="5.08" x2="45.72" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.72" y1="3.81" x2="-2.54" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="24.13" width="0.2032" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="ELECTRON">
<description>The Electron! Cellular capability in a useful dev package with an STM32F205</description>
<wire x1="-7.62" y1="-25.4" x2="10.16" y2="-25.4" width="0.254" layer="94"/>
<wire x1="10.16" y1="-25.4" x2="10.16" y2="22.86" width="0.254" layer="94"/>
<wire x1="10.16" y1="22.86" x2="-7.62" y2="22.86" width="0.254" layer="94"/>
<wire x1="-7.62" y1="22.86" x2="-7.62" y2="-25.4" width="0.254" layer="94"/>
<pin name="VIN" x="-10.16" y="20.32" length="short" direction="pwr"/>
<pin name="GND@2" x="-10.16" y="17.78" length="short" direction="pwr"/>
<pin name="TX" x="-10.16" y="15.24" length="short"/>
<pin name="RX" x="-10.16" y="12.7" length="short"/>
<pin name="WKP" x="-10.16" y="10.16" length="short"/>
<pin name="DAC" x="-10.16" y="7.62" length="short"/>
<pin name="A5" x="-10.16" y="5.08" length="short"/>
<pin name="A4" x="-10.16" y="2.54" length="short"/>
<pin name="A3" x="-10.16" y="0" length="short"/>
<pin name="A2" x="-10.16" y="-2.54" length="short"/>
<pin name="A1" x="-10.16" y="-5.08" length="short"/>
<pin name="A0" x="-10.16" y="-7.62" length="short"/>
<pin name="B5" x="-10.16" y="-10.16" length="short"/>
<pin name="B4" x="-10.16" y="-12.7" length="short"/>
<pin name="B3" x="-10.16" y="-15.24" length="short"/>
<pin name="B2" x="-10.16" y="-17.78" length="short"/>
<pin name="B1" x="-10.16" y="-20.32" length="short"/>
<pin name="B0" x="-10.16" y="-22.86" length="short"/>
<pin name="3V3" x="12.7" y="20.32" length="short" direction="pwr" rot="R180"/>
<pin name="RST" x="12.7" y="17.78" length="short" rot="R180"/>
<pin name="VBAT" x="12.7" y="15.24" length="short" direction="pas" rot="R180"/>
<pin name="GND" x="12.7" y="12.7" length="short" direction="pwr" rot="R180"/>
<pin name="D7" x="12.7" y="10.16" length="short" rot="R180"/>
<pin name="D6" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="D5" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="D4" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="D3" x="12.7" y="0" length="short" rot="R180"/>
<pin name="D2" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="D1" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="D0" x="12.7" y="-7.62" length="short" rot="R180"/>
<pin name="C5" x="12.7" y="-10.16" length="short" rot="R180"/>
<pin name="C4" x="12.7" y="-12.7" length="short" rot="R180"/>
<pin name="C3" x="12.7" y="-15.24" length="short" rot="R180"/>
<pin name="C2" x="12.7" y="-17.78" length="short" rot="R180"/>
<pin name="C1" x="12.7" y="-20.32" length="short" rot="R180"/>
<pin name="C0" x="12.7" y="-22.86" length="short" rot="R180"/>
<text x="-7.366" y="23.622" size="1.778" layer="95">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ELECTRON">
<gates>
<gate name="G$1" symbol="ELECTRON" x="-2.54" y="0"/>
</gates>
<devices>
<device name="DUALROW" package="ELECTRON_DUAL-ROW">
<connects>
<connect gate="G$1" pin="3V3" pad="3V3 55 91"/>
<connect gate="G$1" pin="A0" pad="12 48 A0"/>
<connect gate="G$1" pin="A1" pad="11 47 A1"/>
<connect gate="G$1" pin="A2" pad="10 46 A2"/>
<connect gate="G$1" pin="A3" pad="9 45 A3"/>
<connect gate="G$1" pin="A4" pad="8 44 A4"/>
<connect gate="G$1" pin="A5" pad="7 43 A5"/>
<connect gate="G$1" pin="B0" pad="18 54 B0"/>
<connect gate="G$1" pin="B1" pad="17 53 B1"/>
<connect gate="G$1" pin="B2" pad="16 52 B2"/>
<connect gate="G$1" pin="B3" pad="15 51 B3"/>
<connect gate="G$1" pin="B4" pad="14 50 B4"/>
<connect gate="G$1" pin="B5" pad="13 49 B5"/>
<connect gate="G$1" pin="C0" pad="72 108 C0"/>
<connect gate="G$1" pin="C1" pad="71 107 C1"/>
<connect gate="G$1" pin="C2" pad="70 106 C2"/>
<connect gate="G$1" pin="C3" pad="69 105 C3"/>
<connect gate="G$1" pin="C4" pad="68 104 C4"/>
<connect gate="G$1" pin="C5" pad="67 103 C5"/>
<connect gate="G$1" pin="D0" pad="66 102 D0"/>
<connect gate="G$1" pin="D1" pad="65 101 D1"/>
<connect gate="G$1" pin="D2" pad="64 100 D2"/>
<connect gate="G$1" pin="D3" pad="63 99 D3"/>
<connect gate="G$1" pin="D4" pad="62 98 D4"/>
<connect gate="G$1" pin="D5" pad="61 97 D5"/>
<connect gate="G$1" pin="D6" pad="60 96 D6"/>
<connect gate="G$1" pin="D7" pad="59 95 D7"/>
<connect gate="G$1" pin="DAC" pad="6 42 DAC"/>
<connect gate="G$1" pin="GND" pad="58 94 GND"/>
<connect gate="G$1" pin="GND@2" pad="2 38 GND@2"/>
<connect gate="G$1" pin="RST" pad="56 92 RST"/>
<connect gate="G$1" pin="RX" pad="4 40 RX"/>
<connect gate="G$1" pin="TX" pad="3 39 TX"/>
<connect gate="G$1" pin="VBAT" pad="57 93 VBAT"/>
<connect gate="G$1" pin="VIN" pad="1 37 VIN"/>
<connect gate="G$1" pin="WKP" pad="5 41 WKP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TinkerElectric">
<packages>
<package name="MODULE_1">
<wire x1="15.24" y1="-28.88" x2="15.24" y2="0" width="0.2032" layer="21"/>
<wire x1="15.24" y1="0" x2="-8.885990625" y2="0" width="0.2032" layer="21"/>
<wire x1="-8.885990625" y1="0" x2="-15.22" y2="-6.334009375" width="0.2032" layer="21"/>
<wire x1="-15.22" y1="-6.334009375" x2="-15.22" y2="-28.88" width="0.2032" layer="21"/>
<wire x1="-15.22" y1="-28.88" x2="15.24" y2="-28.88" width="0.2032" layer="21"/>
<pad name="B1" x="12.7" y="-8.89" drill="0.8" rot="R180"/>
<pad name="B2" x="12.7" y="-11.43" drill="0.8" rot="R180"/>
<pad name="B3" x="12.7" y="-13.97" drill="0.8" rot="R180"/>
<pad name="B4" x="12.7" y="-16.51" drill="0.8" rot="R180"/>
<pad name="B5" x="12.7" y="-19.05" drill="0.8" rot="R180"/>
<pad name="A1" x="-12.7" y="-8.89" drill="0.8" rot="R180"/>
<pad name="A2" x="-12.7" y="-11.43" drill="0.8" rot="R180"/>
<pad name="A3" x="-12.7" y="-13.97" drill="0.8" rot="R180"/>
<pad name="A4" x="-12.7" y="-16.51" drill="0.8" rot="R180"/>
<pad name="A5" x="-12.7" y="-19.05" drill="0.8" rot="R180"/>
<text x="-7.62" y="-20.32" size="1.778" layer="21" font="vector" rot="R180" align="top-right">&gt;NAME</text>
<text x="-7.62" y="-13.97" size="1.778" layer="21" font="vector" rot="R180" align="top-right">&gt;VALUE</text>
<hole x="12.7" y="-2.54" drill="3.2"/>
<polygon width="0.127" layer="41">
<vertex x="15.24" y="0"/>
<vertex x="10.16" y="0"/>
<vertex x="10.16" y="-5.08"/>
<vertex x="15.24" y="-5.08"/>
</polygon>
<polygon width="0.127" layer="42">
<vertex x="10.16" y="-5.08"/>
<vertex x="15.24" y="-5.08"/>
<vertex x="15.24" y="0"/>
<vertex x="10.16" y="0"/>
</polygon>
</package>
<package name="SOLENOID_THIN">
<text x="-3.89" y="-9.27" size="1.27" layer="21">&gt;NAME</text>
<text x="-3.89" y="-11.83" size="1.27" layer="21">&gt;VALUE</text>
<wire x1="-10" y1="-18" x2="-10" y2="14.094878125" width="0.127" layer="21"/>
<wire x1="-10" y1="14.094878125" x2="-6.094878125" y2="18" width="0.127" layer="21"/>
<wire x1="-6.094878125" y1="18" x2="12" y2="18" width="0.127" layer="21"/>
<wire x1="12" y1="18" x2="12" y2="-18" width="0.127" layer="21"/>
<wire x1="12" y1="-18" x2="-10" y2="-18" width="0.127" layer="21"/>
<pad name="A3" x="0" y="15" drill="0.8"/>
<pad name="A2" x="-2.54" y="15" drill="0.8"/>
<pad name="A1" x="-5.08" y="15" drill="0.8"/>
<pad name="A4" x="2.54" y="15" drill="0.8"/>
<pad name="A5" x="5.08" y="15" drill="0.8"/>
<pad name="B3" x="0" y="-15" drill="0.8"/>
<pad name="B2" x="-2.54" y="-15" drill="0.8"/>
<pad name="B1" x="-5.08" y="-15" drill="0.8"/>
<pad name="B4" x="2.54" y="-15" drill="0.8"/>
<pad name="B5" x="5.08" y="-15" drill="0.8"/>
<hole x="9.525" y="15.24" drill="3.2"/>
</package>
<package name="02P">
<description>&lt;b&gt;AMP QUICK CONNECTOR&lt;/b&gt;</description>
<text x="-3.316" y="3.4064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.286" y="5.1646" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<smd name="P$2" x="5.08" y="2.54" dx="3.048" dy="7.62" layer="1"/>
<smd name="P$3" x="-5.08" y="2.54" dx="3.048" dy="7.62" layer="1"/>
</package>
<package name="1X02_5MM">
<pad name="P$1" x="0" y="0" drill="1.143" diameter="2.1844" shape="square"/>
<pad name="P$2" x="5" y="0" drill="1.143" diameter="2.1844" shape="long" rot="R90"/>
<wire x1="-2.5" y1="3.85" x2="-2.5" y2="-3.85" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-3.85" x2="7.5" y2="-3.85" width="0.127" layer="21"/>
<wire x1="7.5" y1="-3.85" x2="7.5" y2="3.85" width="0.127" layer="21"/>
<wire x1="7.5" y1="3.85" x2="-2.5" y2="3.85" width="0.127" layer="21"/>
<text x="-1.27" y="2.54" size="1.27" layer="21">&gt;VALUE</text>
<text x="-1.27" y="-3.81" size="1.27" layer="21">&gt;NAME</text>
</package>
<package name="PN453516">
<description>PN453516
PolyNova
https://www.brenclosures.com.au/pn.htm</description>
<wire x1="206" y1="153.5" x2="-185.519303125" y2="153.5" width="0.127" layer="21"/>
<wire x1="-185.519303125" y1="153.5" x2="-186" y2="153.019303125" width="0.127" layer="21" curve="90"/>
<wire x1="206" y1="153.5" x2="206" y2="-153.5" width="0.127" layer="21"/>
<wire x1="206" y1="-153.5" x2="-206" y2="-153.5" width="0.127" layer="21"/>
<wire x1="-205.519303125" y1="133.5" x2="-206" y2="133.019303125" width="0.127" layer="21" curve="90"/>
<wire x1="-206" y1="133.019303125" x2="-206" y2="-153.5" width="0.127" layer="21"/>
<wire x1="-175" y1="123" x2="175" y2="123" width="0.127" layer="21"/>
<wire x1="175" y1="123" x2="175" y2="-123" width="0.127" layer="21"/>
<wire x1="175" y1="-123" x2="-175" y2="-123" width="0.127" layer="21"/>
<wire x1="-175" y1="-123" x2="-175" y2="123" width="0.127" layer="21"/>
<wire x1="-186" y1="153.019303125" x2="-205.519303125" y2="133.5" width="0.127" layer="21" curve="-90"/>
<circle x="-182.5" y="137.5" radius="4" width="0.127" layer="21"/>
<circle x="182.5" y="137.5" radius="4" width="0.127" layer="21"/>
<circle x="182.5" y="-137.5" radius="4" width="0.127" layer="21"/>
<circle x="-182.5" y="-137.5" radius="4" width="0.127" layer="21"/>
</package>
<package name="1554F">
<wire x1="48.603" y1="0.09644375" x2="48.612771875" y2="0.190603125" width="0.1" layer="48"/>
<wire x1="48.612771875" y1="0.190603125" x2="48.63138125" y2="0.28154375" width="0.1" layer="48"/>
<wire x1="48.63138125" y1="0.28154375" x2="48.649990625" y2="0.3724875" width="0.1" layer="48"/>
<wire x1="48.649990625" y1="0.3724875" x2="48.6774375" y2="0.460209375" width="0.1" layer="48"/>
<wire x1="48.6774375" y1="0.460209375" x2="48.712784375" y2="0.543775" width="0.1" layer="48"/>
<wire x1="48.712784375" y1="0.543775" x2="48.748128125" y2="0.62734375" width="0.1" layer="48"/>
<wire x1="48.748128125" y1="0.62734375" x2="48.791375" y2="0.70675625" width="0.1" layer="48"/>
<wire x1="48.791375" y1="0.70675625" x2="48.8415875" y2="0.781075" width="0.1" layer="48"/>
<wire x1="48.8415875" y1="0.781075" x2="48.891796875" y2="0.855396875" width="0.1" layer="48"/>
<wire x1="48.891796875" y1="0.855396875" x2="48.94896875" y2="0.924625" width="0.1" layer="48"/>
<wire x1="48.94896875" y1="0.924625" x2="49.075375" y2="1.05103125" width="0.1" layer="48"/>
<wire x1="49.075375" y1="1.05103125" x2="49.144603125" y2="1.108203125" width="0.1" layer="48"/>
<wire x1="49.144603125" y1="1.108203125" x2="49.218925" y2="1.1584125" width="0.1" layer="48"/>
<wire x1="49.218925" y1="1.1584125" x2="49.29324375" y2="1.208625" width="0.1" layer="48"/>
<wire x1="49.29324375" y1="1.208625" x2="49.37265625" y2="1.251871875" width="0.1" layer="48"/>
<wire x1="49.37265625" y1="1.251871875" x2="49.456225" y2="1.287215625" width="0.1" layer="48"/>
<wire x1="49.456225" y1="1.287215625" x2="49.539790625" y2="1.3225625" width="0.1" layer="48"/>
<wire x1="49.539790625" y1="1.3225625" x2="49.6275125" y2="1.350009375" width="0.1" layer="48"/>
<wire x1="49.6275125" y1="1.350009375" x2="49.71845625" y2="1.36861875" width="0.1" layer="48"/>
<wire x1="49.71845625" y1="1.36861875" x2="49.809396875" y2="1.387228125" width="0.1" layer="48"/>
<wire x1="49.809396875" y1="1.387228125" x2="49.90355625" y2="1.397" width="0.1" layer="48"/>
<wire x1="49.90355625" y1="1.397" x2="50.09644375" y2="1.397" width="0.1" layer="48"/>
<wire x1="50.09644375" y1="1.397" x2="50.190603125" y2="1.387228125" width="0.1" layer="48"/>
<wire x1="50.190603125" y1="1.387228125" x2="50.28154375" y2="1.36861875" width="0.1" layer="48"/>
<wire x1="50.28154375" y1="1.36861875" x2="50.3724875" y2="1.350009375" width="0.1" layer="48"/>
<wire x1="50.3724875" y1="1.350009375" x2="50.460209375" y2="1.3225625" width="0.1" layer="48"/>
<wire x1="50.460209375" y1="1.3225625" x2="50.543775" y2="1.287215625" width="0.1" layer="48"/>
<wire x1="50.543775" y1="1.287215625" x2="50.62734375" y2="1.251871875" width="0.1" layer="48"/>
<wire x1="50.62734375" y1="1.251871875" x2="50.70675625" y2="1.208625" width="0.1" layer="48"/>
<wire x1="50.70675625" y1="1.208625" x2="50.781075" y2="1.1584125" width="0.1" layer="48"/>
<wire x1="50.781075" y1="1.1584125" x2="50.855396875" y2="1.108203125" width="0.1" layer="48"/>
<wire x1="50.855396875" y1="1.108203125" x2="50.924625" y2="1.05103125" width="0.1" layer="48"/>
<wire x1="50.924625" y1="1.05103125" x2="51.05103125" y2="0.924625" width="0.1" layer="48"/>
<wire x1="51.05103125" y1="0.924625" x2="51.108203125" y2="0.855396875" width="0.1" layer="48"/>
<wire x1="51.108203125" y1="0.855396875" x2="51.1584125" y2="0.781075" width="0.1" layer="48"/>
<wire x1="51.1584125" y1="0.781075" x2="51.208625" y2="0.70675625" width="0.1" layer="48"/>
<wire x1="51.208625" y1="0.70675625" x2="51.251871875" y2="0.62734375" width="0.1" layer="48"/>
<wire x1="51.251871875" y1="0.62734375" x2="51.287215625" y2="0.543775" width="0.1" layer="48"/>
<wire x1="51.287215625" y1="0.543775" x2="51.3225625" y2="0.460209375" width="0.1" layer="48"/>
<wire x1="51.3225625" y1="0.460209375" x2="51.350009375" y2="0.3724875" width="0.1" layer="48"/>
<wire x1="51.350009375" y1="0.3724875" x2="51.36861875" y2="0.28154375" width="0.1" layer="48"/>
<wire x1="51.36861875" y1="0.28154375" x2="51.387228125" y2="0.190603125" width="0.1" layer="48"/>
<wire x1="51.387228125" y1="0.190603125" x2="51.397" y2="0.09644375" width="0.1" layer="48"/>
<wire x1="51.397" y1="0.09644375" x2="51.397" y2="-0.09644375" width="0.1" layer="48"/>
<wire x1="51.397" y1="-0.09644375" x2="51.387228125" y2="-0.190603125" width="0.1" layer="48"/>
<wire x1="51.387228125" y1="-0.190603125" x2="51.36861875" y2="-0.28154375" width="0.1" layer="48"/>
<wire x1="51.36861875" y1="-0.28154375" x2="51.350009375" y2="-0.3724875" width="0.1" layer="48"/>
<wire x1="51.350009375" y1="-0.3724875" x2="51.3225625" y2="-0.460209375" width="0.1" layer="48"/>
<wire x1="51.3225625" y1="-0.460209375" x2="51.287215625" y2="-0.543775" width="0.1" layer="48"/>
<wire x1="51.287215625" y1="-0.543775" x2="51.251871875" y2="-0.62734375" width="0.1" layer="48"/>
<wire x1="51.251871875" y1="-0.62734375" x2="51.208625" y2="-0.70675625" width="0.1" layer="48"/>
<wire x1="51.208625" y1="-0.70675625" x2="51.1584125" y2="-0.781075" width="0.1" layer="48"/>
<wire x1="51.1584125" y1="-0.781075" x2="51.108203125" y2="-0.855396875" width="0.1" layer="48"/>
<wire x1="51.108203125" y1="-0.855396875" x2="51.05103125" y2="-0.924625" width="0.1" layer="48"/>
<wire x1="51.05103125" y1="-0.924625" x2="50.924625" y2="-1.05103125" width="0.1" layer="48"/>
<wire x1="50.924625" y1="-1.05103125" x2="50.855396875" y2="-1.108203125" width="0.1" layer="48"/>
<wire x1="50.855396875" y1="-1.108203125" x2="50.781075" y2="-1.1584125" width="0.1" layer="48"/>
<wire x1="50.781075" y1="-1.1584125" x2="50.70675625" y2="-1.208625" width="0.1" layer="48"/>
<wire x1="50.70675625" y1="-1.208625" x2="50.62734375" y2="-1.251871875" width="0.1" layer="48"/>
<wire x1="50.62734375" y1="-1.251871875" x2="50.543775" y2="-1.287215625" width="0.1" layer="48"/>
<wire x1="50.543775" y1="-1.287215625" x2="50.460209375" y2="-1.3225625" width="0.1" layer="48"/>
<wire x1="50.460209375" y1="-1.3225625" x2="50.3724875" y2="-1.350009375" width="0.1" layer="48"/>
<wire x1="50.3724875" y1="-1.350009375" x2="50.28154375" y2="-1.36861875" width="0.1" layer="48"/>
<wire x1="50.28154375" y1="-1.36861875" x2="50.190603125" y2="-1.387228125" width="0.1" layer="48"/>
<wire x1="50.190603125" y1="-1.387228125" x2="50.09644375" y2="-1.397" width="0.1" layer="48"/>
<wire x1="50.09644375" y1="-1.397" x2="49.90355625" y2="-1.397" width="0.1" layer="48"/>
<wire x1="49.90355625" y1="-1.397" x2="49.809396875" y2="-1.387228125" width="0.1" layer="48"/>
<wire x1="49.809396875" y1="-1.387228125" x2="49.71845625" y2="-1.36861875" width="0.1" layer="48"/>
<wire x1="49.71845625" y1="-1.36861875" x2="49.6275125" y2="-1.350009375" width="0.1" layer="48"/>
<wire x1="49.6275125" y1="-1.350009375" x2="49.539790625" y2="-1.3225625" width="0.1" layer="48"/>
<wire x1="49.539790625" y1="-1.3225625" x2="49.456225" y2="-1.287215625" width="0.1" layer="48"/>
<wire x1="49.456225" y1="-1.287215625" x2="49.37265625" y2="-1.251871875" width="0.1" layer="48"/>
<wire x1="49.37265625" y1="-1.251871875" x2="49.29324375" y2="-1.208625" width="0.1" layer="48"/>
<wire x1="49.29324375" y1="-1.208625" x2="49.218925" y2="-1.1584125" width="0.1" layer="48"/>
<wire x1="49.218925" y1="-1.1584125" x2="49.144603125" y2="-1.108203125" width="0.1" layer="48"/>
<wire x1="49.144603125" y1="-1.108203125" x2="49.075375" y2="-1.05103125" width="0.1" layer="48"/>
<wire x1="49.075375" y1="-1.05103125" x2="48.94896875" y2="-0.924625" width="0.1" layer="48"/>
<wire x1="48.94896875" y1="-0.924625" x2="48.891796875" y2="-0.855396875" width="0.1" layer="48"/>
<wire x1="48.891796875" y1="-0.855396875" x2="48.8415875" y2="-0.781075" width="0.1" layer="48"/>
<wire x1="48.8415875" y1="-0.781075" x2="48.791375" y2="-0.70675625" width="0.1" layer="48"/>
<wire x1="48.791375" y1="-0.70675625" x2="48.748128125" y2="-0.62734375" width="0.1" layer="48"/>
<wire x1="48.748128125" y1="-0.62734375" x2="48.712784375" y2="-0.543775" width="0.1" layer="48"/>
<wire x1="48.712784375" y1="-0.543775" x2="48.6774375" y2="-0.460209375" width="0.1" layer="48"/>
<wire x1="48.6774375" y1="-0.460209375" x2="48.649990625" y2="-0.3724875" width="0.1" layer="48"/>
<wire x1="48.649990625" y1="-0.3724875" x2="48.63138125" y2="-0.28154375" width="0.1" layer="48"/>
<wire x1="48.63138125" y1="-0.28154375" x2="48.612771875" y2="-0.190603125" width="0.1" layer="48"/>
<wire x1="48.612771875" y1="-0.190603125" x2="48.603" y2="-0.09644375" width="0.1" layer="48"/>
<wire x1="48.603" y1="-0.09644375" x2="48.603" y2="0.09644375" width="0.1" layer="48"/>
<wire x1="-51.397" y1="0.09644375" x2="-51.387228125" y2="0.190603125" width="0.1" layer="48"/>
<wire x1="-51.387228125" y1="0.190603125" x2="-51.36861875" y2="0.28154375" width="0.1" layer="48"/>
<wire x1="-51.36861875" y1="0.28154375" x2="-51.350009375" y2="0.3724875" width="0.1" layer="48"/>
<wire x1="-51.350009375" y1="0.3724875" x2="-51.3225625" y2="0.460209375" width="0.1" layer="48"/>
<wire x1="-51.3225625" y1="0.460209375" x2="-51.287215625" y2="0.543775" width="0.1" layer="48"/>
<wire x1="-51.287215625" y1="0.543775" x2="-51.251871875" y2="0.62734375" width="0.1" layer="48"/>
<wire x1="-51.251871875" y1="0.62734375" x2="-51.208625" y2="0.70675625" width="0.1" layer="48"/>
<wire x1="-51.208625" y1="0.70675625" x2="-51.1584125" y2="0.781075" width="0.1" layer="48"/>
<wire x1="-51.1584125" y1="0.781075" x2="-51.108203125" y2="0.855396875" width="0.1" layer="48"/>
<wire x1="-51.108203125" y1="0.855396875" x2="-51.05103125" y2="0.924625" width="0.1" layer="48"/>
<wire x1="-51.05103125" y1="0.924625" x2="-50.924625" y2="1.05103125" width="0.1" layer="48"/>
<wire x1="-50.924625" y1="1.05103125" x2="-50.855396875" y2="1.108203125" width="0.1" layer="48"/>
<wire x1="-50.855396875" y1="1.108203125" x2="-50.781075" y2="1.1584125" width="0.1" layer="48"/>
<wire x1="-50.781075" y1="1.1584125" x2="-50.70675625" y2="1.208625" width="0.1" layer="48"/>
<wire x1="-50.70675625" y1="1.208625" x2="-50.62734375" y2="1.251871875" width="0.1" layer="48"/>
<wire x1="-50.62734375" y1="1.251871875" x2="-50.543775" y2="1.287215625" width="0.1" layer="48"/>
<wire x1="-50.543775" y1="1.287215625" x2="-50.460209375" y2="1.3225625" width="0.1" layer="48"/>
<wire x1="-50.460209375" y1="1.3225625" x2="-50.3724875" y2="1.350009375" width="0.1" layer="48"/>
<wire x1="-50.3724875" y1="1.350009375" x2="-50.28154375" y2="1.36861875" width="0.1" layer="48"/>
<wire x1="-50.28154375" y1="1.36861875" x2="-50.190603125" y2="1.387228125" width="0.1" layer="48"/>
<wire x1="-50.190603125" y1="1.387228125" x2="-50.09644375" y2="1.397" width="0.1" layer="48"/>
<wire x1="-50.09644375" y1="1.397" x2="-49.90355625" y2="1.397" width="0.1" layer="48"/>
<wire x1="-49.90355625" y1="1.397" x2="-49.809396875" y2="1.387228125" width="0.1" layer="48"/>
<wire x1="-49.809396875" y1="1.387228125" x2="-49.71845625" y2="1.36861875" width="0.1" layer="48"/>
<wire x1="-49.71845625" y1="1.36861875" x2="-49.6275125" y2="1.350009375" width="0.1" layer="48"/>
<wire x1="-49.6275125" y1="1.350009375" x2="-49.539790625" y2="1.3225625" width="0.1" layer="48"/>
<wire x1="-49.539790625" y1="1.3225625" x2="-49.456225" y2="1.287215625" width="0.1" layer="48"/>
<wire x1="-49.456225" y1="1.287215625" x2="-49.37265625" y2="1.251871875" width="0.1" layer="48"/>
<wire x1="-49.37265625" y1="1.251871875" x2="-49.29324375" y2="1.208625" width="0.1" layer="48"/>
<wire x1="-49.29324375" y1="1.208625" x2="-49.218925" y2="1.1584125" width="0.1" layer="48"/>
<wire x1="-49.218925" y1="1.1584125" x2="-49.144603125" y2="1.108203125" width="0.1" layer="48"/>
<wire x1="-49.144603125" y1="1.108203125" x2="-49.075375" y2="1.05103125" width="0.1" layer="48"/>
<wire x1="-49.075375" y1="1.05103125" x2="-48.94896875" y2="0.924625" width="0.1" layer="48"/>
<wire x1="-48.94896875" y1="0.924625" x2="-48.891796875" y2="0.855396875" width="0.1" layer="48"/>
<wire x1="-48.891796875" y1="0.855396875" x2="-48.8415875" y2="0.781075" width="0.1" layer="48"/>
<wire x1="-48.8415875" y1="0.781075" x2="-48.791375" y2="0.70675625" width="0.1" layer="48"/>
<wire x1="-48.791375" y1="0.70675625" x2="-48.748128125" y2="0.62734375" width="0.1" layer="48"/>
<wire x1="-48.748128125" y1="0.62734375" x2="-48.712784375" y2="0.543775" width="0.1" layer="48"/>
<wire x1="-48.712784375" y1="0.543775" x2="-48.6774375" y2="0.460209375" width="0.1" layer="48"/>
<wire x1="-48.6774375" y1="0.460209375" x2="-48.649990625" y2="0.3724875" width="0.1" layer="48"/>
<wire x1="-48.649990625" y1="0.3724875" x2="-48.63138125" y2="0.28154375" width="0.1" layer="48"/>
<wire x1="-48.63138125" y1="0.28154375" x2="-48.612771875" y2="0.190603125" width="0.1" layer="48"/>
<wire x1="-48.612771875" y1="0.190603125" x2="-48.603" y2="0.09644375" width="0.1" layer="48"/>
<wire x1="-48.603" y1="0.09644375" x2="-48.603" y2="-0.09644375" width="0.1" layer="48"/>
<wire x1="-48.603" y1="-0.09644375" x2="-48.612771875" y2="-0.190603125" width="0.1" layer="48"/>
<wire x1="-48.612771875" y1="-0.190603125" x2="-48.63138125" y2="-0.28154375" width="0.1" layer="48"/>
<wire x1="-48.63138125" y1="-0.28154375" x2="-48.649990625" y2="-0.3724875" width="0.1" layer="48"/>
<wire x1="-48.649990625" y1="-0.3724875" x2="-48.6774375" y2="-0.460209375" width="0.1" layer="48"/>
<wire x1="-48.6774375" y1="-0.460209375" x2="-48.712784375" y2="-0.543775" width="0.1" layer="48"/>
<wire x1="-48.712784375" y1="-0.543775" x2="-48.748128125" y2="-0.62734375" width="0.1" layer="48"/>
<wire x1="-48.748128125" y1="-0.62734375" x2="-48.791375" y2="-0.70675625" width="0.1" layer="48"/>
<wire x1="-48.791375" y1="-0.70675625" x2="-48.8415875" y2="-0.781075" width="0.1" layer="48"/>
<wire x1="-48.8415875" y1="-0.781075" x2="-48.891796875" y2="-0.855396875" width="0.1" layer="48"/>
<wire x1="-48.891796875" y1="-0.855396875" x2="-48.94896875" y2="-0.924625" width="0.1" layer="48"/>
<wire x1="-48.94896875" y1="-0.924625" x2="-49.075375" y2="-1.05103125" width="0.1" layer="48"/>
<wire x1="-49.075375" y1="-1.05103125" x2="-49.144603125" y2="-1.108203125" width="0.1" layer="48"/>
<wire x1="-49.144603125" y1="-1.108203125" x2="-49.218925" y2="-1.1584125" width="0.1" layer="48"/>
<wire x1="-49.218925" y1="-1.1584125" x2="-49.29324375" y2="-1.208625" width="0.1" layer="48"/>
<wire x1="-49.29324375" y1="-1.208625" x2="-49.37265625" y2="-1.251871875" width="0.1" layer="48"/>
<wire x1="-49.37265625" y1="-1.251871875" x2="-49.456225" y2="-1.287215625" width="0.1" layer="48"/>
<wire x1="-49.456225" y1="-1.287215625" x2="-49.539790625" y2="-1.3225625" width="0.1" layer="48"/>
<wire x1="-49.539790625" y1="-1.3225625" x2="-49.6275125" y2="-1.350009375" width="0.1" layer="48"/>
<wire x1="-49.6275125" y1="-1.350009375" x2="-49.71845625" y2="-1.36861875" width="0.1" layer="48"/>
<wire x1="-49.71845625" y1="-1.36861875" x2="-49.809396875" y2="-1.387228125" width="0.1" layer="48"/>
<wire x1="-49.809396875" y1="-1.387228125" x2="-49.90355625" y2="-1.397" width="0.1" layer="48"/>
<wire x1="-49.90355625" y1="-1.397" x2="-50.09644375" y2="-1.397" width="0.1" layer="48"/>
<wire x1="-50.09644375" y1="-1.397" x2="-50.190603125" y2="-1.387228125" width="0.1" layer="48"/>
<wire x1="-50.190603125" y1="-1.387228125" x2="-50.28154375" y2="-1.36861875" width="0.1" layer="48"/>
<wire x1="-50.28154375" y1="-1.36861875" x2="-50.3724875" y2="-1.350009375" width="0.1" layer="48"/>
<wire x1="-50.3724875" y1="-1.350009375" x2="-50.460209375" y2="-1.3225625" width="0.1" layer="48"/>
<wire x1="-50.460209375" y1="-1.3225625" x2="-50.543775" y2="-1.287215625" width="0.1" layer="48"/>
<wire x1="-50.543775" y1="-1.287215625" x2="-50.62734375" y2="-1.251871875" width="0.1" layer="48"/>
<wire x1="-50.62734375" y1="-1.251871875" x2="-50.70675625" y2="-1.208625" width="0.1" layer="48"/>
<wire x1="-50.70675625" y1="-1.208625" x2="-50.781075" y2="-1.1584125" width="0.1" layer="48"/>
<wire x1="-50.781075" y1="-1.1584125" x2="-50.855396875" y2="-1.108203125" width="0.1" layer="48"/>
<wire x1="-50.855396875" y1="-1.108203125" x2="-50.924625" y2="-1.05103125" width="0.1" layer="48"/>
<wire x1="-50.924625" y1="-1.05103125" x2="-51.05103125" y2="-0.924625" width="0.1" layer="48"/>
<wire x1="-51.05103125" y1="-0.924625" x2="-51.108203125" y2="-0.855396875" width="0.1" layer="48"/>
<wire x1="-51.108203125" y1="-0.855396875" x2="-51.1584125" y2="-0.781075" width="0.1" layer="48"/>
<wire x1="-51.1584125" y1="-0.781075" x2="-51.208625" y2="-0.70675625" width="0.1" layer="48"/>
<wire x1="-51.208625" y1="-0.70675625" x2="-51.251871875" y2="-0.62734375" width="0.1" layer="48"/>
<wire x1="-51.251871875" y1="-0.62734375" x2="-51.287215625" y2="-0.543775" width="0.1" layer="48"/>
<wire x1="-51.287215625" y1="-0.543775" x2="-51.3225625" y2="-0.460209375" width="0.1" layer="48"/>
<wire x1="-51.3225625" y1="-0.460209375" x2="-51.350009375" y2="-0.3724875" width="0.1" layer="48"/>
<wire x1="-51.350009375" y1="-0.3724875" x2="-51.36861875" y2="-0.28154375" width="0.1" layer="48"/>
<wire x1="-51.36861875" y1="-0.28154375" x2="-51.387228125" y2="-0.190603125" width="0.1" layer="48"/>
<wire x1="-51.387228125" y1="-0.190603125" x2="-51.397" y2="-0.09644375" width="0.1" layer="48"/>
<wire x1="-51.397" y1="-0.09644375" x2="-51.397" y2="0.09644375" width="0.1" layer="48"/>
<wire x1="-45.8724" y1="-27.11085" x2="-45.93068125" y2="-26.5493125" width="0.1" layer="48"/>
<wire x1="-45.93068125" y1="-26.5493125" x2="-46.1526375" y2="-25.46463125" width="0.1" layer="48"/>
<wire x1="-46.1526375" y1="-25.46463125" x2="-46.316315625" y2="-24.9414875" width="0.1" layer="48"/>
<wire x1="-46.316315625" y1="-24.9414875" x2="-46.737896875" y2="-23.94475625" width="0.1" layer="48"/>
<wire x1="-46.737896875" y1="-23.94475625" x2="-46.995803125" y2="-23.47116875" width="0.1" layer="48"/>
<wire x1="-46.995803125" y1="-23.47116875" x2="-47.2952375" y2="-23.027946875" width="0.1" layer="48"/>
<wire x1="-47.2952375" y1="-23.027946875" x2="-47.594675" y2="-22.584725" width="0.1" layer="48"/>
<wire x1="-47.594675" y1="-22.584725" x2="-47.9356375" y2="-22.171865625" width="0.1" layer="48"/>
<wire x1="-47.9356375" y1="-22.171865625" x2="-48.312553125" y2="-21.794953125" width="0.1" layer="48"/>
<wire x1="-48.312553125" y1="-21.794953125" x2="-48.689465625" y2="-21.4180375" width="0.1" layer="48"/>
<wire x1="-48.689465625" y1="-21.4180375" x2="-49.102325" y2="-21.077075" width="0.1" layer="48"/>
<wire x1="-49.102325" y1="-21.077075" x2="-49.545546875" y2="-20.7776375" width="0.1" layer="48"/>
<wire x1="-49.545546875" y1="-20.7776375" x2="-49.98876875" y2="-20.478203125" width="0.1" layer="48"/>
<wire x1="-49.98876875" y1="-20.478203125" x2="-50.46235625" y2="-20.220296875" width="0.1" layer="48"/>
<wire x1="-50.46235625" y1="-20.220296875" x2="-51.4590875" y2="-19.798715625" width="0.1" layer="48"/>
<wire x1="-51.4590875" y1="-19.798715625" x2="-51.98223125" y2="-19.6350375" width="0.1" layer="48"/>
<wire x1="-51.98223125" y1="-19.6350375" x2="-53.0669125" y2="-19.41308125" width="0.1" layer="48"/>
<wire x1="-53.0669125" y1="-19.41308125" x2="-53.62845" y2="-19.3548" width="0.1" layer="48"/>
<wire x1="-53.62845" y1="-19.3548" x2="-54.2036" y2="-19.3548" width="0.1" layer="48"/>
<wire x1="-54.2036" y1="-19.3548" x2="-54.2036" y2="19.3548" width="0.1" layer="48"/>
<wire x1="-54.2036" y1="19.3548" x2="-53.62845" y2="19.3548" width="0.1" layer="48"/>
<wire x1="-53.62845" y1="19.3548" x2="-53.0669125" y2="19.41308125" width="0.1" layer="48"/>
<wire x1="-53.0669125" y1="19.41308125" x2="-51.98223125" y2="19.6350375" width="0.1" layer="48"/>
<wire x1="-51.98223125" y1="19.6350375" x2="-51.4590875" y2="19.798715625" width="0.1" layer="48"/>
<wire x1="-51.4590875" y1="19.798715625" x2="-50.46235625" y2="20.220296875" width="0.1" layer="48"/>
<wire x1="-50.46235625" y1="20.220296875" x2="-49.98876875" y2="20.478203125" width="0.1" layer="48"/>
<wire x1="-49.98876875" y1="20.478203125" x2="-49.102325" y2="21.077071875" width="0.1" layer="48"/>
<wire x1="-49.102325" y1="21.077071875" x2="-48.689465625" y2="21.4180375" width="0.1" layer="48"/>
<wire x1="-48.689465625" y1="21.4180375" x2="-48.312553125" y2="21.794953125" width="0.1" layer="48"/>
<wire x1="-48.312553125" y1="21.794953125" x2="-47.9356375" y2="22.171865625" width="0.1" layer="48"/>
<wire x1="-47.9356375" y1="22.171865625" x2="-47.594675" y2="22.584725" width="0.1" layer="48"/>
<wire x1="-47.594675" y1="22.584725" x2="-47.2952375" y2="23.027946875" width="0.1" layer="48"/>
<wire x1="-47.2952375" y1="23.027946875" x2="-46.995803125" y2="23.47116875" width="0.1" layer="48"/>
<wire x1="-46.995803125" y1="23.47116875" x2="-46.737896875" y2="23.94475625" width="0.1" layer="48"/>
<wire x1="-46.737896875" y1="23.94475625" x2="-46.316315625" y2="24.9414875" width="0.1" layer="48"/>
<wire x1="-46.316315625" y1="24.9414875" x2="-46.1526375" y2="25.46463125" width="0.1" layer="48"/>
<wire x1="-46.1526375" y1="25.46463125" x2="-45.93068125" y2="26.5493125" width="0.1" layer="48"/>
<wire x1="-45.93068125" y1="26.5493125" x2="-45.8724" y2="27.11085" width="0.1" layer="48"/>
<wire x1="-45.8724" y1="27.11085" x2="-45.8724" y2="38.735" width="0.1" layer="48"/>
<wire x1="-45.8724" y1="38.735" x2="45.8724" y2="38.735" width="0.1" layer="20"/>
<wire x1="45.8724" y1="38.735" x2="45.8724" y2="27.11085" width="0.1" layer="48"/>
<wire x1="45.8724" y1="27.11085" x2="45.93068125" y2="26.5493125" width="0.1" layer="48"/>
<wire x1="45.93068125" y1="26.5493125" x2="46.1526375" y2="25.46463125" width="0.1" layer="48"/>
<wire x1="46.1526375" y1="25.46463125" x2="46.316315625" y2="24.9414875" width="0.1" layer="48"/>
<wire x1="46.316315625" y1="24.9414875" x2="46.737896875" y2="23.94475625" width="0.1" layer="48"/>
<wire x1="46.737896875" y1="23.94475625" x2="46.995803125" y2="23.47116875" width="0.1" layer="48"/>
<wire x1="46.995803125" y1="23.47116875" x2="47.594671875" y2="22.584725" width="0.1" layer="48"/>
<wire x1="47.594671875" y1="22.584725" x2="47.9356375" y2="22.171865625" width="0.1" layer="48"/>
<wire x1="47.9356375" y1="22.171865625" x2="48.312553125" y2="21.794953125" width="0.1" layer="48"/>
<wire x1="48.312553125" y1="21.794953125" x2="48.689465625" y2="21.4180375" width="0.1" layer="48"/>
<wire x1="48.689465625" y1="21.4180375" x2="49.102325" y2="21.077071875" width="0.1" layer="48"/>
<wire x1="49.102325" y1="21.077071875" x2="49.98876875" y2="20.478203125" width="0.1" layer="48"/>
<wire x1="49.98876875" y1="20.478203125" x2="50.46235625" y2="20.220296875" width="0.1" layer="48"/>
<wire x1="50.46235625" y1="20.220296875" x2="51.4590875" y2="19.798715625" width="0.1" layer="48"/>
<wire x1="51.4590875" y1="19.798715625" x2="51.98223125" y2="19.6350375" width="0.1" layer="48"/>
<wire x1="51.98223125" y1="19.6350375" x2="53.0669125" y2="19.41308125" width="0.1" layer="48"/>
<wire x1="53.0669125" y1="19.41308125" x2="53.62845" y2="19.3548" width="0.1" layer="48"/>
<wire x1="53.62845" y1="19.3548" x2="54.2036" y2="19.3548" width="0.1" layer="48"/>
<wire x1="54.2036" y1="19.3548" x2="54.2036" y2="-19.3548" width="0.1" layer="48"/>
<wire x1="54.2036" y1="-19.3548" x2="53.0669125" y2="-19.41308125" width="0.1" layer="48"/>
<wire x1="53.0669125" y1="-19.41308125" x2="51.98223125" y2="-19.6350375" width="0.1" layer="48"/>
<wire x1="51.98223125" y1="-19.6350375" x2="51.4590875" y2="-19.798715625" width="0.1" layer="48"/>
<wire x1="51.4590875" y1="-19.798715625" x2="50.46235625" y2="-20.220296875" width="0.1" layer="48"/>
<wire x1="50.46235625" y1="-20.220296875" x2="49.98876875" y2="-20.478203125" width="0.1" layer="48"/>
<wire x1="49.98876875" y1="-20.478203125" x2="49.545546875" y2="-20.7776375" width="0.1" layer="48"/>
<wire x1="49.545546875" y1="-20.7776375" x2="49.102325" y2="-21.077075" width="0.1" layer="48"/>
<wire x1="49.102325" y1="-21.077075" x2="48.689465625" y2="-21.4180375" width="0.1" layer="48"/>
<wire x1="48.689465625" y1="-21.4180375" x2="48.312553125" y2="-21.794953125" width="0.1" layer="48"/>
<wire x1="48.312553125" y1="-21.794953125" x2="47.9356375" y2="-22.171865625" width="0.1" layer="48"/>
<wire x1="47.9356375" y1="-22.171865625" x2="47.594671875" y2="-22.584725" width="0.1" layer="48"/>
<wire x1="47.594671875" y1="-22.584725" x2="46.995803125" y2="-23.47116875" width="0.1" layer="48"/>
<wire x1="46.995803125" y1="-23.47116875" x2="46.737896875" y2="-23.94475625" width="0.1" layer="48"/>
<wire x1="46.737896875" y1="-23.94475625" x2="46.316315625" y2="-24.9414875" width="0.1" layer="48"/>
<wire x1="46.316315625" y1="-24.9414875" x2="46.1526375" y2="-25.46463125" width="0.1" layer="48"/>
<wire x1="46.1526375" y1="-25.46463125" x2="45.93068125" y2="-26.5493125" width="0.1" layer="48"/>
<wire x1="45.93068125" y1="-26.5493125" x2="45.8724" y2="-27.11085" width="0.1" layer="48"/>
<wire x1="45.8724" y1="-27.11085" x2="45.8724" y2="-38.735" width="0.1" layer="48"/>
<wire x1="45.8724" y1="-38.735" x2="-45.8724" y2="-38.735" width="0.1" layer="20"/>
<wire x1="-45.8724" y1="-38.735" x2="-45.8724" y2="-27.11085" width="0.1" layer="48"/>
<hole x="-38.5" y="34" drill="3.7338"/>
<hole x="38.5" y="34" drill="3.7338"/>
<hole x="38.5" y="-34" drill="3.7338"/>
<hole x="-38.5" y="-34" drill="3.7338"/>
<wire x1="-45.8724" y1="38.735" x2="-45.8724" y2="-38.735" width="0.1" layer="20"/>
<wire x1="45.8724" y1="38.735" x2="45.8724" y2="-38.735" width="0.1" layer="20"/>
</package>
<package name="UNSEEN_ELEMENT">
</package>
</packages>
<symbols>
<symbol name="MODULE_SOCKET_1">
<pin name="A5" x="-10.16" y="-2.54" length="middle"/>
<pin name="A4" x="-10.16" y="2.54" length="middle"/>
<pin name="A3" x="-10.16" y="7.62" length="middle"/>
<pin name="A2" x="-10.16" y="12.7" length="middle"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="19.2679" width="0.254" layer="94"/>
<wire x1="7.62" y1="19.2679" x2="4.0279" y2="22.86" width="0.254" layer="94"/>
<wire x1="4.0279" y1="22.86" x2="-1.4879" y2="22.86" width="0.254" layer="94"/>
<wire x1="-1.4879" y1="22.86" x2="-5.08" y2="19.2679" width="0.254" layer="94"/>
<wire x1="-5.08" y1="19.2679" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<pin name="A1" x="-10.16" y="17.78" length="middle"/>
<pin name="B1" x="12.7" y="17.78" length="middle" rot="R180"/>
<pin name="B2" x="12.7" y="12.7" length="middle" rot="R180"/>
<pin name="B3" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="B4" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="B5" x="12.7" y="-2.54" length="middle" rot="R180"/>
<text x="-2.54" y="25.4" size="1.778" layer="94">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.778" layer="94">&gt;VALUE</text>
</symbol>
<symbol name="M02">
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="BOX">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-7.62" y="0" size="1.778" layer="94">MECHANICAL
OUTLINE</text>
<text x="-7.62" y="10.16" size="1.778" layer="94">&gt;NAME</text>
<text x="-7.62" y="-12.7" size="1.778" layer="94">&gt;VALUE</text>
</symbol>
<symbol name="UNSEEN_ELEMENT">
<text x="-2.54" y="5.08" size="1.778" layer="94">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.778" layer="94">&gt;VALUE</text>
<wire x1="-7.62" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MODULE_1">
<gates>
<gate name="G$1" symbol="MODULE_SOCKET_1" x="5.08" y="5.08"/>
</gates>
<devices>
<device name="" package="MODULE_1">
<connects>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="B1" pad="B1"/>
<connect gate="G$1" pin="B2" pad="B2"/>
<connect gate="G$1" pin="B3" pad="B3"/>
<connect gate="G$1" pin="B4" pad="B4"/>
<connect gate="G$1" pin="B5" pad="B5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOLENOID_THIN" package="SOLENOID_THIN">
<connects>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="B1" pad="B1"/>
<connect gate="G$1" pin="B2" pad="B2"/>
<connect gate="G$1" pin="B3" pad="B3"/>
<connect gate="G$1" pin="B4" pad="B4"/>
<connect gate="G$1" pin="B5" pad="B5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M02" prefix="SL" uservalue="yes">
<description>&lt;b&gt;AMP QUICK CONNECTOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="M02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SMD" package="02P">
<connects>
<connect gate="G$1" pin="1" pad="P$2"/>
<connect gate="G$1" pin="2" pad="P$3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="5MM" package="1X02_5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BOX">
<gates>
<gate name="G$1" symbol="BOX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PN453516">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1554F/G" package="1554F">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UNSEEN_ELEMENT">
<gates>
<gate name="G$1" symbol="UNSEEN_ELEMENT" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="UNSEEN_ELEMENT">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun LEDs&lt;/h3&gt;
This library contains discrete LEDs for illumination or indication, but no displays.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED_5MM">
<description>&lt;B&gt;LED 5mm PTH&lt;/B&gt;&lt;p&gt;
5 mm, round
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED-IR-THRU&lt;/li&gt;</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
</package>
<package name="LED_3MM">
<description>&lt;h3&gt;LED 3MM PTH&lt;/h3&gt;

3 mm, round.

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 3mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="22" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="22" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="22" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="22" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="22"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="22"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
</package>
<package name="LED-1206">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: &lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="2.4" y1="0.6825" x2="2.4" y2="-0.6825" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="0" y="0.9525" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.9525" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="0.65375" y1="0.6825" x2="0.65375" y2="-0.6825" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="0.47625" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="-0.47625" width="0.2032" layer="51"/>
</package>
<package name="LED-0603">
<description>&lt;B&gt;LED 0603 SMT&lt;/B&gt;&lt;p&gt;
0603, surface mount.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.075inch &lt;/li&gt;
&lt;li&gt;Area: 0.06" x 0.03"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED - BLUE&lt;/li&gt;</description>
<smd name="C" x="0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<smd name="A" x="-0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<text x="0" y="0.635" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.635" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.5875" y1="0.47625" x2="1.5875" y2="-0.47625" width="0.127" layer="21"/>
<wire x1="0.15875" y1="0.47625" x2="0.15875" y2="0" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="0.15875" y2="-0.47625" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="0.3175" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="-0.3175" width="0.127" layer="51"/>
</package>
<package name="LED_10MM">
<description>&lt;B&gt;LED 10mm PTH&lt;/B&gt;&lt;p&gt;
10 mm, round
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.2inch&lt;/li&gt;
&lt;li&gt;Diameter: 10mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;</description>
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="22"/>
<text x="0" y="5.715" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.715" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="51"/>
</package>
<package name="FKIT-LED-1206">
<description>&lt;B&gt;LED 1206 SMT&lt;/B&gt;&lt;p&gt;
1206, surface mount
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125"x 0.06" &lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="0" y="1.11125" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.11125" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0" y2="0.3175" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0" y2="-0.3175" width="0.2032" layer="51"/>
</package>
<package name="LED_3MM-NS">
<description>&lt;h3&gt;LED 3MM PTH- No Silk&lt;/h3&gt;

3 mm, round, no silk outline of package

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 3mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.8923" y1="1.2954" x2="1.8923" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.8923" y1="-1.2954" x2="1.8923" y2="-0.8382" width="0.254" layer="21"/>
</package>
<package name="LED_5MM-KIT">
<description>&lt;h3&gt;LED 5mm KIT PTH&lt;/h3&gt;
&lt;p&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example Device:
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
</package>
<package name="LED-1206-BOTTOM">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="21"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="22"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>&lt;h3&gt;LED&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;</description>
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="-3.429" y="-4.572" size="1.778" layer="95" font="vector" rot="R90">&gt;NAME</text>
<text x="1.905" y="-4.572" size="1.778" layer="96" font="vector" rot="R90" align="top-left">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D" uservalue="yes">
<description>&lt;b&gt;LED (Generic)&lt;/b&gt;
&lt;p&gt;Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. Generic LEDs with no color specified.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED_5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED_3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08794" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED_10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED_3MM-NS">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED_5MM-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Passives">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762"/>
<pad name="2" x="2.5" y="0" drill="0.762"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="2" diameter="3"/>
<pad name="P$2" x="10.16" y="0" drill="2" diameter="3"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="AXIAL-0.3EZ">
<description>This is the "EZ" version of the standard .3" spaced resistor package.&lt;br&gt;
It has a reduced top mask to make it harder to install upside-down.</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="-3.81" y="0" radius="0.508" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.523634375" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="3.81" y="0" radius="1.04726875" width="0" layer="30"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="AXIAL-0.3EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<description>&lt;h3&gt;Digital Ground Supply&lt;/h3&gt;</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-0.254" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="GND">
<description>&lt;h3&gt;Ground Supply (Earth Ground Symbol)&lt;/h3&gt;</description>
<pin name="3.3V" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-2.032" y1="0" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.524" x2="0.508" y2="-1.524" width="0.254" layer="94"/>
<text x="0" y="-1.778" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;h3&gt;Ground Supply Symbol&lt;/h3&gt;
&lt;p&gt;Generic signal ground supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND2" prefix="GND">
<description>&lt;h3&gt;Ground Supply (Earth Ground style)&lt;/h3&gt;
&lt;p&gt;Ground supply with a traditional "earth ground" symbol.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="GND" x="2.54" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.6" drill="0">
<clearance class="0" value="0.254"/>
</class>
</classes>
<parts>
<part name="JP1" library="SparkFun-RF" deviceset="XBEE" device="LOCK"/>
<part name="P+1" library="schematic" deviceset="3.3V" device=""/>
<part name="GND1" library="schematic" deviceset="GND" device=""/>
<part name="D2" library="SparkFun-LED" deviceset="LED" device="5MM" value="PWR">
<attribute name="PARTNO" value="APT3216SGC"/>
<attribute name="PROD_ID" value="DIO-09911"/>
</part>
<part name="U$1" library="Particle" deviceset="ELECTRON" device="DUALROW">
<attribute name="PARTNO" value="E260KIT"/>
</part>
<part name="GND2" library="schematic" deviceset="GND" device=""/>
<part name="GND3" library="schematic" deviceset="GND" device=""/>
<part name="P+2" library="schematic" deviceset="3.3V" device=""/>
<part name="GND4" library="schematic" deviceset="GND" device=""/>
<part name="D1" library="SparkFun-LED" deviceset="LED" device="5MM" value="PWR">
<attribute name="PARTNO" value="APT3216SGC"/>
<attribute name="PROD_ID" value="DIO-09911"/>
</part>
<part name="D0" library="SparkFun-LED" deviceset="LED" device="5MM" value="PWR">
<attribute name="PARTNO" value="APT3216SGC"/>
<attribute name="PROD_ID" value="DIO-09911"/>
</part>
<part name="R3" library="SparkFun-Passives" deviceset="RESISTOR" device="AXIAL-0.3" value="220">
<attribute name="PARTNO" value="CRCW1206220RFKEA"/>
</part>
<part name="R2" library="SparkFun-Passives" deviceset="RESISTOR" device="AXIAL-0.3" value="220">
<attribute name="PARTNO" value="CRCW1206220RFKEA"/>
</part>
<part name="R1" library="SparkFun-Passives" deviceset="RESISTOR" device="AXIAL-0.3" value="220">
<attribute name="PARTNO" value="CRCW1206220RFKEA"/>
</part>
<part name="U$3" library="TinkerElectric" deviceset="MODULE_1" device="" value="AUTO_BUCK"/>
<part name="GND5" library="schematic" deviceset="GND" device=""/>
<part name="GND6" library="schematic" deviceset="GND" device=""/>
<part name="POWER" library="TinkerElectric" deviceset="M02" device="5MM"/>
<part name="GND8" library="schematic" deviceset="GND" device=""/>
<part name="IO1" library="TinkerElectric" deviceset="M02" device="5MM"/>
<part name="U$2" library="TinkerElectric" deviceset="MODULE_1" device="" value="OPTO_INPUT"/>
<part name="GND11" library="schematic" deviceset="GND" device=""/>
<part name="GND12" library="schematic" deviceset="GND" device=""/>
<part name="GND13" library="SparkFun-PowerSymbols" deviceset="GND" device="" value="GND_BATT"/>
<part name="GND14" library="SparkFun-PowerSymbols" deviceset="GND2" device="" value="GND_BATT"/>
<part name="GND9" library="SparkFun-PowerSymbols" deviceset="GND2" device="" value="GND_BATT"/>
<part name="BTN" library="TinkerElectric" deviceset="M02" device="5MM"/>
<part name="GND7" library="schematic" deviceset="GND" device=""/>
<part name="U$4" library="TinkerElectric" deviceset="BOX" device="1554F/G">
<attribute name="PARTNO" value="HMF1554F/G"/>
</part>
<part name="P+3" library="schematic" deviceset="3.3V" device=""/>
<part name="U$12" library="TinkerElectric" deviceset="UNSEEN_ELEMENT" device="" value="GROMMET WATERPROOF"/>
<part name="U$13" library="TinkerElectric" deviceset="UNSEEN_ELEMENT" device="" value="SMA ANTENNA WATERPROOF"/>
<part name="U$14" library="TinkerElectric" deviceset="UNSEEN_ELEMENT" device="" value="XBEE_HEADER">
<attribute name="PARTNO" value="366"/>
</part>
<part name="U$15" library="TinkerElectric" deviceset="UNSEEN_ELEMENT" device="" value="XBEE_HEADER">
<attribute name="PARTNO" value="366"/>
</part>
<part name="U$16" library="TinkerElectric" deviceset="UNSEEN_ELEMENT" device="" value="UFL to SMA Bulkhead Waterproof"/>
<part name="U$17" library="TinkerElectric" deviceset="UNSEEN_ELEMENT" device="" value="1x5 Header"/>
<part name="U$18" library="TinkerElectric" deviceset="UNSEEN_ELEMENT" device="" value="1x5 Header"/>
<part name="U$5" library="TinkerElectric" deviceset="UNSEEN_ELEMENT" device="" value="1x5 Header"/>
<part name="U$6" library="TinkerElectric" deviceset="UNSEEN_ELEMENT" device="" value="1x5 Header"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="180.34" y="63.5" size="1.778" layer="91">Wireless Program via Xbee Tutorial
https://www.sparkfun.com/tutorials/122</text>
<text x="-17.78" y="86.36" size="1.778" layer="91">TODO:
-Local isolated IO
-Power, Automotive Grade
-Indicator Lights
</text>
<text x="-22.86" y="5.08" size="1.778" layer="91">TODO: Electron Layout
-Add 2 pins at top not seen (LiPo+USB)

---LATER---
-Change to 2 rows not 2 THP</text>
<text x="81.28" y="86.36" size="1.778" layer="91">Particle provides 800mA, Xbee needs 350mA max (PRSMA version)</text>
</plain>
<instances>
<instance part="JP1" gate="G$1" x="134.62" y="55.88"/>
<instance part="P+1" gate="G$1" x="109.22" y="78.74"/>
<instance part="GND1" gate="1" x="109.22" y="30.48"/>
<instance part="D2" gate="G$1" x="12.7" y="48.26" rot="R90">
<attribute name="PROD_ID" x="12.7" y="48.26" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PARTNO" x="12.7" y="48.26" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="U$1" gate="G$1" x="-10.16" y="50.8">
<attribute name="PARTNO" x="-10.16" y="50.8" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND2" gate="1" x="-38.1" y="60.96"/>
<instance part="GND3" gate="1" x="30.48" y="55.88"/>
<instance part="P+2" gate="G$1" x="10.16" y="81.28"/>
<instance part="GND4" gate="1" x="45.72" y="38.1"/>
<instance part="D1" gate="G$1" x="17.78" y="45.72" rot="R90">
<attribute name="PROD_ID" x="17.78" y="45.72" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PARTNO" x="17.78" y="45.72" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="D0" gate="G$1" x="22.86" y="43.18" rot="R90">
<attribute name="PROD_ID" x="22.86" y="43.18" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PARTNO" x="22.86" y="43.18" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="R3" gate="G$1" x="27.94" y="48.26">
<attribute name="PARTNO" x="27.94" y="48.26" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R2" gate="G$1" x="33.02" y="45.72">
<attribute name="PARTNO" x="33.02" y="45.72" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R1" gate="G$1" x="38.1" y="43.18">
<attribute name="PARTNO" x="38.1" y="43.18" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="U$3" gate="G$1" x="132.08" y="-15.24"/>
<instance part="GND5" gate="1" x="114.3" y="-22.86"/>
<instance part="GND6" gate="1" x="157.48" y="-5.08"/>
<instance part="POWER" gate="G$1" x="5.08" y="-106.68" rot="MR180"/>
<instance part="GND8" gate="1" x="27.94" y="-116.84"/>
<instance part="IO1" gate="G$1" x="2.54" y="-76.2" rot="MR180"/>
<instance part="U$2" gate="G$1" x="15.24" y="-50.8"/>
<instance part="GND11" gate="1" x="-2.54" y="-58.42"/>
<instance part="GND12" gate="1" x="73.66" y="-35.56"/>
<instance part="GND13" gate="1" x="-10.16" y="-60.96"/>
<instance part="GND14" gate="G$1" x="53.34" y="-45.72" rot="MR0"/>
<instance part="GND9" gate="G$1" x="25.4" y="-86.36" rot="MR0"/>
<instance part="BTN" gate="G$1" x="93.98" y="-104.14" rot="MR180"/>
<instance part="GND7" gate="1" x="116.84" y="-114.3"/>
<instance part="U$4" gate="G$1" x="170.18" y="-66.04">
<attribute name="PARTNO" x="170.18" y="-66.04" size="1.778" layer="96" display="off"/>
</instance>
<instance part="P+3" gate="G$1" x="66.04" y="-48.26"/>
<instance part="U$12" gate="G$1" x="190.5" y="-68.58"/>
<instance part="U$13" gate="G$1" x="190.5" y="25.4"/>
<instance part="U$14" gate="G$1" x="121.92" y="81.28">
<attribute name="PARTNO" x="121.92" y="81.28" size="1.778" layer="96" display="off"/>
</instance>
<instance part="U$15" gate="G$1" x="144.78" y="81.28">
<attribute name="PARTNO" x="144.78" y="81.28" size="1.778" layer="96" display="off"/>
</instance>
<instance part="U$16" gate="G$1" x="190.5" y="45.72"/>
<instance part="U$17" gate="G$1" x="121.92" y="17.78"/>
<instance part="U$18" gate="G$1" x="142.24" y="17.78"/>
<instance part="U$5" gate="G$1" x="5.08" y="-15.24"/>
<instance part="U$6" gate="G$1" x="25.4" y="-15.24"/>
</instances>
<busses>
</busses>
<nets>
<net name="3.3V" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="VDD"/>
<wire x1="116.84" y1="66.04" x2="109.22" y2="66.04" width="0.1524" layer="91"/>
<wire x1="109.22" y1="66.04" x2="109.22" y2="78.74" width="0.1524" layer="91"/>
<pinref part="P+1" gate="G$1" pin="3.3V"/>
<wire x1="81.28" y1="66.04" x2="109.22" y2="66.04" width="0.1524" layer="91"/>
<junction x="109.22" y="66.04"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="3V3"/>
<wire x1="2.54" y1="71.12" x2="10.16" y2="71.12" width="0.1524" layer="91"/>
<wire x1="10.16" y1="71.12" x2="10.16" y2="81.28" width="0.1524" layer="91"/>
<pinref part="P+2" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="B4"/>
<wire x1="27.94" y1="-48.26" x2="35.56" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-48.26" x2="35.56" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="B5"/>
<wire x1="35.56" y1="-53.34" x2="27.94" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-53.34" x2="66.04" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="P+3" gate="G$1" pin="3.3V"/>
<wire x1="66.04" y1="-53.34" x2="66.04" y2="-48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="GND"/>
<wire x1="116.84" y1="43.18" x2="109.22" y2="43.18" width="0.1524" layer="91"/>
<wire x1="109.22" y1="43.18" x2="109.22" y2="33.02" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="81.28" y1="43.18" x2="109.22" y2="43.18" width="0.1524" layer="91"/>
<junction x="109.22" y="43.18"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND@2"/>
<wire x1="-20.32" y1="68.58" x2="-38.1" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="68.58" x2="-38.1" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="2.54" y1="63.5" x2="30.48" y2="63.5" width="0.1524" layer="91"/>
<wire x1="30.48" y1="63.5" x2="30.48" y2="58.42" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="33.02" y1="48.26" x2="45.72" y2="48.26" width="0.1524" layer="91"/>
<wire x1="45.72" y1="48.26" x2="45.72" y2="45.72" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="45.72" y1="45.72" x2="45.72" y2="43.18" width="0.1524" layer="91"/>
<wire x1="45.72" y1="43.18" x2="45.72" y2="40.64" width="0.1524" layer="91"/>
<wire x1="38.1" y1="45.72" x2="45.72" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="43.18" y1="43.18" x2="45.72" y2="43.18" width="0.1524" layer="91"/>
<junction x="45.72" y="45.72"/>
<junction x="45.72" y="43.18"/>
</segment>
<segment>
<wire x1="121.92" y1="-17.78" x2="114.3" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="A5"/>
<pinref part="U$3" gate="G$1" pin="A4"/>
<wire x1="114.3" y1="-17.78" x2="106.68" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-12.7" x2="114.3" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-12.7" x2="114.3" y2="-17.78" width="0.1524" layer="91"/>
<label x="106.68" y="-17.78" size="1.778" layer="95"/>
<junction x="114.3" y="-17.78"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="114.3" y1="-20.32" x2="114.3" y2="-17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="162.56" y1="2.54" x2="157.48" y2="2.54" width="0.1524" layer="91"/>
<label x="157.48" y="2.54" size="1.778" layer="95"/>
<pinref part="U$3" gate="G$1" pin="B1"/>
<wire x1="157.48" y1="2.54" x2="149.86" y2="2.54" width="0.1524" layer="91"/>
<wire x1="149.86" y1="2.54" x2="144.78" y2="2.54" width="0.1524" layer="91"/>
<wire x1="149.86" y1="2.54" x2="149.86" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="B2"/>
<wire x1="149.86" y1="-2.54" x2="144.78" y2="-2.54" width="0.1524" layer="91"/>
<junction x="149.86" y="2.54"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="157.48" y1="-2.54" x2="157.48" y2="2.54" width="0.1524" layer="91"/>
<junction x="157.48" y="2.54"/>
</segment>
<segment>
<pinref part="POWER" gate="G$1" pin="2"/>
<wire x1="12.7" y1="-109.22" x2="27.94" y2="-109.22" width="0.1524" layer="91"/>
<label x="30.48" y="-109.22" size="1.778" layer="95"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="27.94" y1="-109.22" x2="33.02" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-114.3" x2="27.94" y2="-109.22" width="0.1524" layer="91"/>
<junction x="27.94" y="-109.22"/>
</segment>
<segment>
<wire x1="5.08" y1="-53.34" x2="-2.54" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="A5"/>
<wire x1="-2.54" y1="-53.34" x2="-10.16" y2="-53.34" width="0.1524" layer="91"/>
<label x="-10.16" y="-53.34" size="1.778" layer="95"/>
<junction x="-2.54" y="-53.34"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="-2.54" y1="-55.88" x2="-2.54" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="-10.16" y1="-58.42" x2="-10.16" y2="-53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="45.72" y1="-33.02" x2="40.64" y2="-33.02" width="0.1524" layer="91"/>
<label x="40.64" y="-33.02" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="B1"/>
<wire x1="40.64" y1="-33.02" x2="27.94" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="73.66" y1="-33.02" x2="40.64" y2="-33.02" width="0.1524" layer="91"/>
<junction x="40.64" y="-33.02"/>
</segment>
<segment>
<pinref part="BTN" gate="G$1" pin="2"/>
<wire x1="101.6" y1="-106.68" x2="116.84" y2="-106.68" width="0.1524" layer="91"/>
<label x="119.38" y="-106.68" size="1.778" layer="95"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="116.84" y1="-106.68" x2="121.92" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-111.76" x2="116.84" y2="-106.68" width="0.1524" layer="91"/>
<junction x="116.84" y="-106.68"/>
</segment>
</net>
<net name="TXO" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="TX"/>
<wire x1="-20.32" y1="66.04" x2="-30.48" y2="66.04" width="0.1524" layer="91"/>
<label x="-30.48" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="116.84" y1="60.96" x2="81.28" y2="60.96" width="0.1524" layer="91"/>
<label x="86.36" y="60.96" size="1.778" layer="95"/>
<pinref part="JP1" gate="G$1" pin="DIN"/>
</segment>
</net>
<net name="RXI" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="RX"/>
<wire x1="-20.32" y1="63.5" x2="-30.48" y2="63.5" width="0.1524" layer="91"/>
<label x="-30.48" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="DOUT"/>
<wire x1="116.84" y1="63.5" x2="81.28" y2="63.5" width="0.1524" layer="91"/>
<label x="86.36" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DIO12"/>
<wire x1="81.28" y1="58.42" x2="116.84" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="RESET"/>
<wire x1="81.28" y1="55.88" x2="116.84" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="RSSI"/>
<wire x1="116.84" y1="53.34" x2="81.28" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DIO11"/>
<wire x1="81.28" y1="50.8" x2="116.84" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="RES@8"/>
<wire x1="116.84" y1="48.26" x2="81.28" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DTR"/>
<wire x1="81.28" y1="45.72" x2="116.84" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DIO4"/>
<wire x1="152.4" y1="43.18" x2="175.26" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="CTS"/>
<wire x1="152.4" y1="45.72" x2="175.26" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DIO9"/>
<wire x1="175.26" y1="48.26" x2="152.4" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="RES@14"/>
<wire x1="152.4" y1="50.8" x2="175.26" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DIO5"/>
<wire x1="152.4" y1="53.34" x2="175.26" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="RTS"/>
<wire x1="152.4" y1="55.88" x2="175.26" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DIO3"/>
<wire x1="152.4" y1="58.42" x2="175.26" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DIO2"/>
<wire x1="175.26" y1="60.96" x2="152.4" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DIO1"/>
<wire x1="152.4" y1="63.5" x2="175.26" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DIO0"/>
<wire x1="175.26" y1="66.04" x2="152.4" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D2"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="2.54" y1="48.26" x2="10.16" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="17.78" y1="48.26" x2="22.86" y2="48.26" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D1"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="2.54" y1="45.72" x2="15.24" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D0"/>
<pinref part="D0" gate="G$1" pin="A"/>
<wire x1="2.54" y1="43.18" x2="20.32" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="22.86" y1="45.72" x2="27.94" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="D0" gate="G$1" pin="C"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="27.94" y1="43.18" x2="33.02" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBUCK_5V" class="0">
<segment>
<wire x1="162.56" y1="-17.78" x2="149.86" y2="-17.78" width="0.1524" layer="91"/>
<label x="160.02" y="-17.78" size="1.778" layer="95"/>
<pinref part="U$3" gate="G$1" pin="B5"/>
<pinref part="U$3" gate="G$1" pin="B4"/>
<wire x1="149.86" y1="-17.78" x2="144.78" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="144.78" y1="-12.7" x2="149.86" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-12.7" x2="149.86" y2="-17.78" width="0.1524" layer="91"/>
<junction x="149.86" y="-17.78"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VIN"/>
<wire x1="-20.32" y1="71.12" x2="-38.1" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="71.12" x2="-38.1" y2="81.28" width="0.1524" layer="91"/>
<label x="-38.1" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="VIN_FIRST" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="A1"/>
<wire x1="111.76" y1="2.54" x2="114.3" y2="2.54" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="A2"/>
<wire x1="114.3" y1="2.54" x2="121.92" y2="2.54" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-2.54" x2="114.3" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-2.54" x2="114.3" y2="2.54" width="0.1524" layer="91"/>
<label x="106.68" y="2.54" size="1.778" layer="95"/>
<junction x="114.3" y="2.54"/>
</segment>
<segment>
<wire x1="25.4" y1="-101.6" x2="27.94" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="POWER" gate="G$1" pin="1"/>
<wire x1="12.7" y1="-106.68" x2="27.94" y2="-106.68" width="0.1524" layer="91"/>
<label x="30.48" y="-106.68" size="1.778" layer="95"/>
<wire x1="27.94" y1="-106.68" x2="33.02" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-101.6" x2="27.94" y2="-106.68" width="0.1524" layer="91"/>
<junction x="27.94" y="-106.68"/>
</segment>
</net>
<net name="OPTO_IO1" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="A3"/>
<wire x1="5.08" y1="-43.18" x2="-30.48" y2="-43.18" width="0.1524" layer="91"/>
<label x="-27.94" y="-43.18" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="55.88" y1="50.8" x2="2.54" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="D3"/>
<label x="40.64" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="OPTO_EXT_IO" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="B2"/>
<wire x1="27.94" y1="-38.1" x2="58.42" y2="-38.1" width="0.1524" layer="91"/>
<label x="50.8" y="-38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IO1" gate="G$1" pin="1"/>
<wire x1="10.16" y1="-76.2" x2="30.48" y2="-76.2" width="0.1524" layer="91"/>
<label x="25.4" y="-76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND_BATT" class="0">
<segment>
<pinref part="IO1" gate="G$1" pin="2"/>
<wire x1="10.16" y1="-78.74" x2="25.4" y2="-78.74" width="0.1524" layer="91"/>
<label x="27.94" y="-78.74" size="1.778" layer="95"/>
<wire x1="25.4" y1="-78.74" x2="30.48" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-83.82" x2="25.4" y2="-78.74" width="0.1524" layer="91"/>
<junction x="25.4" y="-78.74"/>
<pinref part="GND9" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="B3"/>
<pinref part="GND14" gate="G$1" pin="3.3V"/>
<wire x1="27.94" y1="-43.18" x2="53.34" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BTN" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D4"/>
<wire x1="2.54" y1="53.34" x2="25.4" y2="53.34" width="0.1524" layer="91"/>
<label x="17.78" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BTN" gate="G$1" pin="1"/>
<wire x1="101.6" y1="-104.14" x2="121.92" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-104.14" x2="121.92" y2="-96.52" width="0.1524" layer="91"/>
<label x="109.22" y="-104.14" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
