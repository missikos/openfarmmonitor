#!/usr/bin/python
import os



def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""



certFile = open('cert.c', 'w')

#files to define
#//https://www.symantec.com/content/en/us/enterprise/verisign/roots/VeriSign-Class%203-Public-Primary-Certification-Authority-G5.pem
AMAZON_IOT_ROOT_CA_PEM = ['#define AMAZON_IOT_ROOT_CA_PEM \\',
	'"-----BEGIN CERTIFICATE----- \\r\\n"\\',
	'"MIIE0zCCA7ugAwIBAgIQGNrRniZ96LtKIVjNzGs7SjANBgkqhkiG9w0BAQUFADCB\\r\\n"\\',
	'"yjELMAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQL\\r\\n"\\',
	'"ExZWZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwNiBWZXJp\\r\\n"\\',
	'"U2lnbiwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MUUwQwYDVQQDEzxW\\r\\n"\\',
	'"ZXJpU2lnbiBDbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0\\r\\n"\\',
	'"aG9yaXR5IC0gRzUwHhcNMDYxMTA4MDAwMDAwWhcNMzYwNzE2MjM1OTU5WjCByjEL\\r\\n"\\',
	'"MAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQLExZW\\r\\n"\\',
	'"ZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwNiBWZXJpU2ln\\r\\n"\\',
	'"biwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MUUwQwYDVQQDEzxWZXJp\\r\\n"\\',
	'"U2lnbiBDbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0aG9y\\r\\n"\\',
	'"aXR5IC0gRzUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCvJAgIKXo1\\r\\n"\\',
	'"nmAMqudLO07cfLw8RRy7K+D+KQL5VwijZIUVJ/XxrcgxiV0i6CqqpkKzj/i5Vbex\\r\\n"\\',
	'"t0uz/o9+B1fs70PbZmIVYc9gDaTY3vjgw2IIPVQT60nKWVSFJuUrjxuf6/WhkcIz\\r\\n"\\',
	'"SdhDY2pSS9KP6HBRTdGJaXvHcPaz3BJ023tdS1bTlr8Vd6Gw9KIl8q8ckmcY5fQG\\r\\n"\\',
	'"BO+QueQA5N06tRn/Arr0PO7gi+s3i+z016zy9vA9r911kTMZHRxAy3QkGSGT2RT+\\r\\n"\\',
	'"rCpSx4/VBEnkjWNHiDxpg8v+R70rfk/Fla4OndTRQ8Bnc+MUCH7lP59zuDMKz10/\\r\\n"\\',
	'"NIeWiu5T6CUVAgMBAAGjgbIwga8wDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8E\\r\\n"\\',
	'"BAMCAQYwbQYIKwYBBQUHAQwEYTBfoV2gWzBZMFcwVRYJaW1hZ2UvZ2lmMCEwHzAH\\r\\n"\\',
	'"BgUrDgMCGgQUj+XTGoasjY5rw8+AatRIGCx7GS4wJRYjaHR0cDovL2xvZ28udmVy\\r\\n"\\',
	'"aXNpZ24uY29tL3ZzbG9nby5naWYwHQYDVR0OBBYEFH/TZafC3ey78DAJ80M5+gKv\\r\\n"\\',
	'"MzEzMA0GCSqGSIb3DQEBBQUAA4IBAQCTJEowX2LP2BqYLz3q3JktvXf2pXkiOOzE\\r\\n"\\',
	'"p6B4Eq1iDkVwZMXnl2YtmAl+X6/WzChl8gGqCBpH3vn5fJJaCGkgDdk+bW48DW7Y\\r\\n"\\',
	'"5gaRQBi5+MHt39tBquCWIMnNZBU4gcmU7qKEKQsTb47bDN0lAtukixlE0kF6BWlK\\r\\n"\\',
	'"WE9gyn6CagsCqiUXObXbf+eEZSqVir2G3l6BFoMtEMze/aiCKm0oHw0LxOXnGiYZ\\r\\n"\\',
	'"4fQRbxC1lfznQgUy286dUV4otp6F01vvpX1FQHKOtw5rDgb7MzVIcbidJ4vEZV8N\\r\\n"\\',
	'"hnacRHr2lVz2XTIIM6RUthg/aFzyQkqFOFSDX9HoLPKsEdao7WNq\\r\\n"\\',
	'"-----END CERTIFICATE----- "']


for item in AMAZON_IOT_ROOT_CA_PEM:
  certFile.write("%s\n" % item)


#Couple new lines
certFile.write("\n\n")


certFile.write("const char amazonIoTRootCaPem[] = AMAZON_IOT_ROOT_CA_PEM;")
certFile.write("\n\n")

#*.cer.pem
#CELINT_KEY_CRT_PEM 
fname = "test2.cert.pem"

certFile.write('#define CELINT_KEY_CRT_PEM'+' \\\n')
with open(fname) as f:
	for line in f:
		if line != '\n':
			if "---END " not in line: 
				output = '"'+line.rstrip('\n')+'\\r'+'\\n'+'"\\\n'
				certFile.write(output)
				#print(output)
			else:
				output = '"'+line.rstrip('\n')+'"'
				certFile.write(output)
				#print(output)
				break
 
f.close()

#Couple new lines
certFile.write("\n\n")


certFile.write("const char clientKeyCrtPem[] = CELINT_KEY_CRT_PEM;")
certFile.write("\n\n")



#*.private.key
#CELINT_KEY_PEM



#Read the cert file first

fname = "test2.private.key"


certFile.write('#define CELINT_KEY_PEM'+' \\\n')

with open(fname) as f:
	for line in f:
		if line != '\n':
			if "---END " not in line: 
				output = '"'+line.rstrip('\n')+'\\r'+'\\n'+'"\\\n'
				certFile.write(output)
				#print(output)
			else:
				output = '"'+line.rstrip('\n')+'"'
				certFile.write(output)
				#print(output)
				break
f.close()
certFile.write("\n\n")
certFile.write("const char clientKeyPem[] = CELINT_KEY_PEM;")
certFile.write("\n\n")
#certFile.write('"-----BEGIN CERTIFICATE-----\\r\\n"                                       \\\n') 
#certFile.write("and this is another line.") 
#certFile.write("Why? Because we can.") 

awsAddress = ""
with open('start.sh') as f:
	for line in f:
		#print(line)
		if ".amazonaws.com" in line:
			awsAddress = find_between( line, "--host-name=", ".amazonaws.com" )
			break
if awsAddress == "":
	print("FAULT: Didn't find AWS address")
else:
	awsAddress = awsAddress + ".amazonaws.com"

#certFile.write('#define MQTT_ADDRESS "aurahiq1jkf2f.iot.ap-southeast-2.amazonaws.com"')
certFile.write('#define MQTT_ADDRESS "'+awsAddress+'"')

f.close()




certFile.close() 







