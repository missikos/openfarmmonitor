

import os 
from shutil import copyfile

#Create cert file
os.chdir('certs') #into certs directory
os.system('python makeCertFile.py')

#Move cert file
os.chdir('..') #up dir
copyfile('certs/cert.c', 'main/cert.c')

#Compile in cloud
os.chdir('main')
os.system('particle compile photon . --saveTo firmware.bin')

#user put into DFU


#Upload via USB
os.system('particle flash --usb firmware.bin')


