// This #include statement was automatically added by the Particle IDE.
//#define MBEDTLS_SSL_MAX_CONTENT_LEN 4096 //Set this in config.c of MBEDTLS




#include <MQTT-TLS.h>
#include <cert.c>
#include <XBee.h>

XBee xbee = XBee();
XBeeResponse response = XBeeResponse();
ZBRxResponse rx = ZBRxResponse();
ModemStatusResponse msr = ModemStatusResponse();
ZBRxIoSampleResponse ioSample = ZBRxIoSampleResponse();
XBeeAddress64 test = XBeeAddress64();
#define MAX_FRAME_DATA_SIZE 110



void callback(char* topic, byte* payload, unsigned int length);



MQTT client(MQTT_ADDRESS, 8883, callback);

int counter = 0;

// recieve message
void callback(char* topic, byte* payload, unsigned int length) {
    char p[length + 1];
    memcpy(p, payload, length);
    p[length] = NULL;
    String message(p);

    if (message.equals("RED")){
        RGB.color(255, 0, 0);
        counter = counter+1;}
    else if (message.equals("GREEN")){
        RGB.color(0, 255, 0);
        counter = counter+1;}
    else if (message.equals("BLUE")){
        RGB.color(0, 0, 255);
        counter = counter+1;}
    else
        RGB.color(255, 255, 255);
    delay(1000);
}

#define ONE_DAY_MILLIS (24 * 60 * 60 * 1000)
unsigned long lastSync = millis();






String thisDeviceId = System.deviceID();

//Used in MQTT messages
const String productVersion = "v1";
const String productId = "agpump";



void setup() {
  //Turn off charging on Electron because I didn't like red flashing light
  #if PLATFORM_ID == 10 // Electron
    setCharging(false);
  #endif



  Serial.begin(9600); //this is to PC
  Serial1.begin(9600); //this is to XBee
  xbee.begin(Serial1);

  if (millis() - lastSync > ONE_DAY_MILLIS) {
      Particle.syncTime();
      lastSync = millis();
  }

  RGB.control(true);

  // enable tls. set Root CA pem, private key file.
  client.enableTls(amazonIoTRootCaPem, sizeof(amazonIoTRootCaPem),
                   clientKeyCrtPem, sizeof(clientKeyCrtPem),
                   clientKeyPem, sizeof(clientKeyPem));
  Serial.println("tls enable");

  // connect to the server
  client.connect("sparkclient");

  // publish/subscribe
  if (client.isConnected()) {
      Serial.println("client connected");
      client.publish(productId+"/"+productVersion+"/"+thisDeviceId, "started"); //let the world know it booted
      client.subscribe(productId+"/"+productVersion+"/"+thisDeviceId+"/cmd"); //Listen for commands
  }
}



typedef struct {
    String xbeeId;
    int sensorValue;
    int timeStamp;
    int sensorValuePrevious;
    bool timedOut;
    bool cloudAck;
} deviceStruct;

const int devices = 10; //# Devices Supported
deviceStruct device[devices]; //Enumerate the device structs
int checkInTimeout = 300;//Number of seconds that we expect to hear from devices




void loop() {
  Serial.println("MEM:");
  Serial.println(System.freeMemory());

  checkXbee();



  if (client.isConnected()) {

    //If we need to update MQTT
    checkNotifications();
    //client.publish(productId+"/"+productVersion+"/"+thisDeviceId, "started"); //let the world know it booted



    client.loop();
  }else{
    Serial.println("ERROR:Not connected to MQTT, retrying");
    client.connect("sparkclient");
    if (client.isConnected()) {
        Serial.println("SUCCESS: client re-connected");
        client.publish(productId+"/"+productVersion+"/"+thisDeviceId, "re-connected");
        client.subscribe("inTopic/message");
    }
  }
  delay(2000);

}


int checkNotifications(){
    //Notifications needed?
    for (int i = 0; i < devices; i++) {
        if (device[i].sensorValue != device[i].sensorValuePrevious && device[i].sensorValuePrevious != -1 && device[i].cloudAck != 1) {
            client.publish(productId+"/"+productVersion+"/"+thisDeviceId+"/"+device[i].xbeeId+"/dif", String(device[i].sensorValue));
            device[i].cloudAck = 1; //TODO: Technically this should be the cloud calling back from SUBSCRIBE
            //Could bundle up into one message
        }
    }
    return 0;
}


void checkXbee(){
      //attempt to read a packet
    Serial.print("STATUS:checkXbee()");
    String nodeID = "";
    int IOdata = -1;

    xbee.readPacket();

    if (xbee.getResponse().isAvailable()) {
    // got something
      Serial.print("STATUS:response Available");
        //IO SAMPLE, Extract DIO3
        if (xbee.getResponse().getApiId() == ZB_IO_SAMPLE_RESPONSE) {
            XbeeIOSampleHandling(nodeID,IOdata);
        }else {
          Serial.print("Expected I/O Sample, but got ");
          Serial.print(xbee.getResponse().getApiId(), HEX);
        }


    } else if (xbee.getResponse().isError()) {
    Serial.print("Error reading packet.  Error code: ");
    Serial.println(xbee.getResponse().getErrorCode());
    }
}

void printDeviceStruct(){
  for (int i = 0; i < devices; i++) {

    //get it's name from the array
    Serial.print("deviceID:"+String(i)+", ");
    Serial.print("xbeeId:"+ String(device[i].xbeeId)+", ");// = ""//String(i);

    Serial.print("sensorValue:"+ String(device[i].sensorValue)+", ");// = -1;
    Serial.print("lastTimeStamp:"+ String(device[i].timeStamp)+", ");// = -1;
    Serial.print("sensorValuePrevious:"+ String(device[i].sensorValuePrevious)+", ");// = -1;

    Serial.print("timedOut:"+ String(device[i].timedOut)+", ");//= -1;
    Serial.println("cloudAck:"+ String(device[i].cloudAck)+", ");// = -1;

  }
}

void XbeeIOSampleHandling(String nodeID, int IOdata){
    xbee.getResponse().getZBRxIoSampleResponse(ioSample);

    Serial.print("Received I/O Sample from: ");

    nodeID = String(ioSample.getRemoteAddress64().getLsb(), HEX);

    Serial.print(ioSample.getRemoteAddress64().getMsb(), HEX);
    Serial.print(ioSample.getRemoteAddress64().getLsb(), HEX);

    Serial.print("NodeID="+nodeID);

    Serial.println("");

    if (ioSample.containsDigital()) {
    Serial.println("Sample contains digtal data");
        //Specifically extract DI3
        if (ioSample.isDigitalEnabled(3)) {
            //Serial.println(ioSample.isDigitalOn(3), DEC);
            IOdata = int(ioSample.isDigitalOn(3));
        }

    }

    //Store the data
    if (nodeID != "" and IOdata != -1){
        //We have data to store
        Serial.println("We have data to store");
        Serial.println("nodeID="+nodeID+", IO="+IOdata);
        insertIOData(nodeID,IOdata);
    }

    printDeviceStruct();
}



int insertIOData(String nodeID, int IOdata){
    Serial.println("Inserting data...");
    //does it have a slot?
    int deviceID = xbeeID2deviceID(nodeID);
    if (deviceID == -1){
        Serial.println("ERROR: Couldn't insert data, no slots");
        return -1;
    }


    device[deviceID].sensorValuePrevious = device[deviceID].sensorValue;

    device[deviceID].sensorValue = IOdata;


    //ensure there was a valid time, ie if not online then it is old.
    if(Time.now() > 1526271928){
        device[deviceID].timeStamp = Time.now();
    }
    Serial.println("Local Time: "+Time.timeStr());
    device[deviceID].timedOut = 0;
    device[deviceID].cloudAck = 0;
}


//Return array position given the xbeeID

int xbeeID2deviceID(String xbeeIdInput){
    Serial.println("Finding data slot...");
    for (int i = 0; i < devices; i++) {
        if(device[i].xbeeId == xbeeIdInput){
            Serial.println("Found slot...");
            return i;
        }
    }
    Serial.println("Couldn't find slot, finding blank one...");
    //make a slot
    for (int i = 0; i < devices; i++) {
        if(String(device[i].xbeeId) == String("")){
            device[i].xbeeId = String(xbeeIdInput);
            Serial.println("Created slot...");
            return i;
        }
    }

    return -1;//couldn't get a slot
}



//############################################
//####### Initiate Device Structure ##########
//############################################

void initDeviceStruct(){

    for (int i = 0; i < devices; i++) {

        //get it's name from the array
        device[i].xbeeId = "";//String(i);

        device[i].sensorValue = -1;
        device[i].timeStamp = -1;
        device[i].sensorValuePrevious = -1;

        device[i].timedOut= 0;
        device[i].cloudAck = 0;

    }
}







//############################################
//############### Setup Charging #############
//############################################
//on/off the red light and LiPo, WARNING: Setting is sticky ie have to re-enable
#if PLATFORM_ID == 10 // Electron is only one that can run this
void setCharging(bool enable) {
    //https://community.particle.io/t/disabling-the-electron-red-charging-led-when-not-using-the-lipo/26604
	PMIC pmic;

	// DisableCharging turns of charging. DisableBATFET completely disconnects the battery.
	if (enable) {
		pmic.enableCharging();
		pmic.enableBATFET();
	}
	else {
		pmic.disableCharging();
		pmic.disableBATFET();
	}

	// Disabling the watchdog is necessary, otherwise it will kick in and turn
	// charing at random times, even when sleeping.

	// This disables both the watchdog and the charge safety timer in
	// Charge Termination/Timer Control Register REG05
	// pmic.disableWatchdog() disables the watchdog, but doesn't disable the
	// charge safety timer, so the red LED will start blinking slowly after
	// 1 hour if you don't do both.
	byte DATA = pmic.readChargeTermRegister();

	if (enable) {
		DATA |= 0b00111000;
	}
	else {
		// 0b11001110 = disable watchdog
		// 0b11000110 = disable watchdog and charge safety timer
		DATA &= 0b11000110;
	}

	// This would be easier if pmic.writeRegister wasn't private (or disable
	// charge safety timer had an exposed method
	Wire3.beginTransmission(PMIC_ADDRESS);
	Wire3.write(CHARGE_TIMER_CONTROL_REGISTER);
	Wire3.write(DATA);
	Wire3.endTransmission(true);

}

#endif






void flashLed(int pin, int times, int wait) {

    for (int i = 0; i < times; i++) {
      digitalWrite(pin, HIGH);
      delay(wait);
      digitalWrite(pin, LOW);

      if (i + 1 < times) {
        delay(wait);
      }
    }
}
