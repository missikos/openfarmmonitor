PRODUCT_ID(7528);
PRODUCT_VERSION(1);




// This #include statement was automatically added by the Particle IDE.
#include <XBee.h>
XBee xbee = XBee();
XBeeResponse response = XBeeResponse();
ZBRxResponse rx = ZBRxResponse();
ModemStatusResponse msr = ModemStatusResponse();
ZBRxIoSampleResponse ioSample = ZBRxIoSampleResponse();
XBeeAddress64 test = XBeeAddress64();

int statusLed = 13;
int errorLed = 13;
int dataLed = 13;


//SYSTEM_MODE(AUTOMATIC);
//SYSTEM_THREAD(ENABLED);

//StateMachine States
#define START 0
#define READ_SENSORS 1
#define LISTEN_MESSAGES 3
#define FAULT 4
#define HEARTBEAT 5
#define PROCESS_MESSAGES 6
#define SLEEP 7
#define TEST_SYSTEM 8
uint8_t fsm_state = START; //Initial statemachine start


//XBEE
#define MAX_FRAME_DATA_SIZE 110


typedef struct {
    
    int index; //an ID reference if needed
    String xbeeId;
    String name;
    
    int sensorValue;
    int lastTimeStamp;
    int sensorValuePrevious;
    
    int timedOut;
    int cloudNotified;
    
} deviceStruct;

const int devices = 10; //# Devices Supported
deviceStruct device[devices]; //Enumerate the device structs
int checkInTimeout = 300;//Number of seconds that we expect to hear from devices



typedef struct {
    String nodeId;
    String nodeName;
} nodeNameStruct;

nodeNameStruct nodeNames[devices]; //Make 10 of them

//Create device






//############################################
//############### IO Config ##################
//############################################
int led1 = D0; // Instead of writing D0 over and over again, we'll write led1
int led2 = D1; // Instead of writing D7 over and over again, we'll write led2
int led3 = D2;
int internalLed = D7;


String message = "Device booting, please try again in 5 mins...";







typedef struct  {
      String number;
      String name;
    } userObject;
userObject users[5];






int SMSResponse(String command);
int SMSSnooze(String amount);




void setup() {
    
    
    
    nodeNames[0].nodeId = "41575802";
    nodeNames[0].nodeName = "Pump 1";
    
    setCharging(false);
    
    Serial.begin(9600); //this is to PC
    
    // Start XBee
    Serial1.begin(9600); //this is to XBee
    xbee.begin(Serial1);
    
    Particle.function("SMSResponse", SMSResponse);
    Particle.function("SMSSnooze", SMSSnooze);
    //xbee.setSerial(Serial1); //Need both??

    pinMode(led1, OUTPUT);
    pinMode(led2, OUTPUT);
    pinMode(led3, OUTPUT);
    

    
    
    pinMode(internalLed, OUTPUT);
    
    
    
    Particle.syncTime();

    
    initDeviceStruct();
    
    
    flashLed(statusLed, 3, 50);
    
    delay(1000);
    Particle.publish("twilio_sms", "Your device is now ONLINE!", 60,  PRIVATE);




}

void loop() {
    checkXbee();
 
    //TODO: Add snooze control
    checkNotifications();

}



int checkNotifications(){
    
    
    
    //Notifications needed?
    String sendText = "";
    for (int i = 0; i < devices; i++) {
        if (device[i].sensorValue != device[i].sensorValuePrevious && device[i].sensorValuePrevious != -1 && device[i].cloudNotified != 1) {
            sendText = "\n"+String(device[i].name)+" is now ";
            
            //Determine on or off
            if(device[i].sensorValue == 1){
                sendText = sendText + "ON";
            }else if (device[i].sensorValue == 0){
                sendText = sendText + "OFF";
            }else{
                sendText = sendText + "???";
            }
            
        }
    }
    
    if (sendText != ""){
        //There is something to send
        //1.Prepend it
        sendText = String("These changes just happened:\n"+sendText);
        
        
     
        Particle.publish("twilio_sms", sendText, 60,  PRIVATE);

        for (int i = 0; i < 10; i++) {
            device[i].cloudNotified = 1; //so we dont repeat
        }
        
        //Set that cloud was notified 
        
        //2.Send it
        return 1;
    }
    
    return 0;
}








String returnNodeName (String nodeIDinput){
    
    for (int i = 0; i < devices; i++) {
        if (nodeNames[i].nodeId == nodeIDinput) {
            return nodeNames[i].nodeName;
        }
    }
    //None match, return generic
    return "Unknow pump ("+String(nodeIDinput)+")";

}



int SMSResponse(String command)
{
  // look for the matching argument "coffee" <-- max of 64 characters long
  if(command.toLowerCase() == "status" or command.toLowerCase() == "stat")
  {
    currentStatus();
    return 1;
  }
  return -1;
}



int SMSSnooze(String amount)
{
    //Confirm it is an int
    
    //if it's zero, turn off snooze
    
  return -1;
}



void currentStatus(){
    Serial.println("Replying to currentStatus request...");
    String sendText = "";
    for (int i = 0; i < devices; i++) {
        if (device[i].name.length() > 3) {
            sendText = "\n"+String(device[i].name)+" is ";
            
            //Determine on or off
            if(device[i].sensorValue == 1){
                sendText = sendText + "ON";
            }else if (device[i].sensorValue == 0){
                sendText = sendText + "OFF";
            }else{
                sendText = sendText + "???";
            }
            
        }
    }
    
    if (sendText != ""){
        //There is something to send
        //1.Prepend it
        
        sendText = "Current Status:"+sendText;
        Particle.publish("twilio_sms", sendText, 60,  PRIVATE);
    }else{
        sendText = "I couldnt find any devices, are they powered?";
        Particle.publish("twilio_sms", sendText, 60,  PRIVATE);
    }
}




void checkXbee(){
      //attempt to read a packet    
      
    String nodeID = "";
    int IOdata = -1;
      
    xbee.readPacket();
    
    if (xbee.getResponse().isAvailable()) {
    // got something
    
        //IO SAMPLE, Extract DIO3
        if (xbee.getResponse().getApiId() == ZB_IO_SAMPLE_RESPONSE) {
            XbeeIOSampleHandling(nodeID,IOdata);
        }else {
          Serial.print("Expected I/O Sample, but got ");
          Serial.print(xbee.getResponse().getApiId(), HEX);
        }    
        
        
    } else if (xbee.getResponse().isError()) {
    Serial.print("Error reading packet.  Error code: ");  
    Serial.println(xbee.getResponse().getErrorCode());
    }
}
    



void XbeeIOSampleHandling(String nodeID, int IOdata){
    xbee.getResponse().getZBRxIoSampleResponse(ioSample);
    
    Serial.print("Received I/O Sample from: ");
    
    nodeID = String(ioSample.getRemoteAddress64().getLsb(), HEX);
    
    Serial.print(ioSample.getRemoteAddress64().getMsb(), HEX);  
    Serial.print(ioSample.getRemoteAddress64().getLsb(), HEX);  
    
    Serial.print("NodeID="+nodeID);
    
    Serial.println("");
    
    if (ioSample.containsDigital()) {
    Serial.println("Sample contains digtal data");
        //Specifically extract DI3
        if (ioSample.isDigitalEnabled(3)) {
            //Serial.println(ioSample.isDigitalOn(3), DEC);
            IOdata = int(ioSample.isDigitalOn(3));
        }
    
    }    
    
    //Store the data
    if (nodeID != "" and IOdata != -1){
        //We have data to store
        Serial.println("We have data to store");
        Serial.println("nodeID="+nodeID+", IO="+IOdata);
        insertIOData(nodeID,IOdata);
    }

    printDeviceStruct();
}





//############################################
//####### Initiate Device Structure ##########
//############################################

void initDeviceStruct(){

    for (int i = 0; i < devices; i++) {

        //get it's name from the array
        device[i].xbeeId = "";//String(i);
        device[i].name = "";//String(i);
        
        device[i].sensorValue = -1;
        device[i].lastTimeStamp = -1;
        device[i].sensorValuePrevious = -1;
        
        device[i].timedOut= -1;
        device[i].cloudNotified = -1;   
        
    } 
}



void printDeviceStruct(){
    for (int i = 0; i < devices; i++) {

        //get it's name from the array
        Serial.print("deviceID:"+String(i)+", ");
        Serial.print("xbeeId:"+ String(device[i].xbeeId)+", ");// = ""//String(i);
        Serial.print("name:"+ String(device[i].name)+", ");// = ""//String(i);
        
        Serial.print("sensorValue:"+ String(device[i].sensorValue)+", ");// = -1;
        Serial.print("lastTimeStamp:"+ String(device[i].lastTimeStamp)+", ");// = -1;
        Serial.print("sensorValuePrevious:"+ String(device[i].sensorValuePrevious)+", ");// = -1;
        
        Serial.print("timedOut:"+ String(device[i].timedOut)+", ");//= -1;
        Serial.println("cloudNotified:"+ String(device[i].cloudNotified)+", ");// = -1;   
        
    } 
    
    
    
}




int insertIOData(String nodeID, int IOdata){
    Serial.println("Inserting data...");
    //does it have a slot?
    int deviceID = xbeeID2deviceID(nodeID);
    if (deviceID == -1){
        Serial.println("ERROR: Couldn't insert data, no slots");
        return -1;
    }

    
    device[deviceID].sensorValuePrevious = device[deviceID].sensorValue;
    
    device[deviceID].sensorValue = IOdata;
    
    device[deviceID].name = returnNodeName(nodeID);
    

    //ensure there was a valid time, ie if not online then it is old.
    if(Time.now() > 1526271928){
        device[deviceID].lastTimeStamp = Time.now();
    }
    
    
    
    
    

    Serial.println("Local Time: "+Time.timeStr());

    device[deviceID].timedOut = 0;

    
    
    device[deviceID].cloudNotified = 0;
}





//Return array position given the xbeeID

int xbeeID2deviceID(String xbeeIdInput){
    Serial.println("Finding data slot...");
    for (int i = 0; i < devices; i++) {
        if(device[i].xbeeId == xbeeIdInput){
            Serial.println("Found slot...");
            return i;
        }
    }
    Serial.println("Couldn't find slot, finding blank one...");
    //make a slot
    for (int i = 0; i < devices; i++) {
        if(String(device[i].xbeeId) == String("")){
            device[i].xbeeId = String(xbeeIdInput);
            Serial.println("Created slot...");
            return i;
        }
    }

    return -1;//couldn't get a slot
}






//############################################
//############### Setup Charging #############
//############################################
//on/off the red light and LiPo, WARNING: Setting is sticky ie have to re-enable

void setCharging(bool enable) {
    //https://community.particle.io/t/disabling-the-electron-red-charging-led-when-not-using-the-lipo/26604
	PMIC pmic;

	// DisableCharging turns of charging. DisableBATFET completely disconnects the battery.
	if (enable) {
		pmic.enableCharging();
		pmic.enableBATFET();
	}
	else {
		pmic.disableCharging();
		pmic.disableBATFET();
	}

	// Disabling the watchdog is necessary, otherwise it will kick in and turn
	// charing at random times, even when sleeping.

	// This disables both the watchdog and the charge safety timer in
	// Charge Termination/Timer Control Register REG05
	// pmic.disableWatchdog() disables the watchdog, but doesn't disable the
	// charge safety timer, so the red LED will start blinking slowly after
	// 1 hour if you don't do both.
	byte DATA = pmic.readChargeTermRegister();

	if (enable) {
		DATA |= 0b00111000;
	}
	else {
		// 0b11001110 = disable watchdog
		// 0b11000110 = disable watchdog and charge safety timer
		DATA &= 0b11000110;
	}

	// This would be easier if pmic.writeRegister wasn't private (or disable
	// charge safety timer had an exposed method
	Wire3.beginTransmission(PMIC_ADDRESS);
	Wire3.write(CHARGE_TIMER_CONTROL_REGISTER);
	Wire3.write(DATA);
	Wire3.endTransmission(true);

}







void flashLed(int pin, int times, int wait) {
    
    for (int i = 0; i < times; i++) {
      digitalWrite(pin, HIGH);
      delay(wait);
      digitalWrite(pin, LOW);
      
      if (i + 1 < times) {
        delay(wait);
      }
    }
} 





