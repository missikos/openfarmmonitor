"""
Basic example of SMS and MMS response with Twilio.

Demostrates webhook validation, matching against a master number, and the
use of the Twilio Python helper library.
"""

from __future__ import print_function

import os
import urllib
from twilio.twiml.messaging_response import Body, Message, Redirect, MessagingResponse

from twilio.request_validator import RequestValidator

from botocore.vendored import requests
import re
twilio_master_number = os.environ['MASTER_NUMBER']


particle_id = os.environ['PARTICLE_ID']
particle_account = os.environ['PARTICLE_ACC']


def handler(event, context):
    """Lambda function handler for Twilio response."""
    resp = MessagingResponse()
    print("Event:", event)




    if u'twilioSignature' in event and u'Body' in event:

        form_parameters = {
            k: urllib.unquote_plus(v) for k, v in event.items()
            if k != u'twilioSignature'
        }

        validator = RequestValidator(os.environ['AUTH_TOKEN'])
        request_valid = validator.validate(
            os.environ['REQUEST_URL'],
            form_parameters,
            event[u'twilioSignature']
        )

        # print("Form params:",form_parameters,"validity:",request_valid)
        # If the request is valid and this is from the master number,
        # give the secret!
        if request_valid and u'From' in form_parameters and \
                form_parameters[u'From'] == \
                os.environ['MASTER_NUMBER'].encode('utf8'):
            # The message is validated and is from the master number

             
            if event['Body'].lower().find("stat") != -1:
                #status sent
                secret_message = "Correct number and status"

                #Fire off the request to the device
                try:
                    response = requests.post(
                                        'https://api.particle.io/v1/devices/' + str(particle_id) +
                                        '/' + 'SMSResponse',timeout=6,
                                        data={
                                            'access_token': str(particle_account),
                                            'arg': 'status'
                                        }
                                )
                except requests.exceptions.RequestException as e:  # This is the correct syntax
                    print("Request failed")
                    secret_message = "We couldn't contact the device. \nIs it powered?\nCall 0421117141 for further help."
                    resp.message(secret_message)
                    return str(resp)
                else:
                    print("Request SUCCESS")
                    return
            elif event['Body'].lower().find("snooze") != -1:
                #does it want to cancel?
                if event['Body'].lower().find("can") != -1:
                    secret_message = "Snooze is cancelled"
                else:
                    snoozeInput = event['Body']
                    try:
                        extractedNumber = re.findall("\d+", snoozeInput)[0]
                    except:
                        secret_message = "Argh, I didn't understand how many minutes you wanted to snooze for.\ntry 'snooze 5'"
                    else:
                        secret_message = "Snoozing for "+str(extractedNumber)+" minutes"
            else:
                secret_message = "Hi!\nI accept the following:\n'status' - I'll tell you what your pumps are up to.\n'snooze X' - I'll prevent messages for X minutes"


            resp.message(secret_message)

            return str(resp)

        else:
            # Validation failed in some way; don't give up the secret

            secret_message = "Doesn't look like we have met! \nWe are happy to demonstrate the Pump Notifier to you, simply call or text 0421117141. \n-www.TinkerElectric.com"
            resp.message(secret_message)
            return str(resp)

    else:
        resp.message("Lacked Signature")
        return str(resp)